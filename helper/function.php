<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 30/03/2018
 * Time: 11:24 SA
 */

use Illuminate\Support\Facades\Cache;

if (!function_exists('locale')) {
    function locale()
    {
        $data = array(
            'vi' => 'Tieng Viet',
            'en' => 'English',
            'fr' => 'French'
        );

        return $data;
    }
}
if (!function_exists('payment_method')) {
    function payment_method()
    {
        $data = array(
            'pending' => 'Pending',
            'approved' => 'Approved',
            'processing' => 'Processing',
            'finish' => 'Finish',
            'done' => 'Done',
            'fail' => 'Fail'
        );
        return $data;
    }
}

if (!function_exists('payment_status')) {
    function payment_status()
    {
        $data = array(
            ' pending' => 'Pending',
            ' paid' => ' Paid'
        );
        return $data;
    }
}

if (!function_exists('object_name')) {
    function object_name()
    {
        $data = array(
            'cms_abouts' => 'About',
            'cms_articles' => 'Article',
            'cms_contacts' => 'Contact',
            'cms_services' => 'Service',
            'cms_subscribes' => 'Subscribe',
            'cms_portfolios' => 'Portfolio'
        );

        return $data;
    }
}

if (!function_exists('showCategories')) {
    function showCategories($categories, $parent_id = 0, $char = '', &$option = "")
    {
        foreach ($categories as $key => $item) {
            // Nếu là chuyên mục con thì hiển thị
            if ($item['parent_id'] == $parent_id) {
                $option .= '<option value="' . $item['id'] . '">';
                $option .= $char . $item['name'];
                $option .= '</option>';

                // Xóa chuyên mục đã lặp
                unset($categories[$key]);

                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                showCategories($categories, $item['id'], $char . '|--- ', $option);
            }
        }
        return $option;
    }
}
if (!function_exists('set_cache')) {
    function set_cache($key, $value, $minutes = 30)
    {
        $key = '_' . $key;
        if (Cache::has($key)) {
            return Cache::get($key);
        } else {
            Cache::put($key, $value, $minutes);
            return Cache::get($key);
        }
    }
}

if (!function_exists('target_link')) {
    function target_link()
    {
        return array('_blank', '_self', '_parent', '_top');
    }
}

if (!function_exists('config_alepay')) {
    function config_alepay()
    {
        $configAlepay = array(
            "apiKey" => "FtGDdXchfgx85WfxhI4MLgoBywjk5s",
            "encryptKey" => "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtQm4MMQwjpBT7W/3hs3t7E669+iTXyfQaLlpw0Qs7ymMAI6ISn84mNawWuH7seAi099J/nSmV8Ivln2KVEZhpE9/WoMWSY70MWWmwllUMoXea9m29Qg4RIkjaH25SSbVG7rJ24yoChct/wzDyyMUVyOF/C91ojZpY20FuvdvfZQIDAQAB",
            "checksumKey" => "6bbsnIqWE73liywS3HxAl0yyPCsK8O",
            "callbackUrl" => route('payment.result'),
            "env" => "live",
        );

        /*$configAlepay = array(
            "apiKey" => "kz8fXmt3iCvNngaxqkiywhZgkRkCp6",
            "encryptKey" => "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCMwnqarPvrWA1G9Xy2o8MZmxPH/BkAOZmAeTkG3WxZGsqG53IX1qqmzMFIRTBNfLPW4w+BhVksYQRX7gsfe/UynjxkwEZxSL+EwPhsBJ/1mL3HrjT9Yy3FKYFIWqTaXNgvZ2mmn2XqYAbI47ra/FEW0qoz74s+YC8g+N2gtzbMKQIDAQAB",
            "checksumKey" => "kULJVwirQ0olKYY9EEJSbCFKpMuGwf",
            "callbackUrl" => route('payment.result'),
            "env" => "test",
        );*/

        return $configAlepay;
    }
}
if (!function_exists('getValidationError')) {
    function getValidationError($validator)
    {
        $error = $validator->errors();
        $errorMessage = '';
        if (!empty($error)) {
            foreach ($error->all() as $key => $value) {
                $errorMessage .= $value . '</br>';
            }
        }
        $error = $errorMessage;
        return $error;
    }
}
