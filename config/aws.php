<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/12/2018
 * Time: 8:29 AM
 */

return array(
    /*
     * Access Key ID
     *
     * */
    'key'=> env('AWS_ACCESS_KEY_ID', ''),

    /*
     * Secret Access Key
     *
     * */
    'secret'=> env('AWS_SECRET_ACCESS_KEY', ''),

    /*
     * Name Region  `
     *
     * */
    'region'=> env('APP_AWS_REGION', ''),

    /*
     * Name Bucket
     *
     * */
    'bucket'=> env('AWS_BUCKET', ''),

    /*
     * Name Bucket
     *
     * */
    'version'=> env('AWS_VERSION', 'latest'),

    /*
    * Quyền
    *
    * */
    'acl'=> env('AWS_ACL', 'public-read'),
    /*
    * Quyền
    *
    * */
    'profile'=> env('AWS_PROFILE', 'default'),

);