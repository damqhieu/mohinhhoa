<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsPage extends Model
{
    protected $fillable = [
        'title',
        'code_page',
        'slug',
        'img_thumbnail',
        'img_banner',
        'author',
        'overview',
        'description',
        'content',
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
        'locale',
        'is_active',
        'is_new',
        'is_top',
        'is_hot'
    ];

    public function cms_tags()
    {
        return $this->belongsToMany(CmsTag::class);
    }
}
