<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsTestimonial extends Model
{
    protected $fillable = [
        'image',
        'name',
        'position',
        'title',
        'evaluate',
        'locale',
        'is_active',
    ];
}
