<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsFaq extends Model
{
    protected $fillable = [
        'question',
        'answer',
        'locale',
        'is_active'
    ];

}
