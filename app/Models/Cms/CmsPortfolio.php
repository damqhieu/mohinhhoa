<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsPortfolio extends Model
{
    protected $fillable = [
        'cms_category_id',
        'title',
        'image',
        'alt',
        'is_active'
    ];
    public function CmsCategory()
    {
        return $this->belongsTo(CmsCategory::class);
    }
}
