<?php

namespace App\Models\Cms;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CmsSubscribe extends Model
{
    use Notifiable;

    protected $fillable = [
        'email'
    ];
}
