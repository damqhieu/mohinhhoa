<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/27/2018
 * Time: 2:38 PM
 */

namespace App\Models\Cms;


use Illuminate\Database\Eloquent\Model;

class CmsTag  extends Model
{
    protected $fillable = [
        'title',
        'url',
        'locale'
    ];

    public function cms_articles()
    {
        return $this->belongsToMany(CmsArticle::class);
    }


}