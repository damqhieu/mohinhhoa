<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsStatistic extends Model
{
    protected $fillable = [
        'name',
        'value',
        'icon',
        'locale',
        'is_active'
    ];
}
