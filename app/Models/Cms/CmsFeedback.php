<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsFeedback extends Model
{
    const STATUS_PENDING = "pending";
    const STATUS_APPROVE = "approve";

    protected $fillable = [
        'title',
        'name',
        'phone',
        'email',
        'address',
        'content',
        'is_active'
    ];
}
