<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsPartner extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'website',
        'overview',
        'image',
        'locale',
        'is_active'
    ];
}
