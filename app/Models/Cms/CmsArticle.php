<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsArticle extends Model
{
    protected $fillable = [
        'cms_category_id',
        'title',
        'slug',
        'img_thumbnail',
        'img_banner',
        'video',
        'audio',
        'author',
        'published',
        'overview',
        'description',
        'content',
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
        'locale',
        'is_active',
        'is_new',
        'is_top',
        'is_hot'
    ];

    public function CmsCategory()
    {
        return $this->belongsTo(CmsCategory::class);
    }

    public function cms_tags()
    {
        return $this->belongsToMany(CmsTag::class);
    }
}
