<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsSlide extends Model
{
    protected $fillable = [
        'title',
        'image',
        'alt',
        'is_active'
    ];

    public function slide_elements()
    {
        return $this->hasMany(SlideElement::class);
    }
}
