<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsCategory extends Model
{
    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'overview',
        'description',
        'content',
        'object_name',
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
        'locale',
        'is_active',
    ];

    public function scopeActive($query)
    {
        return $query->where('is_active', ACTIVE);
    }

    public function cms_articles()
    {
        return $this->hasMany(CmsArticle::class);
    }

    public function getParentCate()
    {
        return $this->hasOne(CmsCategory::class, 'parent_id', 'id');
    }
}
