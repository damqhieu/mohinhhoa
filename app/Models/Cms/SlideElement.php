<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class SlideElement extends Model
{
    protected $fillable = array(
        'cms_slide_id',
        'title',
        'url',
        'target'
    );
}
