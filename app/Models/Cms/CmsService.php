<?php

namespace App\Models\Cms;

use Illuminate\Database\Eloquent\Model;

class CmsService extends Model
{
    protected $fillable = [
        'cms_category_id',
        'title',
        'slug',
        'icon',
        'img_thumbnail',
        'img_banner',
        'overview',
        'description',
        'content',
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
        'locale',
        'is_active',
        'is_new',
        'is_top',
        'is_hot'
    ];

    public function CmsCategory(){
        return $this->belongsTo(CmsCategory::class);
    }
}
