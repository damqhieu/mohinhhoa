<?php

namespace App\Models\Transaction;

use App\Models\Transport\TransportTrip;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Transport\Transport;

class TransactionBooking extends Model
{
    protected $fillable = [
        'order_code',
        'transaction_code',
        'transport_trip_id',
        'name',
        'email',
        'phone',
        'address',
        'time_departure',
        'flight_number',
        'time_arrival',
        'quantity_adult',
        'quantity_child',
        'departure_date',
        'return_date',
        'booking_type',
        'booking_note',
        'booking_time',
        'payment_method',
        'payment_status',
        'booking_status',
        'is_3d',
        'bank_code',
        'bank_name',
        'bank_hotline',
        'transaction_time',
        'success_time',
        'payer_fee'

    ];

    public static function getAllData(Request $request)
    {
        $columnDataTable = [
            'order_code',
            'transaction_code',
            'transport_trip_id',
            'name',
            'phone',
            'time_departure',
            'time_arrival'
        ];

        $start = $request->start;
        $length = $request->length;
        $order = $columnDataTable[$request->order[0]['column']];
        $dir = $request->order[0]['dir'];

        $recordsTotal = $recordsFiltered = self::when(!empty($request->search['value']), function ($query) use ($request) {
            $query->where('transport_trip_id', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('order_code', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('transaction_code', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('name', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('phone', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('time_departure', 'like', '%' . $request->search['value'] . '%');
            $query->orWhere('time_arrival', 'like', '%' . $request->search['value'] . '%');
        })->count();

        $data = [];
        self::with(['transport_trip' => function ($query) {
            $query->with('departure_place', 'destination_place');
        }])->select(
            'id',
            'order_code',
            'transaction_code',
            'transport_trip_id',
            'name',
            'phone',
            'time_departure',
            'time_arrival')
            ->when(!empty($request->search['value']), function ($query) use ($request) {
                $query->where('transport_trip_id', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('order_code', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('transaction_code', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('name', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('phone', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('time_departure', 'like', '%' . $request->search['value'] . '%');
                $query->orWhere('time_arrival', 'like', '%' . $request->search['value'] . '%');;
            })
            ->orderBy($order, $dir)
            ->offset($start)
            ->take($length)
            ->get()
            ->map(function ($item) use (&$data) {
                $val = [
                    'id' => $item->id,
                    'order_code' => $item->order_code,
                    'transaction_code' => $item->transaction_code,
                    'transport_trip_id' => $item->transport_trip->departure_place->name . " - " . $item->transport_trip->destination_place->name,
                    'name' => $item->name,
                    'phone' => $item->phone,
                    'time_departure' => $item->time_departure,
                    'time_arrival' => $item->time_arrival
                ];
                $data[] = $val;
            });

        $rs = [
            'draw' => intval($request->draw),
            'recordsTotal' => intval($recordsTotal),
            'recordsFiltered' => intval($recordsFiltered),
            'length' => $length,
            'data' => $data
        ];

        return $rs;
    }

    public function transport_trip()
    {
        return $this->belongsTo(TransportTrip::class);
    }

}
