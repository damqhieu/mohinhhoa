<?php

namespace App\Models\Transaction;

use App\Models\Transport\TransportRental;
use Illuminate\Database\Eloquent\Model;

class TransactionBookingRental extends Model
{
    protected $fillable = [
        'transport_rental_id',
        'name',
        'email',
        'phone',
        'address',
        'time_departure',
        'flight_number',
        'time_arrival',
        'booking_time',
        'quantity_adult',
        'quantity_child',
        'departure_date',
        'return_date',
        'booking_note',
        'booking_status',
        'payment_method',
        'payment_status',
    ];

    public function transport_rental()
    {
        return $this->belongsTo(TransportRental::class);
    }
}
