<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $fillable = [
        'id',
        'name',
        'domain',
        'author',
        'email',
        'hotline',
        'address',
        'google_analytics',
        'google_webmaster',
        'google_adsense',
        'google_map',
        'social_share',
        'live_chat',
        'img_logo',
        'img_banner',
        'favicon',
        'style',
        'script',
        'link_facebook',
        'link_google_plus',
        'link_twitter',
        'link_linkedin',
        'link_vimeo',
        'link_skype',
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
    ];

    public static function getPathName() {
        return 'upload/system/' . (new self)->getTable() . DIRECTORY_SEPARATOR;
    }
}
