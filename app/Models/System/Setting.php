<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key',
        'value',
        'locale'
    ];
}
