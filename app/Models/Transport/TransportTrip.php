<?php

namespace App\Models\Transport;

use Illuminate\Database\Eloquent\Model;

class TransportTrip extends Model
{
    protected $fillable = [
        'image',
        'name',
        'departure',
        'destination',
        'price',
        'price_sale',
        'discount',
        'currency',
        // Đa ngữ
        'locale',
        // SEO
        'tag_title',
        'meta_author',
        'meta_keywords',
        'meta_description',
        'meta_canonical',
        'sort_order',
        // Hiển thị
        'is_active',
        'is_new',
        'is_top',
        'is_hot'
    ];

    public function departure_place()
    {
        return $this->belongsTo(TransportPlace::class,'departure','id');
    }
    public function destination_place()
    {
        return $this->belongsTo(TransportPlace::class,'destination','id');
    }
    public function transport_schedule()
    {
        return $this->hasMany(TransportSchedule::class);
    }
}
