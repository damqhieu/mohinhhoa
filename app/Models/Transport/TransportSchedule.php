<?php

namespace App\Models\Transport;

use Illuminate\Database\Eloquent\Model;

class TransportSchedule extends Model
{
    protected $fillable = [
        'transport_trip_id',
        'start_time',
        'end_time',
        'sort_order',
        'description',
        'is_active'
    ];

    public function transport_trip()
    {
        return $this->belongsTo(TransportTrip::class);
    }
}
