<?php

namespace App\Models\Transport;

use Illuminate\Database\Eloquent\Model;

class TransportRental extends Model
{
    protected $fillable = [
        'name',
        'description',
        'price',
        'currency',
        'image'
    ];
}
