<?php

namespace App\Models\Transport;

use Illuminate\Database\Eloquent\Model;

class TransportPlace extends Model
{
    protected $fillable = [
        'name',
        'image',
        'address',
        'latitude',
        'longitude',
        'sort_order',
        'locale',
        'is_active',
        'is_new',
        'is_top',
        'is_hot',
    ];
}
