<?php

namespace App\Listeners;

use App\Events\SendMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendMailFired implements ShouldQueue
{

    use InteractsWithQueue;

    public function handle(SendMail $event)
    {
        $data = $event->cmsFeedback->toArray();
        Mail::send('client.mail.mail-contact-us', [], function($message) use ($data) {
            $message->to($data['email']);
            $message->subject('Thank you for contacting us!');
        });
        Mail::send('client.mail.mail-company', compact('data'), function($message){
            $message->to(env('MAIL_USERNAME_COMPANY'));
            $message->subject('New contact!');
        });
    }
}