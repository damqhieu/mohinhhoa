<?php

namespace App\Providers;

use App\Http\ViewComposers\AboutComposer;
use App\Http\ViewComposers\CmsCategoryComposer;
use App\Http\ViewComposers\GeneralComposer;
use App\Http\ViewComposers\NewArticleComposer;
use App\Http\ViewComposers\PartnerComposer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        /*  Share data
        */
        View::composer(
            [
                'admin.cms-manager.about.create',
                'admin.cms-manager.article.create',
                'admin.cms-manager.contact.create',
                'admin.cms-manager.service.create',
                'admin.cms-manager.portfolio.create',

                'admin.cms-manager.about.edit',
                'admin.cms-manager.article.edit',
                'admin.cms-manager.contact.edit',
                'admin.cms-manager.service.edit',
                'admin.cms-manager.portfolio.edit',

                'client.blog.blog-detail'
            ],
            CmsCategoryComposer::class
        );
        View::composer('layouts.patial.partner',PartnerComposer::class );
        View::composer('*', GeneralComposer::class );
        View::composer([
            'layouts.patial.header',
            'layouts.patial.footer'
        ], AboutComposer::class );
        View::composer([
            'layouts.patial.header',
            'layouts.patial.footer'
        ], NewArticleComposer::class );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
