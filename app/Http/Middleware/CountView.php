<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CountView
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->ajax() || $request->post())
            return $next($request);

        $path = 'views.txt';
        $data = file_get_contents($path);

        $ip = $_SERVER['REMOTE_ADDR'];

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        if (empty(Session::has('ip'))) {
            Session::put('ip', $ip);
            $data++;
        }

        $fp = fopen($path, "w");

        if(is_resource($fp)) {
            fwrite($fp, $data);
            fclose($fp);
        }

        return $next($request);
    }
}
