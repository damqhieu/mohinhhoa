<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/8/2018
 * Time: 11:42 AM
 */

/**
 * PAYPAL API SERVICE TEST
 */

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\PayPalService as PayPalSvc;
use Illuminate\Http\Request;

class PayPalController extends Controller
{

    private $paypalSvc;

    public function __construct(PayPalSvc $paypalSvc)
    {
        $this->paypalSvc = $paypalSvc;
    }

    public function index(Request $request)
    {
        if ($request->post()) {
            $data = [
                [
                    'name' => $request->name,
                    'quantity' => 1,
                    'date' => '22/1/1994',
                    'price' => 1.5,
                    'sku' => '1'
                ]
            ];

            $transactionDescription = "Tobaco";

            $paypalCheckoutUrl = $this->paypalSvc
//             ->setCurrency('usd')
                ->setReturnUrl(route('paypal.status'))
                ->setItem($data)
                ->createPayment($transactionDescription);

            if ($paypalCheckoutUrl) {
                return redirect($paypalCheckoutUrl);
            } else {
                abort(404);
            }
        } else {
            return back();
        }
    }

    public function status()
    {
        $paymentStatus = $this->paypalSvc->getPaymentStatus();
        dd($paymentStatus);
    }

    public function paymentList()
    {
        $limit = 10;
        $offset = 0;

        $paymentList = $this->paypalSvc->getPaymentList($limit, $offset);

        dd($paymentList);
    }

    public function paymentDetail($paymentId)
    {
        $paymentDetails = $this->paypalSvc->getPaymentDetails($paymentId);

        dd($paymentDetails);
    }
}