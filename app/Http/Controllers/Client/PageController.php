<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 6/19/2018
 * Time: 2:28 PM
 */

namespace App\Http\Controllers\Client;

use App\Models\Cms\CmsAbout;
use App\Models\Cms\CmsFaq;
use App\Models\Cms\CmsPage;
use App\Models\Cms\CmsPortfolio;
use App\Models\Cms\CmsCategory;
use App\Models\Cms\CmsSlide;
use App\Models\Cms\CmsService;
use App\Models\Cms\CmsArticle;
use App\Models\Cms\CmsTestimonial;
use App\Models\Transport\TransportRental;
use App\Models\Transport\TransportTrip;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;


class PageController extends Controller
{
    public function index()
    {
        $cmsSlide = set_cache('cms_slide', CmsSlide::where('is_active', ACTIVE)->get(), 45);
        $services = set_cache('services', CmsService::where('is_active', ACTIVE)->orWhere('is_new', ACTIVE)->take(3)->get(), 45);
        $articles = set_cache('articles_index', CmsArticle::where('is_active', ACTIVE)->where('is_new', ACTIVE)->take(2)->get(), 45);
        $trips = set_cache('trips', TransportTrip::where('is_active', ACTIVE)
            ->orWhere('is_hot', ACTIVE)
            ->orWhere('is_top', ACTIVE)
            ->orWhere('is_new', ACTIVE)
            ->orderBy('is_hot', 'is_top', 'is_new')
            ->get(), 45);
        return view('client.index', compact('trips', 'cmsSlide', 'services', 'articles'));
    }

    public function about()
    {
        $abouts = CmsAbout::where('is_active', ACTIVE)->get();
        $page = set_cache('page', CmsPage::where('code_page', 'about')->where('is_active', ACTIVE)->first(), 45);
        $cmsTestimonial = set_cache('cms_testimonial', CmsTestimonial::where('is_active', ACTIVE)->get(), 45);

        return view('client.about-us', compact('page', 'cmsTestimonial','abouts'));
    }

    public function contact()
    {
        return view('client.contact-us');
    }

    public function portfolio()
    {
        $cmsPortfolio = set_cache('cms_portfolio', CmsPortfolio::with('CmsCategory')->where('is_active', ACTIVE)->get(), 45);
        $nameCategory = set_cache('name_category', CmsCategory::where('object_name', 'cms_portfolios')->where('is_active', ACTIVE)->get(), 45);

        return view('client.portfolio', compact('cmsPortfolio', 'nameCategory'));
    }

    public function busSchedule()
    {
        $trips = TransportTrip::with('transport_schedule')->get();
        return view('client.bus-schedule', compact('trips'));
    }
    public function booking($name = null)
    {
        $trips = TransportTrip::all();
        return view('client.booking', compact('trips','name'));
    }

    public function carasiaRental()
    {
        $rentals = TransportRental::all();
        return view('client.carasia-rental',compact('rentals'));
    }

    public function faq()
    {
        $faqs = CmsFaq::select('question','answer')->where('is_active',ACTIVE)->get();
        return view('client.faqs',compact('faqs'));
    }
    public function term()
    {
        $page = CmsPage::where('code_page', 'terms_and_conditions')->where('is_active', ACTIVE)->first();
        return view('client.term',compact('page'));
    }

}