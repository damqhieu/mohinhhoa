<?php

namespace App\Http\Controllers\Client;

use App\Lib\Alepays\Alepay;
use App\Lib\Alepays\Utils\AlepayUtils;
use App\Models\Transaction\TransactionBooking;
use App\Models\Transport\TransportTrip;
use App\Rules\Utf8StringRule;
use Endroid\QrCode\QrCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AlepayController extends Controller
{
    /*
     * Hàm này sẽ nhận thông tin của form booking truyền sang
     * Các thông tin là: idTrip, time(2 days), số người (2 type), type (onway || return)
     * */
    /*$data = [
            'type' => $request->type,
            'trip_id' => $request->trip_id,
            'departure_date' => $request->departure_date,
            'return_date' => $request->return_date,
            'quantity_adult' => $request->quantity_adult,
            'quantity_child' => $request->quantity_child,
        ];

        $request->session()->put('data_booking', $data);*/

    public function handleBooking(Request $request)
    {
        if (isset($request->type)) {
            if ($request->type === 'return') {

                $error = $this->validateForm($request);
                if (!empty($error)) {
                    return back()->with('error', $error);
                }
                $data = [
                    'type' => $request->type,
                    'trip_id' => $request->trip_id,
                    'departure_date' => $request->departure_date,
                    'return_date' => $request->return_date,
                    'quantity_adult' => $request->quantity_adult,
                    'quantity_child' => $request->quantity_child,
                ];

                $request->session()->put('data_booking', $data);
            } elseif ($request->type === 'oneway') {

                $error = $this->validateForm($request);
                if (!empty($error)) {
                    return back()->with('error', $error);
                }

                $data = [
                    'type' => $request->type,
                    'trip_id' => $request->trip_id,
                    'departure_date' => $request->departure_date,
                    'quantity_adult' => $request->quantity_adult,
                    'quantity_child' => $request->quantity_child,
                ];

                $request->session()->put('data_booking', $data);
            }

            return redirect()->route('payment.checkout-step1');
        } else {
            $error = 'Missing parameter';
            return $error;
        }
    }

    public function validateForm(Request $request)
    {
        if ($request->type === 'return') {
            $validator = Validator::make($request->all(), [
                'type' => ['required', Rule::in(['return', 'oneway'])],
                'trip_id' => 'required|integer',
                'departure_date' => 'required|date_format:"m/d/Y"',
                'return_date' => 'required|date_format:"m/d/Y"',
                'quantity_adult' => 'required|integer|max:100',
                'quantity_child' => 'nullable|integer|max:100'
            ], [
                'departure_date.required' => 'Departure date is not empty',
                'departure_date.date_format' => 'Departure date is not format: m/d/Y',
                'return_date.required' => 'Return date is not empty',
                'return_date.date_format' => 'Return date is not format: m/d/Y',
                'quantity_adult.max' => 'Quantity adult is max 100',
                'quantity_child.max' => 'Quantity child is max 100'
            ]);

            if ($validator->fails()) {
                $error = getValidationError($validator);
                return $error;
            }
            return null;
        } elseif ($request->type === 'oneway') {
            $validator = Validator::make($request->all(), [
                'type' => ['required', Rule::in(['return', 'oneway'])],
                'trip_id' => 'required|integer',
                'departure_date' => 'required|date_format:"m/d/Y"',
                'quantity_adult' => 'required|integer|max:100',
                'quantity_child' => 'required|integer|max:100'
            ], [
                'departure_date.required' => 'Departure date is not empty',
                'departure_date.date_format' => 'Departure date is not format: m/d/Y',
                'quantity_adult.max' => 'Quantity adult is max 100',
                'quantity_child.max' => 'Quantity child is max 100'
            ]);

            if ($validator->fails()) {
                $error = getValidationError($validator);
                return $error;
            }

        }
    }

    public function checkoutStep1(Request $request)
    {
        if (!Session::has('data_booking')) {
            \session()->flash('error', 'Please select previous jump');
            return redirect(route('booking'));
        }
        if ($request->post()) {
            $validator = Validator::make($request->all(), [
                'buyerName' => ['required', 'max:100', new Utf8StringRule()],
                'buyerEmail' => ['required', 'max:100', 'email'],
                'buyerPhone' => ['required', 'max:15'],
                'buyerCity' => ['required', 'max:100', new Utf8StringRule()],
                'buyerCountry' => ['required', 'max:100', new Utf8StringRule()],
                'buyerAddress' => ['required', 'max:100', new Utf8StringRule()],
                'flightNumber' => ['required', 'max:50', new Utf8StringRule()],
                'timeDeparture' => ['required', 'date_format:"H:i"', new Utf8StringRule()],
                'timeArrival' => ['required', 'date_format:"H:i"', new Utf8StringRule()],
                'note' => ['required', 'max:1000', new Utf8StringRule()],
            ]);
            if ($validator->fails()) {
                $error = getValidationError($validator);
                return back()->with('error', $error);
            }

            $dataBooking = Session::get('data_booking');
            $trip = TransportTrip::findOrFail($dataBooking['trip_id']);
            $quantityAdult = $dataBooking['quantity_adult'];
            $quantityChild = $dataBooking['quantity_child'];

            $dataBooking['totalItem'] = $quantityAdult + $quantityChild;
            $dataBooking['amount'] = $this->intoMoney($trip, $quantityAdult);
            $dataBooking['trip'] = $trip;
            $dataBooking['infoPerson'] = $request->all();

            /*
             * Lưu dữ liệu vào transaction booking
             * */
            $orderCode = strtoupper(md5(time() . 'Tong-Van-Duc'));

            $transactionBooking = new TransactionBooking();
            $transactionBooking->order_code = $orderCode;
            $transactionBooking->transport_trip_id = $dataBooking['trip_id'];
            $transactionBooking->name = $request->buyerName;
            $transactionBooking->email = $request->buyerEmail;
            $transactionBooking->phone = $request->buyerPhone;
            $transactionBooking->address = $request->buyerAddress;
            $transactionBooking->flight_number = $request->flightNumber;
            $transactionBooking->time_departure = $request->timeDeparture;
            $transactionBooking->time_arrival = $request->timeArrival;
            $transactionBooking->quantity_adult = $quantityAdult;
            $transactionBooking->quantity_child = $quantityChild;
            $transactionBooking->departure_date = date('Y-m-d', strtotime($dataBooking['departure_date']));
            $transactionBooking->return_date = isset($dataBooking['return_date']) ? date('Y-m-d', strtotime($dataBooking['return_date'])) : null;
            $transactionBooking->booking_type = (isset($dataBooking['return_date'])) ? 'return' : 'oneway';
            $transactionBooking->booking_note = $request->note;
            $transactionBooking->booking_time = time();
            $transactionBooking->payment_status = 'pending';
            $transactionBooking->booking_status = 'pending';
            $transactionBooking->save();

            $dataBooking['order_code'] = $orderCode;

            Session::put('data_booking', $dataBooking);

            return redirect(route('payment.checkout-step2'));
        }
        if (Session::has('data_booking') && isset(Session::get('data_booking')['infoPerson'])) {
            $dataBooking = Session::get('data_booking');
            return view('client.checkout.checkout-step1', compact('dataBooking'));
        }
        return view('client.checkout.checkout-step1');
    }

    public function checkoutStep2()
    {
        $dataBooking = Session::get('data_booking');

        return view('client.checkout.checkout-step2', compact('dataBooking'));
    }

    public function emptyBooking()
    {
        Session::forget('data_booking');
        return redirect(route('booking'));
    }

    public function handlePayment()
    {
        if (!Session::has('data_booking')) {
            \session()->flash('error', 'Please select previous jump');
            return redirect(route('booking'));
        }

        $dataBooking = Session::get('data_booking');

        $config = config_alepay();
        $alepay = new Alepay($config);
        $data = [];

        $data['cancelUrl'] = route('payment.empty-booking');
        $data['amount'] = $dataBooking['amount'];
        $data['orderCode'] = $dataBooking['order_code'];
        $data['currency'] = $dataBooking['trip']->currency ?? 'USD';
        $data['orderDescription'] = $dataBooking['infoPerson']['note'];
        $data['totalItem'] = $dataBooking['totalItem'];
        $data['checkoutType'] = 1; // 0 : cho phép thanh toán bằng cả 2 cách, 1 : chỉ thanh toán thường , 2: chỉ thanh toán trả góp
        $data['buyerName'] = $dataBooking['infoPerson']['buyerName'];
        $data['buyerEmail'] = $dataBooking['infoPerson']['buyerEmail'];
        $data['buyerPhone'] = $dataBooking['infoPerson']['buyerPhone'];
        $data['buyerAddress'] = $dataBooking['infoPerson']['buyerAddress'];
        $data['buyerCity'] = $dataBooking['infoPerson']['buyerCity'];
        $data['buyerCountry'] = $dataBooking['infoPerson']['buyerCountry'];
        $data['month'] = 0;
        $data['paymentHours'] = 12; // 12 tiếng :  Thời gian cho phép thanh toán (tính bằng giờ)

        $result = $alepay->sendOrderToAlepay($data); // Khởi tạo

        if (isset($result) && !empty($result->checkoutUrl)) {
            return redirect($result->checkoutUrl);
        } else {
            return back()->with("error", $result->errorDescription);
        }
    }

    public function intoMoney($trip, $quantityAdult)
    {
        if (empty($trip->price_sale)) {
            $total = $trip->price * $quantityAdult;
        } else {
            $total = $trip->price_sale * $quantityAdult;
        }

        if (!empty($trip->discount)) {
            $total *= ($trip->discount / 100);
        }

        return $total;
    }

    public function result(Request $request)
    {
        $config = config_alepay();
        $encryptKey = $config['encryptKey'];
        $alepay = new Alepay($config);
        $utils = new AlepayUtils();
        $result = $utils->decryptCallbackData($request->data, $encryptKey);
        $obj_data = json_decode($result);

        if (!$obj_data->cancel) {

            $info = json_decode($alepay->getTransactionInfo($obj_data->data));

            $transactionBooking = TransactionBooking::where('order_code', $info->orderCode)
                ->where('transaction_code', $info->transactionCode)
                ->first();

            if ($transactionBooking) {
                return redirect()->route('payment.booking-detail', [
                    'order_code' => $transactionBooking->order_code,
                    'transaction_code' => $transactionBooking->transaction_code
                ]);
            }

            $transactionBooking = TransactionBooking::where('order_code', $info->orderCode)->first();
            $transactionBooking->transaction_code = $info->transactionCode;
            $transactionBooking->is_3d = $info->is3D;
            $transactionBooking->bank_code = $info->bankCode;
            $transactionBooking->bank_name = $info->bankName;
            $transactionBooking->bank_hotline = $info->is3D;
            $transactionBooking->payment_method = $info->method;
            $transactionBooking->payment_status = 'done';
            $transactionBooking->transaction_time = $info->transactionTime;
            $transactionBooking->success_time = $info->successTime;
            $transactionBooking->payer_fee = $info->payerFee;
            $transactionBooking->booking_status = 'done';
            $transactionBooking->save();

            //tạo qr code
            $nameImage = md5($info->orderCode) . '.png';
            $qrCode = new QrCode(route('payment.booking-detail', [
                'order_code' => $info->orderCode,
                'transaction_code' => $info->transactionCode
            ]));
            $qrCode->setSize(200);
            $qrCode->writeFile('qrcode/' . $nameImage);
            $imgQRCode = 'qrcode/' . $nameImage;

            $dataSendMail = [
                'transaction_code' => $transactionBooking->transaction_code,
                'order_code' => $transactionBooking->order_code,
                'name' => $transactionBooking->name,
                'email' => $transactionBooking->email,
                'phone' => $transactionBooking->phone,
                'address' => $transactionBooking->address,
                'time_departure' => $transactionBooking->time_departure,
                'time_arrival' => $transactionBooking->time_arrival,
                'flight_number' => $transactionBooking->flight_number,
                'quantity_adult' => $transactionBooking->quantity_adult,
                'quantity_child' => $transactionBooking->quantity_child,
                'departure_date' => $transactionBooking->departure_date,
                'return_date' => $transactionBooking->return_date,
                'booking_type' => $transactionBooking->booking_type,
                'booking_status' => $transactionBooking->booking_status,
                'booking_note' => $transactionBooking->booking_note,
                'payment_method' => $transactionBooking->payment_method,
                'payment_status' => $transactionBooking->payment_status,
                'booking_detail' => route('payment.booking-detail', [
                    'order_code' => $transactionBooking->order_code,
                    'transaction_code' => $transactionBooking->transaction_code
                ])
            ];

            Mail::send('client.mail.mail-booking', $dataSendMail, function ($message) use ($transactionBooking) {
                $message->to($transactionBooking->email);
                $message->subject('Car - Asia RESERVATION #' . $transactionBooking->transaction_code);
            });

            Mail::send('client.mail.mail-booking', $dataSendMail, function ($message) use ($transactionBooking) {
                $message->to(env('MAIL_USERNAME_COMPANY'));
                $message->subject('Có khách hàng đặt vé - #' . $transactionBooking->transaction_code);
            });

            Session::forget('data_booking');

            return view('client.alepay-result', compact('imgQRCode', 'transactionBooking'));
        }

        return redirect()->route('home');
    }

    public function bookingDetail($order_code, $transaction_code)
    {
        $transactionBooking = TransactionBooking::where('order_code', $order_code)
            ->where('transaction_code', $transaction_code)
            ->first();

        if (!$transactionBooking) {
            abort(404);
        }
        $nameImage = md5($transactionBooking->order_code) . '.png';
        $imgQRCode = 'qrcode/' . $nameImage;

        return view('client.booking-detail', compact('transactionBooking', 'imgQRCode'));
    }
}
