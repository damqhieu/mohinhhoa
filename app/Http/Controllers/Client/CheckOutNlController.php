<?php

namespace App\Http\Controllers\Client;

//header("Content-Type: image/png");
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Lib\NL_CheckOutV3;

use Endroid\QrCode\QrCode;


class CheckOutNlController extends Controller
{
    public function Checkout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'no' => 'required|integer|min:1'

        ]);
//
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $booking = new TransportBooking();
        $price_depart = TransportTrip::active()->select('price')->where('id', $request->departure_trip_id)->first();

        if(isset($request->ret_date)){
            $price_return =TransportTrip::active()->select('price')->where('id',$request->return_trip_id)->first();
            $booking->price = ($price_depart->price + $price_return->price - DISCOUNT*2)*$request->no;
        }else{
            $booking->price = $price_depart->price*$request->no;
        }
        $booking->name = $request->name;
        $booking->email = $request->email;
        $booking->phone = $request->phone;
        $booking->address = $request->address;
        $booking->quantity = $request->no;
        $booking->departure_trip_id = $request->departure_trip_id;
        $booking->return_trip_id = isset($request->return_trip_id) ? $request->return_trip_id : NULL;
        $booking->departure_date = $request->dep_date;
        $booking->return_date = $request->ret_date;
        $booking->booking_type = isset($request->ret_date) ? 'return' : 'oneway';
        $booking->booking_note = $request->note;
        $booking->transaction_id = '';
        $booking->payment_method = 'online';
        $booking->payment_status = 'pending';
        $booking->booking_status = 'pending';
        $booking->application_id = env('APPLICATION_ID');
        $booking->created_date = date("Y-m-d");
        $flag = $booking->save();

        if ($flag) {
            $request->session()->flash('delete', 'delete');
        }
        $nlcheckout = new NL_CheckOutV3(env('NL_MERCHANT_ID'), env('NL_MERCHANT_PASS'), env('RECEIVER'), env('URL_API'));
        $total_amount = $booking->price;
        $array_items[0] = array('item_name1' => 'aa',
            'item_quantity1' => 5,
            'item_amount1' => $total_amount
        );

        $array_items = array();
//        $payment_method = 'VISA';
        $bank_code = $request->bankcode;
        $order_code = $booking->id;

        $payment_type = '';
        $lang_code = 'en';
        $discount_amount = 0;
        $order_description = '';
        $tax_amount = 0;
        $fee_shipping = 0;
        $return_url = route('payment-success');
        $cancel_url = route('cancel-url', $booking->id);

        $buyer_fullname = $booking->name;
        $buyer_email = $booking->email;
        $buyer_mobile = $booking->phone;
        $buyer_address = $booking->address;
        $nl_result = $nlcheckout->VisaCheckout($order_code, $total_amount, $payment_type, $order_description, $tax_amount,
            $fee_shipping, $discount_amount, $return_url, $cancel_url, $buyer_fullname, $buyer_email, $buyer_mobile,
            $buyer_address, $array_items, $bank_code, $lang_code);
        session()->put('bankcode', $request->bankcode);
        return redirect($nl_result->checkout_url);


    }

    public function paymentSuccess(Request $request)
    {
        $detail = (new NL_CheckOutV3(env('NL_MERCHANT_ID'), env('NL_MERCHANT_PASS'), env('RECEIVER'), env('URL_API')))->GetTransactionDetail($request->token);
        //lưu transport booking
        $tranposortBooking = TransportBooking::find($request->order_code);
        $tranposortBooking->transaction_id = $detail->transaction_id;
        $tranposortBooking->payment_status = 'paid';
        $tranposortBooking->booking_status = 'done';
        $tranposortBooking->save();
        //tạo app_user_transaction
        $ap_user_transaction = new AppUserTransaction();
        $ap_user_transaction->transaction_id = $detail->transaction_id;
        $ap_user_transaction->transaction_status = 1;
        $ap_user_transaction->currency = env('CURRENCY');
        $ap_user_transaction->amount = $tranposortBooking->price;
        $ap_user_transaction->payment_method = 'online';
        $ap_user_transaction->type = session('bankcode');
        //$ap_user_transaction->checksum='';
        $ap_user_transaction->note = $tranposortBooking->book;
        $ap_user_transaction->status = 'APPROVED';
        $ap_user_transaction->is_active = 1;
        $ap_user_transaction->created_date = date("Y-m-d");
        $ap_user_transaction->save();
        session()->forget('bankcode');
        //tạo qr code
        $qrCode = new QrCode(route('booking-detail-qr', $tranposortBooking->id));
        $qrCode->setSize(200);
        $qrCode->writeFile('client2/images/qrcode/' . $tranposortBooking->id . '.png');

        $pickup_id = TransportTrip::with('DestinationPickup')->active()->where('application_id', env('APPLICATION_ID'))->where('id', $tranposortBooking->departure_trip_id)->first();
        $pickup_id = $pickup_id->DestinationPickup->name;
        $dropoff_id = TransportTrip::with('DestinationDropoff')->active()->where('application_id', env('APPLICATION_ID'))->where('id', $tranposortBooking->departure_trip_id)->first();
        $dropoff_id = $dropoff_id->DestinationDropoff->name;
        $data = ['name' => $tranposortBooking->name,
            'email' => $tranposortBooking->email,
            'transaction_id' => $tranposortBooking->transaction_id,
            'price' => $tranposortBooking->price,
            'booking_quantity' => $tranposortBooking->quantity,
            'booking_id' => $tranposortBooking->id,
            'booking_type' => $tranposortBooking->booking_type,
            'departure_date' => $tranposortBooking->departure_date,
            'return_date' => $tranposortBooking->return_date,
            'pickup_id' => $pickup_id,
            'dropoff_id' => $dropoff_id

        ];

        mail::send('client2.email.email_success', $data, function ($mess) use ($data) {
            $mess->from(env('MAIL_USERNAME'), 'Car-asia.com.vn');
            $mess->to($data['email'])->subject('dặt xe tại asia-car');
        });

        unlink(public_path('client2/images/qrcode/' . $tranposortBooking->id . '.png'));
        $request->session()->flash('success', 'success');
        return redirect()->route('checkout-success', $tranposortBooking->id);

    }

    public function cancelUrl($id, Request $request)
    {
        if ($request->session()->has('delete')) {
            $model = TransportBooking::findOrfail($id);
            $flag = $model->delete();
            if (isset($flag)) {
                return view('client2.cancel-url')->with('msg', 'huy giao dich thanh cong');
            }
        }

    }

    public function bookingQrCode($order_code)
    {
        $booking = TransportBooking::where('order_code', $order_code)->first();
        if ($booking) {
            $pickup_id = TransportTrip::with('DestinationPickup')->where('application_id', env('APPLICATION_ID'))->active()->where('id', $booking->departure_trip_id)->first();
            $pickup_id = $pickup_id->DestinationPickup->name;
            $dropoff_id = TransportTrip::with('DestinationDropoff')->where('application_id', env('APPLICATION_ID'))->active()->where('id', $booking->departure_trip_id)->first();
            $dropoff_id = $dropoff_id->DestinationDropoff->name;
            return view('client2.components.qr_code_detail', compact('booking', 'pickup_id', 'dropoff_id'));
        } else {
            abort(404);
        }
    }

    public function checkoutSuccess($id, Request $request)
    {
        if (Session::has('success')) {
            $booking = TransportBooking::findOrfail($id);
            $pickup_id = TransportTrip::with('DestinationPickup')->where('application_id', env('APPLICATION_ID'))->active()->where('id', $booking->departure_trip_id)->first();
            $pickup_id = $pickup_id->DestinationPickup->name;
            $dropoff_id = TransportTrip::with('DestinationDropoff')->where('application_id', env('APPLICATION_ID'))->active()->where('id', $booking->departure_trip_id)->first();
            $dropoff_id = $dropoff_id->DestinationDropoff->name;
            return view('client2.checkout_success', compact('booking', 'pickup_id', 'dropoff_id'));
        } else {
            return redirect()->route('home');
        }
    }
}
