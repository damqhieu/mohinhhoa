<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cms\CmsSubscribe;
use App\Notifications\SendMailSubscribeNotification;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:191|unique:cms_subscribes',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)->with('error', 'Error!')
                ->withInput();
        }
        $model = new CmsSubscribe();

        $model->fill($request->all());


        if ($model->save()) {
            $model->notify(new SendMailSubscribeNotification($model));
            return back()->with('success', 'Subscribe Successfully!');
        }
        return back()->with('error', 'Error!');

    }
}




