<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cms\CmsArticle;
use App\Models\Cms\CmsCategory;
use Illuminate\Support\Facades\Cache;

class BlogController extends Controller
{
    public function blog(Request $request)
    {
        $page = $request->page ?? 1;

        $articles = set_cache('articles_blog_' . $page, CmsArticle::where('is_active', ACTIVE)->paginate(3));

        return view('client.blog.blog-list', compact('articles'));
    }

    public function blogDetail($slug)
    {
        $value = CmsArticle::with('cms_tags')
            ->where('is_active', 1)
            ->where('slug', $slug)
            ->first();

        $article = set_cache('article_blog_detail', $value);

        $tags = $value->cms_tags;

        $articleLast = set_cache('articles_lastest', CmsArticle::where('is_active', ACTIVE)
            ->where('is_new', 1)
            ->take(3)
            ->get(), 10);

        return view('client.blog.blog-detail', compact('article', 'articleLast', 'tags'));
    }

    public function blogCategory($slug)
    {
        $category = set_cache('category_blog_category', CmsCategory::where('is_active', ACTIVE)
            ->where('slug', $slug)
            ->first(), 10);

        $articles = set_cache('articles_blog_category', CmsArticle::where('is_active', ACTIVE)
            ->where('cms_category_id', $category->id)
            ->paginate(3), 10);

        return view('client.blog.blog-list', compact('articles', 'category'));
    }
}
