<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsStatistic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsStatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = "Statistics";
        $statistics = CmsStatistic::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.statistic.index', compact('titlePage', 'statistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Create Statistics";
        return view('admin.cms-manager.statistic.create', compact('titlePage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:cms_statistics|max:191',
            'icon' => 'nullable|max:191',
            'value' => 'nullable|integer|min:1|max:1000000',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsStatistic();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-statistics.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsStatistic = CmsStatistic::findOrFail($id);
        $titlePage = "Create Statistics";
        return view('admin.cms-manager.statistic.edit', compact('titlePage', 'cmsStatistic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsStatistic::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_statistics,name,' ,
            'icon' => 'nullable|max:191',
            'value' => 'nullable|integer|min:1|max:1000000',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CmsStatistic::find($id);

        $msg = $model->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
