<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsPortfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsPortfolioController extends Controller
{
    public function index()
    {
        $titlePage = 'Portfolio';
        $titlePageSmall = 'Portfolio';
        $cmsPortfolios = CmsPortfolio::with('CmsCategory')->orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.portfolio.index', compact('cmsPortfolios', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new portfolio';
        $titlePageSmall = 'Create new portfolio';
        return view('admin.cms-manager.portfolio.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'cms_category_id' => 'required|numeric',
            'title' => 'required|max:191',
            'alt' => 'max:191',
            'image' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('cms-portfolios.create')
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsPortfolio();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-portfolios.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsPortfolio = CmsPortfolio::findOrFail($id);
        $titlePage = 'Edit portfolio: ';
        $titlePageSmall = 'Edit portfolio';

        return view('admin.cms-manager.portfolio.edit', compact('cmsPortfolio', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsPortfolio::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'cms_category_id' => 'required|numeric',
            'title' => 'required|max:191',
            'alt' => 'max:191',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-portfolios.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            if (file_exists($model->image))
                unlink(public_path($model->image));
        }

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-portfolios.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsPortfolio = CmsPortfolio::findOrFail($id);

        $msg = $cmsPortfolio->delete();

        if ($msg) {

            if (file_exists($cmsPortfolio->image))
                unlink(public_path($cmsPortfolio->image));

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
