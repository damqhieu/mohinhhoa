<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsArticle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Article';
        $titlePageSmall = 'Article';
        $articles = CmsArticle::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.article.index', compact('articles', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new article';
        $titlePageSmall = 'Create new article';
        return view('admin.cms-manager.article.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_articles|max:185',
            'slug' => 'required|unique:cms_articles|max:191',
            'author' => 'required|max:191',
            'cms_category_id' => 'required|numeric',
            'locale' => 'required|min:2|max:3',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'content' => 'max:5000',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'audio' => 'nullable|file',
            'video' => 'max:191',

        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-articles.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsArticle();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;


        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }
        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }
        if ($request->hasFile('audio')) {

            $image = $request->file('audio');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/audio', $imageName);
            //Lưu DB
            $model->audio = 'upload/' . $model->getTa0ble() . '/audio/' . $imageName;
        }

        $flag = $model->save();

        $model->cms_tags()->sync($request->tags);

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-articles.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsArticle = CmsArticle::findOrFail($id);
        $titlePage = 'Edit article: ' . str_limit($cmsArticle->title, 35);
        $titlePageSmall = 'Edit article';

        return view('admin.cms-manager.article.edit', compact('cmsArticle', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();
        $model = CmsArticle::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:185|unique:cms_pages,title,'. $id,
            'slug' => 'required|max:191|unique:cms_pages,slug,'. $id,
            'author' => 'required|max:191',
            'cms_category_id' => 'required|numeric',
            'locale' => 'required|min:2|max:3',
            'tag_title' => 'max:191',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'content' => 'max:5000',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'audio' => 'nullable|file',
            'video' => 'max:191'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-articles.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('img_thumbnail')) {
            if (file_exists($model->img_thumbnail))
                unlink(public_path($model->img_thumbnail));
        }

        if ($request->hasFile('img_banner')) {
            if (file_exists($model->img_banner))
                unlink(public_path($model->img_banner));
        }

        if ($request->hasFile('audio')) {
            if (file_exists($model->audio))
                unlink(public_path($model->audio));
        }

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;

        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }
        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }
        if ($request->hasFile('audio')) {

            $image = $request->file('audio');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/audio', $imageName);
            //Lưu DB
            $model->audio = 'upload/' . $model->getTable() . '/audio/' . $imageName;
        }

        $flag = $model->save();

        $model->cms_tags()->sync($request->tags);

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('cms-articles.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CmsArticle::find($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->img_thumbnail))
            unlink(public_path($model->img_thumbnail));

            if (file_exists($model->img_banner))
                unlink(public_path($model->img_banner));

            if (file_exists($model->audio))
            unlink(public_path($model->audio));
        
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
