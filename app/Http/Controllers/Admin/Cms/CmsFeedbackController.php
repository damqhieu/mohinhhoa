<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsFeedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsFeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Feedback';
        $titlePageSmall = 'Feedback';
        $cmsFeebacks = CmsFeedback::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.feedback.index', compact('cmsFeebacks', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsFeeback = CmsFeedback::findOrFail($id);
        $titlePage = 'Edit feedback';
        $titlePageSmall = 'Edit feedback';

        return view('admin.cms-manager.feedback.edit', compact('cmsFeeback', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();
        $model = CmsFeedback::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_feedbacks,name,'. $id,
            'status' => 'required',
            'phone' => 'max:12',
            'email' => 'nullable|email|max:191',
            'address' => 'max:191',
            'content' => 'max:2000',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-feedbacks.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-feedbacks.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsFeeback = CmsFeedback::findOrFail($id);

        $msg = $cmsFeeback->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
