<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Category';
        $titlePageSmall = 'Category';
        $cmsCategories = CmsCategory::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.category.index', compact('cmsCategories', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new category';
        $titlePageSmall = 'Create new category';
        $listCategories = CmsCategory::get()->toArray();
        return view('admin.cms-manager.category.create', compact('titlePage', 'titlePageSmall', 'listCategories'));
    }

    public function getCateByObjectName(Request $request)
    {
        if ($request->ajax()) {

            $object_name = $request->object_name;
            $action      = $request->action ?? '';
            $id          = $request->id     ?? '';

            $listCate = null;

            if ($action == 'edit' && !empty($id)) {
                $listCate = CmsCategory::active()
                    ->where('object_name',$object_name)
                    ->where('id', '<>', $id)
                    ->get()->toArray();
            } else {
                $listCate = CmsCategory::active()->where('object_name',$object_name)->get()->toArray();
            }

            $rs = "<option class='choose' value='0'>please choose category</option>";
            $rs .= showCategories($listCate);

            return response()->json([
                'status' => 200,
                'msg' => 'Request successfully!',
                'option' => $rs,

            ]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185|unique:cms_categories',
            'slug' => 'required|max:191|unique:cms_categories',
            'locale' => 'required|min:2|max:3',
            'object_name' => 'max:191',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'content' => 'max:2000',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-categories.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsCategory();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsCategory = CmsCategory::findOrFail($id);
        $titlePage = 'Edit category: ' . str_limit($cmsCategory->name, 35);
        $titlePageSmall = 'Edit category';
        $listCategories = CmsCategory::get()->toArray();
        return view('admin.cms-manager.category.edit', compact('listCategories', 'cmsCategory', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsCategory::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185|unique:cms_categories,name,'. $id,
            'slug' => 'required|max:191|unique:cms_categories,slug,'. $id,
            'locale' => 'required|min:2|max:3',
            'object_name' => 'max:191',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'content' => 'max:2000',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-categories.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('cms-categories.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CmsCategory::find($id);

        $msg = $model->delete();
        if ($msg) {

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
