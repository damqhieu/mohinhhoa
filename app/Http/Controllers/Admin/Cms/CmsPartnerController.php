<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsPartner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsPartnerController extends Controller
{
    public function index()
    {
        $titlePage = 'Cms Partner';
        $partners = CmsPartner::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.partner.index', compact('titlePage', 'partners'));
    }

    public function create()
    {
        $titlePage = 'Create Cms Partner';
        return view('admin.cms-manager.partner.create', compact('titlePage'));
    }

    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_partners',
            'email' => 'nullable|max:191|email',
            'phone' => 'nullable|max:191',
            'address' => 'nullable|max:191',
            'website' => 'nullable|max:191',
            'overview' => 'nullable|max:2000',
            'locale' => 'required|max:191',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsPartner();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-partners.index'));
    }

    public function edit($id)
    {
        $cmsPartner = CmsPartner::findOrFail($id);
        $titlePage = 'Edit: ' . str_limit($cmsPartner->name, 35);
        return view('admin.cms-manager.partner.edit', compact('cmsPartner', 'titlePage'));
    }

    public function update(Request $request, $id)
    {
        Cache::flush();

        $cmsPartner = CmsPartner::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_partners,name,' . $id,
            'email' => 'nullable|max:191|email',
            'phone' => 'nullable|max:191',
            'address' => 'nullable|max:191',
            'website' => 'nullable|max:191',
            'overview' => 'nullable|max:2000',
            'locale' => 'required|max:191',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            if (file_exists($cmsPartner->image))
                unlink(public_path($cmsPartner->image));
        }

        $cmsPartner->fill($request->all());

        $cmsPartner->is_active = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $cmsPartner->getTable() . '/image', $imageName);
            //Lưu DB
            $cmsPartner->image = 'upload/' . $cmsPartner->getTable() . '/image/' . $imageName;
        }

        $flag = $cmsPartner->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }

    public function destroy($id)
    {
        $model = CmsPartner::find($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->image ))
                unlink(public_path($model->image ));

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
