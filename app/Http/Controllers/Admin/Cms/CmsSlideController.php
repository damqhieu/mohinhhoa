<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsSlide;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsSlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Slide';
        $titlePageSmall = 'Slide';
        $cmsSlides = CmsSlide::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.slide.index', compact('cmsSlides', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new slide';
        $titlePageSmall = 'Create new slide';
        return view('admin.cms-manager.slide.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_slides|max:191',
            'alt' => 'max:191',
            'image' => 'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('cms-slides.create')
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsSlide();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-slides.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsSlide = CmsSlide::with('slide_elements')->findOrFail($id);
        $titlePage = 'Edit slide: ' . str_limit($cmsSlide->title, 35);
        $titlePageSmall = 'Edit slide';

        return view('admin.cms-manager.slide.edit', compact('cmsSlide', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsSlide::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:191|unique:cms_slides,title,' . $id,
            'alt' => 'max:191',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-slides.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            if (file_exists($model->image))
                unlink(public_path($model->image));
        }

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-slides.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cache::flush();
        $cmsSlide = CmsSlide::findOrFail($id);

        $msg = $cmsSlide->delete();

        if ($msg) {

            if (file_exists($cmsSlide->image))
                unlink(public_path($cmsSlide->image));

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }

    public function is_active(Request $request)
    {
        dd($request->value);
    }
}
