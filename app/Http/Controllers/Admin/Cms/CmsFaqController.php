<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsFaq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsFaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Faq';
        $faqs = CmsFaq::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.faq.index', compact('faqs', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new faq';
        $titlePageSmall = 'Create new faq';
        return view('admin.cms-manager.faq.create', compact('titlePage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'question' => 'required|unique:cms_faqs|max:191',
            'answer' => 'required|max:2000',
            'locale' => 'required|max:191',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsFaq();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-faqs.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsFaq = CmsFaq::findOrFail($id);
        $titlePage = 'Edit faq: ' . str_limit($cmsFaq->question, 35);

        return view('admin.cms-manager.faq.edit', compact('cmsFaq', 'titlePage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:191|unique:cms_faqs,question,'. $id,
            'answer' => 'required|max:2000',
            'locale' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $model = CmsFaq::findOrFail($id);

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-faqs.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsFaq = CmsFaq::findOrFail($id);

        $msg = $cmsFaq->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
