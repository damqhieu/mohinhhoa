<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 7/29/2018
 * Time: 3:55 PM
 */

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsSubscribe;
use App\Http\Controllers\Controller;

class SubscribeController extends Controller
{
    public function index(){
        $subscribes = CmsSubscribe::cursor();
        return view('admin.cms-manager.subscribe.index',compact('subscribes'));
    }
}