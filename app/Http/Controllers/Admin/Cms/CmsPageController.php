<?php

namespace App\Http\Controllers\Admin\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Models\Cms\CmsPage;

class CmsPageController extends Controller
{
    public function index()
    {
        $titlePage = 'Page';
        $titlePageSmall = 'Page';
        $pages = CmsPage::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.page.index', compact('pages', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new page';
        $titlePageSmall = 'Create new page';
        return view('admin.cms-manager.page.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_pages|max:185',
            'code_page' => 'required|unique:cms_pages|max:30',
            'slug' => 'required|unique:cms_pages|max:191',
            'author' => 'max:191',
            'locale' => 'required|min:2|max:3',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-pages.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsPage();

        $model->fill($request->all());

        $model->tags = json_encode($request->tag);
        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;
        $model->code_page = strtolower(str_slug($request->code_page, '_'));


        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }
        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }


        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-pages.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsPage = CmsPage::findOrFail($id);
        $titlePage = 'Edit page: ' . str_limit($cmsPage->title, 35);
        $titlePageSmall = 'Edit page';

        return view('admin.cms-manager.page.edit', compact('cmsPage', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsPage::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:185|unique:cms_pages,title,'. $id,
            'slug' => 'required|max:191|unique:cms_pages,slug,'. $id,
            'author' => 'max:191',
            'locale' => 'required|min:2|max:3',
            'tag_title' => 'max:191',
            'overview' => 'max:1000',
            'description' => 'max:1000',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-pages.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('img_thumbnail')) {
            if (file_exists($model->img_thumbnail))
                unlink(public_path($model->img_thumbnail));
        }

        if ($request->hasFile('img_banner')) {
            if (file_exists($model->img_banner))
                unlink(public_path($model->img_banner));
        }



        $model->fill($request->all());

        $model->tags = json_encode($request->tag);
        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;

        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }
        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }


        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('cms-pages.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CmsPage::findOrFail($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->img_thumbnail))
                unlink(public_path($model->img_thumbnail));

            if (file_exists($model->img_banner))
                unlink(public_path($model->img_banner));

            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
