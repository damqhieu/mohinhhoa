<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsTestimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsTestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = "Testimonials";
        $testimonials = CmsTestimonial::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.testimonial.index', compact('titlePage', 'testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Create Testimonials";
        return view('admin.cms-manager.testimonial.create', compact('titlePage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_testimonials',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'position' => 'nullable|max:191',
            'title' => 'nullable|max:191',
            'evaluate' => 'nullable|max:2000',
            'locale' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsTestimonial();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-testimonials.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsTestimonial = CmsTestimonial::findOrFail($id);
        $titlePage = "Edit " . $cmsTestimonial->name;
        return view('admin.cms-manager.testimonial.edit', compact('titlePage', 'cmsTestimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $cmsTestimonial = CmsTestimonial::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:cms_testimonials,name,' . $id,
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'position' => 'nullable|max:191',
            'title' => 'nullable|max:191',
            'evaluate' => 'nullable|max:2000',
            'locale' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            if (file_exists($cmsTestimonial->image))
                unlink(public_path($cmsTestimonial->image));
        }

        $cmsTestimonial->fill($request->all());

        $cmsTestimonial->is_active = (isset($request->is_active)) ? 1 : 0;

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $cmsTestimonial->getTable() . '/image', $imageName);
            //Lưu DB
            $cmsTestimonial->image = 'upload/' . $cmsTestimonial->getTable() . '/image/' . $imageName;
        }

        $flag = $cmsTestimonial->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = CmsTestimonial::find($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->image ))
                unlink(public_path($model->image ));

            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
