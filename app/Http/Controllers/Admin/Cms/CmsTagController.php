<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsTagController extends Controller
{
    public function index()
    {
        $titlePage = 'Tag';
        $titlePageSmall = 'Tag';
        $cmsTags = CmsTag::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.tag.index', compact('cmsTags', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new tag';
        $titlePageSmall = 'Create new tag';
        return view('admin.cms-manager.tag.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_tags|max:185',
            'url' => 'nullable|url',
            'locale' => 'required|min:2|max:3'
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('cms-tags.create')
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsTag();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;


        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-tags.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsTag = CmsTag::findOrFail($id);
        $titlePage = 'Edit tag: ';
        $titlePageSmall = 'Edit tag';

        return view('admin.cms-manager.tag.edit', compact('cmsTag', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = CmsTag::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:185|unique:cms_tags,title,' . $id,
            'url' => 'nullable|url',
            'locale' => 'required|min:2|max:3'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-tags.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }


        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;


        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-tags.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsTag = CmsTag::findOrFail($id);

        $msg = $cmsTag->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
