<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/28/2018
 * Time: 2:05 PM
 */

namespace App\Http\Controllers\Admin\Cms;


use App\Http\Controllers\Controller;
use App\Models\Cms\CmsSlide;
use App\Models\Cms\SlideElement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsSlideElementController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $listElement = SlideElement::where('cms_slide_id', $request->id)->get()->toArray();
            if (empty($listElement))
                return response()->json(['data' => false]);
            return response()->json(['data' => $listElement]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $titlePage = 'Slide element';
        $titlePageSmall = 'Slide element';
        $slide = CmsSlide::select('id', 'title as text')->get()->toArray();

        return view('admin.cms-manager.slide-element.create', compact('slide', 'titlePageSmall', 'titlePage'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'cms_slide_id' => 'required|numeric',
            'title' => 'required|unique:slide_elements|max:191',
            'url' => 'nullable|url',
            'target' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-slide-elements.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new SlideElement();

        $model->fill($request->all());

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-slides.edit', $model->cms_slide_id));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $titlePage = 'Edit slide element';
        $titlePageSmall = 'Edit slide element';
        $slideElement = SlideElement::findOrFail($id);
        $slide = CmsSlide::select('id', 'title as text')->get()->toArray();
        return view('admin.cms-manager.slide-element.edit', compact('slideElement', 'slide', 'titlePage', 'titlePageSmall'));
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = SlideElement::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'cms_slide_id' => 'required|numeric',
            'title' => 'required|max:191|unique:slide_elements,title,' . $id,
            'url' => 'nullable|url',
            'target' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $model->fill($request->all());

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-slide-elements.edit', $id));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $model = SlideElement::find($id);
        $msg = $model->delete();
        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}