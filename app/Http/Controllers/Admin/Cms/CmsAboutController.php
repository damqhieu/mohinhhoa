<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsAbout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsAboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'About';
        $titlePageSmall = 'About';
        $abouts = CmsAbout::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.about.index', compact('abouts', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new about';
        $titlePageSmall = 'Create new about';
        return view('admin.cms-manager.about.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_abouts|max:185',
            'slug' => 'required|unique:cms_abouts|max:191',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-abouts.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new CmsAbout();

        $model->fill($request->all());

        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }

        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }



        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-abouts.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $cmsAbout
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsAbout = CmsAbout::findOrFail($id);
        $titlePage = 'Edit about';
        $titlePageSmall = 'Edit about';
        return view('admin.cms-manager.about.edit', compact('cmsAbout', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $cmsAbout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();
        $model = CmsAbout::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:185|unique:cms_abouts,title,'. $id,
            'slug' => 'required|max:191|unique:cms_abouts,slug,'. $id,
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
            'img_thumbnail' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-abouts.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }




        if ($request->hasFile('img_thumbnail')) {
            if (file_exists($model->img_thumbnail))
                unlink(public_path($model->img_thumbnail));
        }

        if ($request->hasFile('img_banner')) {
            if (file_exists($model->img_banner))
                unlink(public_path($model->img_banner));
        }

        $model->fill($request->all());

        if ($request->hasFile('img_thumbnail')) {

            $image = $request->file('img_thumbnail');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_thumbnail', $imageName);
            //Lưu DB
            $model->img_thumbnail = 'upload/' . $model->getTable() . '/img_thumbnail/' . $imageName;
        }

        if ($request->hasFile('img_banner')) {

            $image = $request->file('img_banner');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
            //Lưu DB
            $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
        }

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-abouts.edit',$id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $cmsAbout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsAbout = CmsAbout::findOrFail($id);

        $msg = $cmsAbout->delete();

        if ($msg) {
            if (file_exists($cmsAbout->img_thumbnail))
                unlink(public_path($cmsAbout->img_thumbnail));
            if (file_exists($cmsAbout->img_banner))
                unlink(public_path($cmsAbout->img_banner));
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
