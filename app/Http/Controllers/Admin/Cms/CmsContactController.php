<?php

namespace App\Http\Controllers\Admin\Cms;

use App\Models\Cms\CmsContact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class CmsContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Contact';
        $titlePageSmall = 'Contact';
        $contacts = CmsContact::orderBy('created_at', 'DESC')->get();
        return view('admin.cms-manager.contact.index', compact('contacts', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new contact';
        $titlePageSmall = 'Create new contact';
        return view('admin.cms-manager.contact.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:cms_contacts|max:185',
            'slug' => 'required|unique:cms_contacts|max:191',
            'cms_category_id' => 'nullable|numeric',
            'locale' => 'required|min:2|max:3',
            'phone' => 'max:12',
            'email' => 'nullable|email|max:191',
            'address' => 'max:191',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);
        if ($validator->fails()) {
            return redirect()
                ->route('cms-contacts.create')
                ->withErrors($validator)
                ->withInput();
        }

        $model = new CmsContact();

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot      = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top      = (isset($request->is_top)) ? 1 : 0;
        $model->is_new      = (isset($request->is_new)) ? 1 : 0;

        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('cms-contacts.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cmsContact = CmsContact::findOrFail($id);
        $titlePage = 'Edit contact: ' . str_limit($cmsContact->title, 35);
        $titlePageSmall = 'Edit contact';

        return view('admin.cms-manager.contact.edit', compact('cmsContact', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();
        $model = CmsContact::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'title' => 'sometimes|required|max:185|unique:cms_contacts,title,'. $id,
            'slug' => 'sometimes|required|max:191|unique:cms_contacts,slug,'. $id,
            'cms_category_id' => 'nullable|numeric',
            'locale' => 'required|min:2|max:3',
            'phone' => 'max:12',
            'email' => 'nullable|email|max:191',
            'address' => 'max:191',
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('cms-contacts.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot      = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top      = (isset($request->is_top)) ? 1 : 0;
        $model->is_new      = (isset($request->is_new)) ? 1 : 0;

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('cms-contacts.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmsContact = CmsContact::findOrFail($id);

        $msg = $cmsContact->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
