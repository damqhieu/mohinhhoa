<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class UserAdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $titlePage = "User manager";
        if ($request->ajax()) {
            return Datatables::of(Admin::query())
                ->addColumn('is_active', function ($user) {
                    return view('admin.user-manager.active', ['active' => $user->is_active]);
                })
                ->addColumn('action', function ($user) {
                    return '<a href="' . route('user-admins.edit', $user->id) . '" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i></a>'
                        . "&nbsp;" . '<a href="' . route('user-admins.destroy', $user->id) . '" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i></a>';
                })
                ->editColumn('id', '{{$id}}')
                ->rawColumns(['is_active', 'action'])
                ->make(true);
        }

        return view('admin.user-manager.index', compact('titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = "Create user";
        return view('admin.user-manager.create', compact('titlePage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:admins,name',
            'email' => 'required|max:191|unique:admins,email',
            'type' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user-admins.create')
                ->withErrors($validator)
                ->withInput();
        }

        $user = new Admin();

        $user->fill($request->all());
        $user->password = bcrypt(123456);
        $user->token    = hash_hmac('sha256', $request->name, $request->email);

        $flag = $user->save();

        if ($flag) {
            $mailTo = $user->email;
            $nameTo = $user->name;

            $link = route('user.check-user', $user->token);

            Mail::send('admin.mail.check-user', compact('link'), function($message) use ($mailTo, $nameTo){
                $message->to( $mailTo, $nameTo)->subject('Create user admin!');
            });

            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Admin::findOrFail($id);
        $titlePage = "Edit user";
        return view('admin.user-manager.edit', compact('user', 'titlePage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $user = Admin::findOrFail($id);

        $user->fill($request->all());

        if ($user->update()) {
            return back()->with('success', 'Cập nhật thành công!');
        }
        return back()->with('danger', 'Cập nhật thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $msg = Admin::find($id)->delete();

        if ($msg) {
            return response()->json(array(
                'status' => 204,
                'msg' => '204: HTTP requests successful'
            ));
        }
        return response()->json(array(
            'status' => 400,
            'msg' => '400: Bad Request'
        ));
    }
}
