<?php

namespace App\Http\Controllers\Admin\Transport;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transport\TransportPlace;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class TransportPlaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Transport Place';
        $titlePageSmall = 'Transport Place';
        $places = TransportPlace::orderBy('created_at', 'DESC')->get();
        return view('admin.transport-manager.place.index', compact('places', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new Transport Place';
        $titlePageSmall = 'Create new Transport Place';
        return view('admin.transport-manager.place.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:transport_places|max:185',
            'address' => 'required|max:191',
            'latitude' => 'max:191',
            'longitude' => 'max:191',
            'sort_order' => 'numeric',
            'locale' => 'required|min:2|max:3',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-places.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new TransportPlace();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }


        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('transport-places.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place = TransportPlace::findOrFail($id);
        $titlePage = 'Edit transport place';
        $titlePageSmall = 'Edit transport place';

        return view('admin.transport-manager.place.edit', compact('place', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransportPlace::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185|unique:transport_places,name,'. $id,
            'address' => 'required|max:191',
            'latitude' => 'max:191',
            'longitude' => 'max:191',
            'sort_order' => 'numeric',
            'locale' => 'required|min:2|max:3',
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-places.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }


        if ($request->hasFile('image')) {
            if (file_exists($model->image))
                unlink(public_path($model->image));
        }

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('transport-places.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = TransportPlace::find($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->imgage))
                unlink(public_path($model->imgage));
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
