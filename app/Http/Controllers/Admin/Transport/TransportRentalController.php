<?php

namespace App\Http\Controllers\Admin\Transport;

use App\Models\Transport\TransportRental;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransportRentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Transport Rental';
        $titlePageSmall = 'Transport Rental';
        $rentals = TransportRental::orderBy('created_at', 'DESC')->get();
        return view('admin.transport-manager.rental.index', compact('rentals', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new Transport Rental';
        $titlePageSmall = 'Create new Transport Rental';
        return view('admin.transport-manager.rental.create', compact('titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:transport_rentals|max:185',
            'description' => 'max:1000',
            'currency' => 'required',Rule::in(['VND','USD']),
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-rentals.create')
                ->withErrors($validator)
                ->withInput();
        }



        $model = new TransportRental();


        $model->fill($request->all());

        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('transport-rentals.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rental = TransportRental::findOrFail($id);
        $titlePage = 'Edit transport rental';
        $titlePageSmall = 'Edit transport rental';

        return view('admin.transport-manager.rental.edit', compact('rental', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransportRental::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185|unique:transport_rentals,name,'. $id,
            'description' => 'max:1000',
            'currency' => 'required',Rule::in(['VND','USD']),
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-rentals.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }


        if ($request->hasFile('image')) {
            if (file_exists($model->image))
                unlink(public_path($model->image));
        }

        $model->fill($request->all());


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('transport-rentals.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = TransportRental::find($id);

        $msg = $model->delete();

        if ($msg) {
            if (file_exists($model->imgage))
                unlink(public_path($model->imgage));
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
