<?php

namespace App\Http\Controllers\Admin\Transport;

use Illuminate\Http\Request;
use App\Models\Transport\TransportSchedule;
use App\Models\Transport\TransportTrip;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransportScheduleController extends Controller
{
    public function index(){

        $schedules = TransportSchedule::with('transport_trip')->get();
        return view('admin.transport-manager.schedule.index',compact('schedules'));
    }

    public function create()
    {
        $titlePage = 'Create new transport';
        $titlePageSmall = 'Create new transport';
        $trips = TransportTrip::with(['departure_place', 'destination_place'])->get();
        return view('admin.transport-manager.schedule.create', compact('titlePage', 'titlePageSmall','trips'));
    }

    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'transport_trip_id' => 'required|integer',
            'start_time' => 'date_format:"H : i"',
            'end_time' => 'date_format:"H : i"',
            'author' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-schedules.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new TransportSchedule();

        $model->fill($request->all());


        $model->is_active = (isset($request->is_active)) ? 1 : 0;


        $flag = $model->save();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('transport-schedules.index'));
    }

    public function edit($id)
    {
        $schedule = TransportSchedule::findOrFail($id);
        $titlePage = 'Edit schedule';
        $trips = TransportTrip::with(['departure_place', 'destination_place'])->get();
        return view('admin.transport-manager.schedule.edit', compact('trips','schedule', 'titlePage'));
    }

    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransportSchedule::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'transport_trip_id' => 'required|integer',
//            'start_time' => 'required|date_format:"H:i A"',
//            'end_time' => 'required|date_format:"H:i A"',
            'author' => 'max:191'

        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $model->fill($request->all());

        $model->is_active   = (isset($request->is_active)) ? 1 : 0;

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('transport-schedules.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = TransportSchedule::findOrFail($id);
        $msg = $schedule->delete();

        if ($msg) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
