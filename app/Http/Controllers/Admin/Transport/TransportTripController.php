<?php

namespace App\Http\Controllers\Admin\Transport;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transport\TransportTrip;
use App\Models\Transport\TransportPlace;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransportTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titlePage = 'Transport Trip';
        $titlePageSmall = 'Transport Trip';
        $trips = TransportTrip::orderBy('created_at', 'DESC')->get();
        $places = TransportPlace::all();
        return view('admin.transport-manager.trip.index', compact('places','trips', 'titlePageSmall', 'titlePage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new Transport Trip';
        $titlePageSmall = 'Create new Transport Trip';
        $places = TransportPlace::all();
        return view('admin.transport-manager.trip.create', compact('places','titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'name' => 'required|max:185',
            'departure' => 'required|max:191',
            'destination' => 'required|max:191',
            'price' => 'numeric',
            'price_sale' => 'nullable|numeric',
            'discount' => 'nullable|integer',
            'locale' => 'required|min:2|max:3',
            'currency' => 'required',Rule::in(['VND','USD']),
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-trips.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new TransportTrip();

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }


        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('transport-trips.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = TransportTrip::findOrFail($id);
        $titlePage = 'Edit transport trip';
        $titlePageSmall = 'Edit transport trip';
        $places = TransportPlace::all();
        return view('admin.transport-manager.trip.edit', compact('places','trip', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransportTrip::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'image' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
            'name' => 'required|max:185',
            'departure' => 'required|max:191',
            'destination' => 'required|max:191',
            'price' => 'numeric',
            'price_sale' => 'nullable|numeric',
            'discount' => 'nullable|integer',
            'locale' => 'required|min:2|max:3',
            'currency' => 'required',Rule::in(['VND','USD']),
            'tag_title' => 'max:191',
            'meta_author' => 'max:191',
            'meta_keywords' => 'max:191',
            'meta_description' => 'max:191',
            'meta_canonical' => 'max:191',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transport-trips.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }


        if ($request->hasFile('image')) {
            if (file_exists($model->image))
                unlink(public_path($model->image));
        }

        $model->fill($request->all());

        $model->is_active = (isset($request->is_active)) ? 1 : 0;
        $model->is_hot = (isset($request->is_hot)) ? 1 : 0;
        $model->is_top = (isset($request->is_top)) ? 1 : 0;
        $model->is_new = (isset($request->is_new)) ? 1 : 0;


        if ($request->hasFile('image')) {

            $image = $request->file('image');
            $imageName = time() . $image->getClientOriginalName();
            // Lưu thư mục nào
            $image->storeAs('upload/' . $model->getTable() . '/image', $imageName);
            //Lưu DB
            $model->image = 'upload/' . $model->getTable() . '/image/' . $imageName;
        }

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return redirect(route('transport-trips.edit', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = TransportTrip::find($id);

        $msg = $model->delete();

        if ($msg) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
