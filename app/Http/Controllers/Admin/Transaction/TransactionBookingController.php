<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Models\Transaction\TransactionBooking;
use App\Models\Transport\TransportTrip;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionBookingController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $data = TransactionBooking::getAllData($request);
            return response()->json($data);
        }
        return view('admin.transaction-manager.booking.index');
    }

    public function create()
    {
        $titlePage = 'Create new Transaction booking';
        $titlePageSmall = 'Create new Transaction booking';
        $trips = TransportTrip::with(['departure_place', 'destination_place'])->get();
        return view('admin.transaction-manager.booking.create', compact('trips', 'titlePage', 'titlePageSmall'));
    }

    public function store(Request $request)
    {
        Cache::flush();

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185',
            'email' => 'email',
//            'time_departure' =>'',
//            'time_arrival' =>'after:time_departure',
            'flight_number' => 'max:191',
            'quantity_adult' => 'numeric|min:0|max:1000',
            'quantity_child' => 'numeric|min:0|max:1000',
            'departure_date' => 'date',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('transaction-bookings.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new TransactionBooking();

        $model->fill($request->all());

        $model->order_code = strtoupper(md5(time() . 'Tong-Van-Duc'));
        $model->transaction_code = strtoupper(md5($model->order_code));

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect(route('transaction-bookings.index'));
    }

    public function edit($id)
    {
        $booking = TransactionBooking::findOrFail($id);
        $trips = TransportTrip::with(['departure_place', 'destination_place'])->get();
        $titlePage = 'Edit transaction booking';
        $titlePageSmall = 'Edit transaction booking';

        return view('admin.transaction-manager.booking.edit', compact('trips', 'booking', 'titlePage', 'titlePageSmall'));
    }

    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransactionBooking::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185',
            'email' => 'email',
//            'time_departure' =>'',
//            'time_arrival' =>'after:time_departure',
            'flight_number' => 'max:191',
            'quantity_adult' => 'numeric|min:0|max:1000',
            'quantity_child' => 'numeric|min:0|max:1000',
            'departure_date' => 'date',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $model->fill($request->all());
        
        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('transaction-bookings.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = TransactionBooking::findOrFail($id);

        $msg = $booking->delete();

        if ($msg) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
