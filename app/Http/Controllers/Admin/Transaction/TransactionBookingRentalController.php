<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Models\Transaction\TransactionBookingRental;
use App\Models\Transport\TransportRental;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class TransactionBookingRentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $bookingRentals = TransactionBookingRental::with('transport_rental')->get();
        return view('admin.transaction-manager.booking-rental.index',compact('bookingRentals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titlePage = 'Create new Transaction booking rental ';
        $titlePageSmall = 'Create new Transaction booking rental';
        $rentals = TransportRental::select('id','name')->get();
        return view('admin.transaction-manager.booking-rental.create', compact( 'rentals','titlePage', 'titlePageSmall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Cache::flush();
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185',
            'email' => 'email',
            'flight_number' => 'max:191',
            'quantity_adult' => 'numeric|min:0|max:1000',
            'quantity_child' => 'numeric|min:0|max:1000',
            'departure_date' => 'nullable|date',
            'return_date' => 'nullable|date',
        ]);

        if ($validator->fails()) {
            if($request->is_view){
                return back()->with('error','Error!');
            }
            return redirect()
                ->route('transaction-booking-rentals.create')
                ->withErrors($validator)
                ->withInput();
        }
        $model = new TransactionBookingRental();

        $model->fill($request->all());
        $model->booking_time = time();

        $flag = $model->save();


        if ($flag) {
            session()->flash('success', "Success!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }


        if($request->is_view){
            $rental = TransportRental::where('id',$model->transport_rental_id)->select('name')->get();
            Mail::send('client.mail.mail-booking-rental', compact('model','rental'), function($message) use ($model){
                $message->to($model->email);
                $message->subject('New rental!');
            });
            return back();
        }

        return redirect(route('transaction-booking-rentals.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bookingRental = TransactionBookingRental::findOrFail($id);
        $titlePage = 'Edit transaction booking rental';
        $titlePageSmall = 'Edit transaction booking rental';
        $rentals = TransportRental::select('id','name')->get();

        return view('admin.transaction-manager.booking-rental.edit', compact('rentals','bookingRental', 'titlePage', 'titlePageSmall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cache::flush();

        $model = TransactionBookingRental::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:185',
            'email' => 'email',
            'flight_number' => 'max:191',
            'quantity_adult' => 'numeric|min:0|max:1000',
            'quantity_child' => 'numeric|min:0|max:1000',
            'departure_date' => 'date',
            'return_date' => 'date',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $model->fill($request->all());

        $flag = $model->update();

        if ($flag) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }

        return redirect()->route('transaction-booking-rentals.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rentals = TransactionBookingRental::findOrFail($id);

        $msg = $rentals->delete();

        if ($msg) {
            session()->flash('success', "Thành công!!!");
        } else {
            session()->flash('danger', "Không thành công!!!");
        }
        return back();
    }
}
