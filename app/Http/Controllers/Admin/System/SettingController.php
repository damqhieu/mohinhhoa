<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/30/2018
 * Time: 3:46 PM
 */

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Controller;
use App\Models\System\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function save(Request $request, $id = null)
    {
        Cache::flush();

        $datas = Setting::orderBy('key')->get(['id', 'key', 'value']);

        if(!empty($id)) {
            $model = Setting::findOrFail($id);
        }

        if ($request->post()) {


            $validator = Validator::make($request->all(), array(
                'key' => 'required|max:191',
                'value' => 'max:2000'
            ));

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $model = $model ?? new Setting();

            $model->key = strtolower($request->key);
            $model->value = $request->value;

            $flag = $model->save();

            if($flag) {
                session()->flash('success', 'Hành động thành công!');
            } else {
                session()->flash('danger', 'Hành động không thành công!');
            }

            return redirect(route('system-setting.save'));
        }
        return view('admin.system-administrator.setting.index', compact('datas', 'model'));
    }
}