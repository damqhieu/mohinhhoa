<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/30/2018
 * Time: 3:46 PM
 */

namespace App\Http\Controllers\Admin\System;

use App\Http\Controllers\Controller;
use App\Lib\MediaLib;
use App\Models\System\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{
    public function save(Request $request)
    {
        Cache::flush();

        $model = General::firstOrCreate(['id' => 1]);

        if ($request->post()) {
            $validator = Validator::make($request->all(), array(
                'name' => 'max:191',
                'domain' => 'max:191',
                'author' => 'max:191',
                'email' => 'nullable|email|max:191',
                'hotline' => 'max:191',
                'address' => 'max:191',
                'google_analytics' => 'max:1000',
                'google_webmaster' => 'max:1000',
                'google_adsense' => 'max:1000',
                'google_map' => 'max:1000',
                'social_share' => 'max:1000',
                'live_chat' => 'max:1000',
                'img_logo' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
                'img_banner' => 'nullable|image|max:2048|mimes:jpeg,png,jpg,gif,svg',
                'favicon' => 'nullable|file|max:2048|mimes:jpeg,png,jpg,gif,svg,ico',
                'link_facebook' => 'nullable|url|max:191',
                'link_google_plus' => 'nullable|url|max:191',
                'link_twitter' => 'nullable|url|max:191',
                'link_linkedin' => 'nullable|url|max:191',
                'link_vimeo' => 'nullable|url|max:191',
                'link_skype' => 'nullable|url|max:191',
                'tag_title' => 'max:191',
                'meta_author' => 'max:191',
                'meta_keywords' => 'max:191',
                'meta_description' => 'max:191',
                'meta_canonical' => 'max:191',
            ));
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            if ($request->hasFile('img_logo')) {
                if (file_exists( General::getPathName() . $model->img_logo) && is_file(General::getPathName() . $model->img_logo))
                    unlink(public_path(General::getPathName() . $model->img_logo));
            }

            if ($request->hasFile('img_banner')) {
                if (file_exists( General::getPathName() . $model->img_banner) && is_file(General::getPathName() . $model->img_banner))
                    unlink(public_path(General::getPathName() . $model->img_banner));
            }

            if ($request->hasFile('favicon')) {
                if (file_exists( General::getPathName() . $model->favicon) && is_file(General::getPathName() . $model->favicon))
                    unlink(public_path(General::getPathName() . $model->favicon));
            }

            $model->fill($request->all());

            if ($request->hasFile('img_logo')) {

                $image = $request->file('img_logo');
                $imageName = time() . $image->getClientOriginalName();
                // Lưu thư mục nào
                $image->storeAs('upload/' . $model->getTable() . '/img_logo', $imageName);
                //Lưu DB
                $model->img_logo = 'upload/' . $model->getTable() . '/img_logo/' . $imageName;
            }

            if ($request->hasFile('img_banner')) {

                $image = $request->file('img_banner');
                $imageName = time() . $image->getClientOriginalName();
                // Lưu thư mục nào
                $image->storeAs('upload/' . $model->getTable() . '/img_banner', $imageName);
                //Lưu DB
                $model->img_banner = 'upload/' . $model->getTable() . '/img_banner/' . $imageName;
            }

            if ($request->hasFile('favicon')) {

                $image = $request->file('favicon');
                $imageName = time() . $image->getClientOriginalName();
                // Lưu thư mục nào
                $image->storeAs('upload/' . $model->getTable() . '/favicon', $imageName);
                //Lưu DB
                $model->favicon = 'upload/' . $model->getTable() . '/favicon/' . $imageName;
            }

            $flag = $model->save();

            if($flag) {
                session()->flash('success', 'Cập nhật thành công!');
            } else {
                session()->flash('danger', 'Cập nhật không thành công!');
            }
        }

        return view('admin.system-administrator.general.form', compact('model'));
    }
}