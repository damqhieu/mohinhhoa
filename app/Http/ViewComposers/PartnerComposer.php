<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Cms\CmsPartner;
class PartnerComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $rs = set_cache('partners',CmsPartner::where('is_active',1)->get(),10);
        $view->with('partners', $rs);
    }
}