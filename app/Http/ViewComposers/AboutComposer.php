<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Cms\CmsAbout;
class AboutComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $rs = set_cache('about',CmsAbout::where('is_active',1)->first(),20);
        $view->with('about', $rs);
    }
}