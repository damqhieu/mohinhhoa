<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 30/03/2018
 * Time: 10:43 SA
 */

namespace App\Http\ViewComposers;

use App\Models\Cms\CmsCategory;
use Illuminate\View\View;

class CmsCategoryComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void0
     */
    public function compose(View $view)
    {
        $rs = set_cache('data_categories', CmsCategory::select(['id', 'name','slug', 'object_name'])->get(),10);
        $view->with('dataCategories', $rs);
    }
}