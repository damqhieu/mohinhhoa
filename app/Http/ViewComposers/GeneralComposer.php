<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\System\General;

class GeneralComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $general = set_cache('general', General::first(), 20);

        $view->with('general', $general);
    }
}