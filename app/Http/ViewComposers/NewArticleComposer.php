<?php

namespace App\Http\ViewComposers;

use App\Models\Cms\CmsArticle;
use Illuminate\View\View;
class NewArticleComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $rs = set_cache('article_new',CmsArticle::where('is_active',1)->where('is_new',1)->take(3)->get(),10);
        $view->with('articles', $rs);
    }
}