<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 5/30/2018
 * Time: 9:33 AM
 */

namespace App\Lib;

use Aws\Credentials\CredentialProvider;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;

class MediaLib
{

    /*
     * Cách dùng như sau
         Route::match(['get', 'post'], 'upload-file', function (Request $request) {
            if ($request->post()) {
                $file = $request->file('image');
                $path = \App\Lib\MediaLib::uploadFileAwsS3Sdk($file);
                return $path;
            }
            return view('upload');

          })->name('upload');
    */

    /*
     * Nếu dùng S3 thì config phần này vào filesystem
     * required composer.json : "aws/aws-sdk-php": "3.0.0",
         's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'endpoint' => "https://s3-".env('AWS_REGION').".".env('AWS_URL')
        ],
    */

    /*
     * Config trong file ENV
        #Region - https://docs.aws.amazon.com/general/latest/gr/rande.html
        AWS_BUCKET=mediaz2
        AWS_ACCESS_KEY_ID=AKIAJLLNHVSOUT4XFNGQ
        AWS_SECRET_ACCESS_KEY=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
        AWS_REGION=ap-southeast-1
        AWS_VERSION=latest
        AWS_URL=amazonaws.com
        AWS_ACL=public-read
        AWS_PROFILE=default

        #local or s3 or local_public
        MAX_FILE_SIZE=10485760 #dung lượng file tính ra byte <=> 10MB
    */

    public static function uploadFileLocalStorage($file, $pathName = 'app/public/')
    {
        $filename = strtoupper(md5(time())) . '-' . str_slug($file->getClientOriginalName(), '_') . '.' . $file->getClientOriginalExtension();
        if (!is_dir(storage_path($pathName))) {
            mkdir(storage_path($pathName), 0777, true);
        }
        $fileUpload = storage_path($pathName . $filename);
        return (move_uploaded_file($file->getPathname(), $fileUpload)) ? $filename : null;
    }

    public static function uploadFilePublic($file, $pathName = 'images')
    {
        $filename = strtoupper(md5(time())) . '-' . str_slug($file->getClientOriginalName(), '_') . '.' . $file->getClientOriginalExtension();
        $destinationPath = public_path($pathName);
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        return ($file->move($destinationPath, $filename)) ? $filename : null;
    }

    public static function uploadFileAwsS3($file)
    {
        $fileName = strtoupper(md5(time())) . '-' . str_slug($file->getClientOriginalName(), '_') . '.' . $file->getClientOriginalExtension();
        Storage::disk('s3')->put($fileName, file_get_contents($file), 'public');
        $fileUrl = Storage::disk('s3')->url($fileName);
        return (!empty($fileUrl)) ? $fileUrl : null;
    }

    public static function uploadFileAwsS3Sdk($file)
    {

        // Only load credentials from environment variables
        $provider = CredentialProvider::env();

        $s3 = new S3Client([
            'version' => env('AWS_VERSION'),
            'region' => env('AWS_REGION'),
            'credentials' => $provider
        ]);

        try {
            $fileName = strtoupper(md5(time())) . '-' . str_slug($file->getClientOriginalName(), '_') . '.' . $file->getClientOriginalExtension();
            $result = $s3->upload(env('AWS_BUCKET'), $fileName, fopen($file->getRealPath(), 'rb'), env('AWS_ACL'));
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
            die;
        }

        return (isset($result['ObjectURL']) && !empty($result['ObjectURL'])) ? $result['ObjectURL'] : null;
    }
}