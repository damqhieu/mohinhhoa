<?php

use Illuminate\Database\Seeder;

class CmsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            \App\Models\CmsCategory::create([
                'parent_id' => rand(0, 8),
                'name' => $faker->name,
                'slug' => str_slug($faker->name),
                'overview' => $faker->text(191),
                'description' => $faker->text(191),
                'content' => $faker->text(191),
                'object_name' => '',
                'tag_title' => $faker->text(191),
                'meta_author' => $faker->name,
                'meta_keywords' => $faker->randomKey(),
                'meta_description' => $faker->text(191),
                'meta_canonical' => $faker->url,
                'locale' => $faker->locale,
            ]);
        }
    }
}
