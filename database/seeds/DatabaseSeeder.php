<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            UsersTableSeeder::class,
            AdminsTableSeeder::class,
//            CmsCategoriesTableSeeder::class,
//            CmsAboutsTableSeeder::class,
//            CmsArticlesTableSeeder::class,
//            CmsContactsTableSeeder::class,
//            CmsFeedbacksTableSeeder::class,
//            CmsServicesTableSeeder::class,
//            CmsSlidesTableSeeder::class,
//            CmsTagsTableSeeder::class
        ]);
    }
}
