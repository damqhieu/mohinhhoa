<?php

use Illuminate\Database\Seeder;

class CmsFeedbacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            \App\Models\CmsFeedback::create([
                'title' => $faker->text(191),
                'name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->slug(),
                'address' => $faker->address,
                'content' => $faker->text(191),
            ]);
        }
    }
}
