<?php

use Illuminate\Database\Seeder;

class CmsArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 100; $i++) {
            \App\Models\CmsArticle::create([
                'cms_category_id' => rand(1, 10),
                'title' => $faker->text(191),
                'slug' => $faker->slug(),
                'img_thumbnail' => $faker->imageUrl(),
                'img_banner' => $faker->imageUrl(),
                'video' => $faker->url,
                'audio' => $faker->url,
                'author' => $faker->name,
                'published' => $faker->date(),
                'overview' => $faker->text(191),
                'description' => $faker->text(191),
                'content' => $faker->text(191),
                'tag_title' => $faker->text(191),
                'meta_author' => $faker->text(191),
                'meta_keywords' => $faker->text(191),
                'meta_description' => $faker->text(191),
                'meta_canonical' => $faker->text(191),
                'locale' => $faker->locale
            ]);
        }
    }
}
