<?php

use Illuminate\Database\Seeder;

class CmsSlidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            \App\Models\CmsSlide::create([
                'title' => $faker->text(191),
                'image' => $faker->imageUrl(),
                'alt' => $faker->text(191),
            ]);
        }
    }
}
