<?php

use Illuminate\Database\Seeder;

class CmsTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            \App\Models\Cms\CmsTag::create([
                'title' => $faker->text(191),
                'url' => $faker->url,
            ]);
        }
    }
}
