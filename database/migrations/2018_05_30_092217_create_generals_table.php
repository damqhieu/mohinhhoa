<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('domain')->nullable();
            $table->string('author')->nullable();
            $table->string('email')->nullable();
            $table->string('hotline')->nullable();
            $table->string('address')->nullable();
            $table->text('google_analytics')->nullable();
            $table->text('google_webmaster')->nullable();
            $table->text('google_adsense')->nullable();
            $table->text('google_map')->nullable();
            $table->text('social_share')->nullable();
            $table->text('live_chat')->nullable();
            $table->string('img_logo')->nullable();
            $table->string('img_banner')->nullable();
            $table->string('favicon')->nullable();
            $table->longText('style')->nullable();
            $table->longText('script')->nullable();
            $table->string('link_facebook')->nullable();
            $table->string('link_google_plus')->nullable();
            $table->string('link_twitter')->nullable();
            $table->string('link_linkedin')->nullable();
            $table->string('link_vimeo')->nullable();
            $table->string('link_skype')->nullable();
            $table->string('tag_title')->nullable();
            $table->string('meta_author')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_canonical')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generals');
    }
}
