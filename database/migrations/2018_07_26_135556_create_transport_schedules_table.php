<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Bảng này sẽ lưu time các chuyến đi trong ngày của 1 trip
         * */
        Schema::create('transport_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_trip_id');
            $table->string('start_time');
            $table->string('end_time');
            $table->string('sort_order')->default(1);
            $table->string('description')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_schedules');
    }
}
