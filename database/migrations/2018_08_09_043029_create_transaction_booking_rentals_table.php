<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionBookingRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_booking_rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_rental_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('time_departure')->nullable();
            $table->string('flight_number')->nullable();
            $table->string('time_arrival')->nullable();
            $table->string('booking_time')->nullable()->comment('Cái này lưu time()');
            $table->integer('quantity_adult')->nullable()->comment('số lượng người lớn');
            $table->integer('quantity_child')->nullable()->comment('số lượng trẻ em');
            $table->date('departure_date')->nullable()->comment('Ngày đi');
            $table->date('return_date')->nullable()->comment('Ngày về. Nếu chọn 2 chiều thì mới có cái này');
            $table->text('booking_note')->nullable();
            $table->string('booking_status')->nullable()->comment('success, fail');
            $table->string('payment_method')->default('VISA')->nullable();
            $table->string('payment_status')->nullable()->comment('pending, done, fail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_booking_rentals');
    }
}
