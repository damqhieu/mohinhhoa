<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_slide_id');
            $table->string('title');
            $table->string('url')->nullable();
            $table->enum('target', ['_blank', '_self', '_parent', '_top'])->default('_self');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide_elements');
    }
}
