<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Bảng này lưu trữ thông tin các chuyến đi
         *
         * */
        Schema::create('transport_trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('name');
            $table->integer('departure')->comment('Địa điểm bắt đầu');
            $table->integer('destination')->comment('Địa điểm đến');
            $table->double('price', 10, 2)->nullable();
            $table->double('price_sale', 10, 2)->nullable();
            $table->integer('discount')->nullable();
            $table->enum('currency', ['VND', 'USD'])->default('USD');
            // Đa ngữ
            $table->string('locale')->default('en')->comment('en, vi, fr');
            // SEO
            $table->string('tag_title')->nullable();
            $table->string('meta_author')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_canonical')->nullable();
            $table->unsignedInteger('sort_order')->default(1);
            // Hiển thị
            $table->boolean('is_active')->default(1);
            $table->boolean('is_new')->default(1);
            $table->boolean('is_top')->default(0);
            $table->boolean('is_hot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_trips');
    }
}
