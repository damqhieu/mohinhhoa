<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transport_trip_id');
            $table->string('order_code')->nullable();
            $table->string('transaction_code')->nullable();
            $table->boolean('is_3d')->default(1);
            $table->string('bank_code')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_hotline')->nullable();
            $table->string('transaction_time')->nullable();
            $table->string('success_time')->nullable();
            $table->double('payer_fee',10,2)->nullable();
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('time_departure')->nullable();
            $table->string('flight_number')->nullable();
            $table->string('time_arrival')->nullable();
            $table->integer('quantity_adult')->nullable()->comment('số lượng người lớn');
            $table->integer('quantity_child')->nullable()->comment('số lượng trẻ em');
            $table->date('departure_date')->nullable()->comment('Ngày đi');
            $table->date('return_date')->nullable()->comment('Ngày về. Nếu chọn 2 chiều thì mới có cái này');
            $table->string('booking_type')->nullable()->comment('vé 1 chiều hay vé 2 chiều: oneway | return');
            $table->text('booking_note')->nullable();
            $table->string('booking_time')->nullable();
            $table->string('payment_method')->default('VISA')->nullable();
            $table->string('payment_status')->nullable()->comment('pending, approved, processing, finish, done, fail');
            $table->string('booking_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_bookings');
    }
}
