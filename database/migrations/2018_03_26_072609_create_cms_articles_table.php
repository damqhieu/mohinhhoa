<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_category_id');
            $table->string('title')->unique();
            $table->text('slug');
            $table->text('img_thumbnail')->nullable();
            $table->text('img_banner')->nullable();
            $table->text('video')->nullable();
            $table->text('audio')->nullable();
            $table->string('author')->nullable();
            $table->date('published')->nullable();
            $table->text('overview')->nullable();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            // SEO
            $table->string('tag_title')->nullable();
            $table->string('meta_author')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_canonical')->nullable();
            // Đa ngữ
            $table->string('locale')->default('en')->comment('en, vi, fr');
            // Hiển thị
            $table->boolean('is_active')->default(1);
            $table->boolean('is_new')->default(1);
            $table->boolean('is_top')->default(0);
            $table->boolean('is_hot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_articles');
    }
}
