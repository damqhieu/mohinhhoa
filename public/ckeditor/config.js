/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.filebrowserBrowseUrl = 'https://www.car-asia.com.vn/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = 'https://www.car-asia.com.vn/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = 'https://www.car-asia.com.vn/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = 'https://www.car-asia.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = 'https://www.car-asia.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = 'https://www.car-asia.com.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

    config.language = 'vi';
    config.uiColor = '#AADC6E';

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    // config.removeButtons = 'Underline,Subscript,Superscript';

    // Set the most common block elements.
    // config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    // config.removeDialogTabs = 'image:advanced;link:advanced';

};
