@extends('layouts.app')
@section('content')
    @php
        $carasia_rental_name = \App\Models\System\Setting::select('value')->where('key','carasia_rental_name')->first();
        $carasia_rental_banner = \App\Models\System\Setting::select('value')->where('key','carasia_rental_banner')->first();
        $carasia_rental_banner_title = \App\Models\System\Setting::select('value')->where('key','carasia_rental_banner_title')->first();
        $carasia_rental_banner_subtitle = \App\Models\System\Setting::select('value')->where('key','carasia_rental_banner_subtitle')->first();
        $carasia_rental_information = \App\Models\System\Setting::select('value')->where('key','carasia_rental_information')->first();
        $carasia_rental_img_thumbnail = \App\Models\System\Setting::select('value')->where('key','carasia_rental_img_thumbnail')->first();
        $carasia_rental_no_price = \App\Models\System\Setting::select('value')->where('key','carasia_rental_no_price')->first();
        $carasia_rental_term = \App\Models\System\Setting::select('value')->where('key','carasia_rental_term')->first();
    @endphp
    <div class="banner">
        <img src="{{ (isset($carasia_rental_banner->value)) ? $carasia_rental_banner->value  : 'client/assets/images/bg-about-us.jpg' }}"
             class="img-responsive" width="100%" height="430"
             alt="{{ isset( $carasia_rental_name->value) ?  $carasia_rental_name->value : 'Car-Asia Rental' }}">
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>Car-asia Rental</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="row">
            <div class="col-md-8 title">
                @php
                    $html = '<h1>CAR ASIA SKYBUS SERVICE</h1>
                            <p>Travelling has been made easier with Car Asia, a door-to-door van service that will get you to destinations such as Ha Noi, Noi Bai Airport, and many more. The rates are competitive for the family van category and are listed below:-</p>
                            <p><strong>ATTENTION:</strong>&nbsp;Booking must be made&nbsp;&nbsp;2 working days in advance</p>';
                @endphp
                {!! isset( $carasia_rental_information->value) ?  $carasia_rental_information->value : $html !!}
            </div>
            <div class="col-md-4">
                <img src="{{ isset( $carasia_rental_img_thumbnail->value) ?  asset($carasia_rental_img_thumbnail->value ): '' }}"
                     class="img-responsive" alt="Car-Asia Rental thumbnail">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2>Schedule & Pricing</h2>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <table border="0" style="width: 100%" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td class="bg_ccc" colspan="4" height="30">
                            <h3>Car-Asia Transfer Charges</h3>
                        </td>
                    </tr>
                    <tr>
                        <td class="bg_off" height="30"><strong>Name</strong></td>
                        <td class="bg_off" height="15%"><strong>Image</strong></td>
                        <td class="bg_off" width="45%" height="30"><strong>Description</strong></td>
                        <td class="bg_off" height="30"><strong>Price</strong></td>
                    </tr>
                    @foreach($rentals as $item)
                        @if($loop->iteration % 2 !=0 )
                            <tr>
                                <td class="bg_true" height="37">{{ $item->name }}</td>
                                <td class="bg_true" height="37">@if(isset($item->image))
                                        <div class="img-rental"><img src="{{ asset($item->image) }}" height="auto"
                                                                     alt="{{ $item->name }}"></div> @endif</td>
                                <td class="bg_true" height="37">{!! $item->description !!}</td>
                                <td class="bg_true"
                                    height="37">{{ isset($item->price)) ? $item->price . ' '. $item->currency : (isset( $carasia_rental_no_price->value) ?  $carasia_rental_no_price->value : '' }}</td>
                            </tr>
                        @else
                            <tr>
                                <td class="bg_false" height="37">{{ $item->name }}</td>
                                <td class="bg_false" height="37">@if(isset($item->image))
                                        <div class="img-rental"><img src="{{ asset($item->image) }}" height="auto"
                                                                     alt="{{ $item->name }}"></div> @endif</td>
                                <td class="bg_false" height="37">{!! $item->description !!}</td>
                                <td class="bg_false"
                                    height="37">{{ isset($item->price)) ? $item->price . ' '. $item->currency : (isset( $carasia_rental_no_price->value) ?  $carasia_rental_no_price->value : '' }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h2 style="padding-bottom: 10px;padding-top: 15px">Car-asia Booking Rental</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <form action="{{ route('transaction-booking-rentals.store') }}" method="post" class="form-in-checkout">
                    @csrf
                    <input type="hidden" name="is_view" value="1">
                    <ul class="row">
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="name" class="">Rental <abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <select class="form-control" name="transport_rental_id" id="">
                                    @foreach($rentals as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="name" class="">Name <abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="name"
                                       id="name">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="email" class="">Email <abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="email"
                                       id="email">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="phone" class="">Phone <abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="phone"
                                       id="phone">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="address" class="">Address<abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="address"
                                       id="address">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="flight_number" class="">Flight number<abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="flight_number"
                                       id="flight_number">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="time_departure" class="">Time departure<abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="time_departure"
                                       id="time_departure">
                            </p>
                        </li>
                        <li class="col-md-6">
                            <p class="form-row validate-required">
                                <label for="time_arrival" class="">Time arrival<abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <input type="text" class="input-text form-control"
                                       name="time_arrival"
                                       id="time_arrival">
                            </p>
                        </li>
                        <div class="clearfix"></div>
                        <li class="col-md-12">
                            <p class="form-row validate-required">
                                <label for="booking_note" class="">Note<abbr
                                            class="required"
                                            title="required"> *</abbr></label>
                                <textarea class="form-control" name="booking_note"
                                          id="note">I have been sure</textarea>
                            </p>
                        </li>
                    </ul>

                    <div class="checkout-col-footer">
                        <input type="submit" value="Submit" class="btn-step btn-highligh">
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="rang">
                    @php
                        $html = '<h2>Book your Car-Asia</h2>
                        <h3>Terms &amp; Conditions</h3>
                        <h5>Charter</h5>
                        <ul class="list type-7 ">
                            <li>You will have the 8-seater van or 10-seater van to yourself.</li>
                            <li>There is no sharing with others and</li>
                            <li>No waiting time.</li>
                            <li>Additional charge RM20 for arrival at KLIA</li>
                        </ul>
                        <h5>CANCELLATION FEE</h5>
                        <ul class="list type-7 ">
                            <li>If a booking is cancelled under 6 hours of pick-up time, a cancellation fee of 10% of total fare is charged.</li>
                        </ul>
                        <h2>Rate per</h2>
                        <h5>Charter</h5>
                        <p><strong>6am – 11pm</strong> &nbsp;&nbsp;&nbsp;&nbsp; Original Price (refer to pricing in table shown above)</p>
                        <p><strong>11pm – 6am </strong> &nbsp;&nbsp;&nbsp;&nbsp; Original Price (refer to pricing in table shown above) + 30% Midnight Surcharge</p>
                        <h5>Enquiries</h5>
                        <p>Note: <span style="color: #ee1b24;">Booking must be made 3 days in advance</span></p>';
                    @endphp
                    {!! isset( $carasia_rental_term->value) ?  $carasia_rental_term->value : $html !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <style>

        /*table*/
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table h3 {
            padding: 25px;
            color: #ee1b24 !important;
            font-size: 23px;
        }

        .img-rental img {
            max-width: 120px;
        }

        .bg_ccc {
            background: #ccc;
            vertical-align: middle !important;
            text-align: center;
            width: 164px;
        }

        .bg_off {
            background: #ee1b24;
            vertical-align: middle !important;
            text-align: center;
            color: #fff !important;
        }

        .bg_true {
            background: #fff;
            vertical-align: middle !important;
            border-width: 0 1px 1px 1px;
            border-color: #e9e9e9;
            border-style: dotted;
            text-align: center;
            height: 30px;
        }

        .bg_false {
            background: #f8f8f8;
            vertical-align: middle !important;
            border-width: 0 1px 1px 1px;
            border-color: #e9e9e9;
            border-style: dotted;
            height: 30px;
            text-align: center;
        }

        /*content */

        .rang {
            border-width: 1px;
            border-color: #ccc;
            border-style: solid;
            border-radius: 10px;
            background-color: #ebebeb;
            padding: 10px;
            margin-bottom: 20px;
        }

        .rang h1, .rang h2, .rang h3, .rang h4, .rang h5, .rang h6 {
            font-family: 'Open Sans', sans-serif;
            color: #ee1b24 !important;
        }

        h2 {
            font-size: 30px;
        }

        ul.list {
            margin-bottom: 20px;
        }

        /*thead {*/
        /*background-color: #ee1b24;*/
        /*}*/

        /*th {*/
        /*color: white;*/
        /*}*/
        h1, h2, h3, h4, h5, h6 {
            font-family: 'Open Sans', sans-serif;
            color: #ee1b24;
        }

        .title h2, .title h3, .title h4, .title h1 {
            color: #ee1b24;
            margin: 10px 0;
            font-size: 30px;
        }

        .about-us-top {
            background: url({{ (isset($carasia_rental_banner->value)) ? $carasia_rental_banner->value : 'client/assets/images/bg-about-us.jpg' }}) top center no-repeat !important;
            padding: 325px 15px 105px;
        }

        @media only screen and (max-width: 338px) {
            .banner img {
                height: 80px;
            }

            .img-rental img {
                width: 100px !important;
            }

        }

        @media only screen and (max-width: 338px) {
            .banner img {
                height: 80px;
            }

            .img-rental img {
                width: 100px !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .banner img {
                height: 80px;
            }

            .img-rental img {
                width: 100px !important;
            }
        }

        @media only screen and (max-width: 480px) {
            .banner img {
                height: 100px;
            }
        }
    </style>
@endsection