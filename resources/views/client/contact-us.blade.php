@extends('layouts.app')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>Contact Us</strong></li>
            </ul>
        </div>
    </div>

    <div class="container">
        <h2 class="title-contact text-center">You can see us on Google Map</h2>
        <iframe src="{{ $general->google_map }}" width="100%"
                height="400"></iframe>
        {{--<div id="map"></div>--}}
        <ul class="row text-center">
            <li class="col-md-4">
                <div class="contact-list-item"><a
                            href="tel:{{ (isset($general->hotline)) ? $general->hotline : '84968313497' }}"><i
                                class="icon-call-in"></i><br><span
                                class="text">{{ (isset($general->hotline)) ? $general->hotline : '84968313497' }}</span></a>
                </div>
            </li>
            <li class="col-md-4">
                <div class="contact-list-item"><i class="icon-pointer"></i><br><span
                            class="text">{{ (isset($general->address)) ? $general->address : 'Me tri ha - Ha noi' }}</span>
                </div>
            </li>
            <li class="col-md-4">
                <div class="contact-list-item"><a
                            href="mailto:{{ (isset($general->email)) ? $general->email : 'CONTACT@ABANISTORE.COM' }}"><i
                                class="icon-envelope-open"></i>
                        <br><span
                                class="text">{{ (isset($general->email)) ? $general->email : 'CONTACT@ABANISTORE.COM' }}</span></a>
                </div>
            </li>
        </ul>
        <form action="{{ route('contact.store') }}" class="contact-form form-wrapper">
            <ul class="row">
                <li class="col-md-6">
                    <label for="">Title: <span class="text-danger">*</span></label>
                    <div class="form-controls"><input name="title" type="text" placeholder="title" class="form-control">
                    </div>

                    <label for="">Email: <span class="text-danger">*</span></label>
                    <div class="form-controls"><input name="email" type="email" placeholder="Email*"
                                                      class="form-control"></div>
                    <label for="">Address: <span class="text-danger">*</span></label>
                    <div class="form-controls"><input name="address" type="text" placeholder="Address"
                                                      class="form-control"></div>
                </li>
                <li class="col-md-6">
                    <label for="">Name: <span class="text-danger">*</span></label>
                    <div class="form-controls"><input name="name" type="text" placeholder="Name*" class="form-control">
                    </div>
                    <label for="">Phone: <span class="text-danger">*</span></label>
                    <div class="form-controls"><input name="phone" type="text" placeholder="Phone" class="form-control">
                    </div>

                </li>
                <div class="clearfix"></div>
                <li class="col-md-12">
                    <label for="">Content: <span class="text-danger">*</span></label>
                    <div class="form-controls"><textarea name="content" placeholder="Message"
                                                         class="form-control"></textarea></div>
                </li>
            </ul>
            <div class="text-center"><input type="submit" value="Send Message" class="inp-submit"></div>
        </form>
    </div>
@endsection
@section('style')

@endsection
@section('js')
    <script>
        var map;

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -21.014138, lng: 105.7834404},
                zoom: 8
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByrDWo7XmXmRtS63Ok8-9DwZfGLOU32fE&callback=initMap"
            async defer></script>
@endsection
