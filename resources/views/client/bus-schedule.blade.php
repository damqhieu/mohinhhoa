@extends('layouts.app')
@section('content')
    @php
        $bus_schedule_banner = \App\Models\System\Setting::select('value')->where('key','bus_schedule_banner')->first();
        $bus_schedule_banner_title = \App\Models\System\Setting::select('value')->where('key','bus_schedule_banner_title')->first();
        $bus_schedule_banner_subtitle = \App\Models\System\Setting::select('value')->where('key','bus_schedule_banner_subtitle')->first();
        $bus_schedule_name = \App\Models\System\Setting::select('value')->where('key','bus_schedule_name')->first();
        $bus_schedule_no_schedule = \App\Models\System\Setting::select('value')->where('key','bus_schedule_no_schedule')->first();
        $bus_schedule_attention = \App\Models\System\Setting::select('value')->where('key','bus_schedule_attention')->first();
    @endphp
    {{--<div class="about-us-top">--}}
    <!--- <div class="title">
            <div class="top">{{ isset( $carasia_rental_name->value) ?  $carasia_rental_name->value : '' }}</div>
            <h1>{{ isset($carasia_rental_banner_title->value) ? $carasia_rental_banner_title->value : '' }}</h1>
            <div class="bottom">{{ isset($carasia_rental_banner_subtitle->value) ? $carasia_rental_banner_subtitle->value : '' }}</div>
        </div>-->
    {{--</div>--}}

    <div class="banner">
        <img src="{{ (isset($bus_schedule_banner->value)) ? $bus_schedule_banner->value  : 'client/assets/images/bg-about-us.jpg' }}"
             class="img-responsive" width="100%" height="430" alt="Bus Schedule">
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>Bus schedule</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container schedule">
        <div class="row">
            <div class="col-md-9">
                <div class="tab tab-vert">
                    <ul class="tab-legend">
                        @foreach($trips as $item)
                            <li class="@if($loop->index == 0) active @endif">{{ $item->name}}</li>
                        @endforeach
                    </ul>
                    <ul class="tab-content">
                        @foreach($trips as $item)
                            <li>
                                <h2  style="font-size: 19px; color: #e42021">{{ $item->name }}</h2>
                                <table class="table table-striped">
                                    <thead>
                                    <th style="font-size: 17px">{{ $item->departure_place->name }}</th>
                                    <th style="font-size: 17px"> {{ $item->destination_place->name }}</th>
                                    </thead>
                                    <tbody>
                                    @if(count($item->transport_schedule) === 0)
                                        <tr>
                                            <td colspan="2">{!! "<h3>" . ((isset($bus_schedule_no_schedule->value)) ? $bus_schedule_no_schedule->value : '') . "</h3>" !!}</td>
                                        </tr>
                                    @else
                                        @foreach($item->transport_schedule as $value)
                                            <tr>
                                                <td>{{ $value->start_time }}</td>
                                                <td>{{ $value->end_time }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" style="padding-top: 20px;"><span class="blink"><a title="{{ $item->name }}" href="{{ route('booking',str_slug( $item->name)) }}" style="font-weight: bold"><button class="button" style="background-color:#e42021 ;padding: 12px 15px 26px; color:#fff "><span style="font-weight: bold">Booking Now</span></button></a></span></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-3 tab-right">
                <h2>Attention:</h2>
                <div class="content">
                    {!! (isset($bus_schedule_attention->value)) ? $bus_schedule_attention->value : '' !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('style')
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="client/assets/dist/main.css"/>
    <link rel="stylesheet" type="text/css" href="client/assets/dist/tabModule.css"/>
    <style>
        .blink{
            color:red;
            /*padding: 15px;*/
            text-align: center;
        }
        span.blink{
            font-size: 18px;
            animation: blink 1.0s linear infinite;
        }
        @keyframes blink{
            0%{opacity: 0;}
            50%{opacity: .75;}
            100%{opacity: 1;}
        }
        .tab-right h2, .tab-right h1 .tab-right h3{
            color: #e42021;
        }

        .schedule .content {
            background-color: #ebebeb;;
            border-radius: 5px;
            padding: 10px;
            border: 1px solid;
        }

        .schedule .content h2 {
            padding-top: 20px;
        }
        .schedule tr:hover{
            background: #fdfdfd;
        }
        .schedule tr:nth-child(even) {
            background: #fdfcfc;
        }
        .table-striped>tbody>tr:nth-child(odd)>td{
            background: white;
        }
        .schedule button.button span {
            display: block;
             height: 0px !important;
             line-height: 1 !important;
        }
        .schedule button.button:hover {
            color: #fff;
            background-color: #e42021;
            border-color: #fff;
        }
        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 360px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 480px) {
            .banner img{
                height: 100px;
            }
        }

        {{--.about-us-top {--}}
            {{--background: url({{ (isset($bus_schedule_banner->value)) ? $bus_schedule_banner->value  : 'client/assets/images/bg-about-us.jpg' }}) top center no-repeat !important;--}}
            {{--padding: 175px 15px 105px;--}}
            {{--background-size: cover;--}}
            {{--text-transform: uppercase;--}}
            {{--color: #fff;--}}
            {{--text-align: center;--}}
        {{--}--}}
        {{--.about-us-top {--}}
            {{--padding: 325px 15px 105px;--}}
        {{--}--}}
    </style>
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="client/assets/dist/tabModule.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            tabModule.init();
        });
    </script>
@endsection