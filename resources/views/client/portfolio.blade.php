@extends('layouts.app')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{route('index')}}" title="Go to Home Page">Home</a></li>
                <li><strong>Porfolio</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="content-top no-border">
            <h2>Portfolio Packery</h2>
        </div>
        <ul class="filter-control filter-packery" target="#portfolio_filter_packery">
            <li class="active" filter="*">All</li>
            @foreach($nameCategory as $item)
                <li filter="{{$item->name}}">{{$item->name}}</li>
            @endforeach
        </ul><!--- .filter-control-->
        <div class="row">
            <div id="portfolio_filter_packery" class="filter-content">
                @foreach($cmsPortfolio as $item)
                    <div class="filter-item {{$item->CmsCategory->name}}">
                        <div class="filter-images">
                            <img src="{{ asset($item->image) }}" alt="{{ $item->alt }}">
                            <div class="bottom"><p class="title">{{$item->title}}</p>
                                <p class="sub-categories">{{$item->CmsCategory->name}}</p></div>
                        </div>
                    </div>
                @endforeach
            </div><!--- .filter-content-->
        </div><!--- .row->
		</div><!--- .container -->
    </div>
@endsection