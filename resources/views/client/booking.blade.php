<!DOCTYPE html>
<html>
<head>
    @php
        $booking_background = \App\Models\System\Setting::select('value')->where('key','booking_background')->first();
    @endphp
    <title>Ticket Booking</title>
    <link rel="stylesheet" href="{{ asset('/client/booking/css/style.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords"
          content="Flight Ticket Booking  Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <style>
        .resp-tabs-list {
            list-style: none;
            padding: 15px 0px;
            margin: 0 auto;
            text-align: center;
            background: #ee1b24;
        }
        .trip {
            padding: 12px 0 12px 0;
            width: 100%;
            font-size: 15px;
        }
        h3 {
            font-size: 18px;
            color: #ee1b24;
            margin-bottom: 7px;
            font-weight: bold;
        }
        span{
            font-weight: bold;
        }
        input[type="submit"] {
            font-size: 14px;
            color: #fff;
            background: #ee1b24;
            border: none;
            outline: none;
            padding: 12px 40px;
            margin-top: 50px;
            cursor: pointer;
            transition: 0.5s all ease;
            -webkit-transition: 0.5s all ease;
            -moz-transition: 0.5s all ease;
            -o-transition: 0.5s all ease;
            -ms-transition: 0.5s all ease;
        }
        .trip option {
            padding: 5px 5px;
        }
        body {
            background: url({{isset($booking_background->value) ? asset($booking_background->value) : '../images/1.jpg' }}) no-repeat 0px 0px;
            background-size: cover;
            font-family: 'Open Sans', sans-serif;
            background-attachment: fixed;
            background-position: center;
        }
        input[type="submit"]:hover {
            background: #ee1b24;
            color: #fff;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
</head>
<body>
<h1 style="color: #ee1b24;    font-weight: bold;">Ticket Booking</h1>
<br>
<a href="{{ route('index') }}" title="Car-Asia Booking"><h4
            style="text-align: center !important;    font-weight: bold; font-size: 18px !important; margin-top: 0px !important; color: #fff;"><i style="color:#ee1b24;" class="fa fa-backward" aria-hidden="true"></i><u> Back
            home</u></h4></a>
<div class="main-agileinfo">
    <div class="sap_tabs">
        <div id="horizontalTab">
            <ul class="resp-tabs-list">
                <li class="resp-tab-item"><span>Round Trip</span></li>
                <li class="resp-tab-item"><span>One way</span></li>
                {{--<li class="resp-tab-item"><span>Multi city</span></li>--}}
            </ul>
            <div class="clearfix"></div>
            <div class="resp-tabs-container">
                <div class="tab-1 resp-tab-content roundtrip">
                    <form action="{{ route('payment.handle-booking') }}" method="post">
                        @csrf
                        <input type="hidden" name="type" value="return">
                        <div class="trip">
                            <h3>Trip</h3>
                            <select name="trip_id" class="frm-field trip required">
                                @foreach($trips as $item)
                                    <option @if($name == str_slug($item->name)) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="date">
                            <div class="depart">
                                <h3>Depart</h3>
                                <input id="datepicker" name="departure_date" type="text" value="{{ date('m/d/Y') }}"
                                       onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                           </div>
                            <div class="return">
                                <h3>Return</h3>
                                <input id="datepicker1" name="return_date" type="text" value="{{ date('m/d/Y', strtotime('+1 day')) }}"
                                       onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <div class="numofppl">
                            <div class="adults">
                                <h3>Adult:<span style="color:#fff;">(7+ yrs)</span></h3>
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <input type="hidden" id="quantity_adult" name="quantity_adult" value="1">
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="child">
                                <h3>Child:<span style="color:#fff;">(2-6 yrs)</h3>
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>0</span></div>
                                        <input type="hidden" id="quantity_child" name="quantity_child" value="0">
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <input type="submit" value="Booking Trip"  style="font-weight:  bold;">
                    </form>
                </div>
                <div class="tab-1 resp-tab-content oneway">
                    <form action="{{ route('payment.handle-booking') }}" method="post">
                        @csrf
                        <input type="hidden" name="type" value="oneway">
                        <div class="trip">
                            <h3>Trip</h3>
                            <select name="trip_id" class="frm-field trip required">
                                @foreach($trips as $item)
                                    <option @if($name == str_slug($item->name)) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="clear"></div>
                        <div class="date">
                            <div class="depart">
                                <h3>Depart</h3>
                                <input class="date" id="datepicker2" name="departure_date" type="text"
                                       value="{{ date('m/d/Y') }}"
                                       onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div class="numofppl">
                            <div class="adults">
                                <h3>Adult:<span style="color:#fff;">(7+ yrs)</h3>
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>1</span></div>
                                        <input type="hidden" id="quantity_adult" name="quantity_adult" value="1">
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="child">
                                <h3>Child:<span style="color:#fff;">(2-6 yrs)</h3>
                                <div class="quantity">
                                    <div class="quantity-select">
                                        <div class="entry value-minus">&nbsp;</div>
                                        <div class="entry value"><span>0</span></div>
                                        <input type="hidden" id="quantity_child" name="quantity_child" value="0">
                                        <div class="entry value-plus active">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <input type="submit" value="Booking Trip" style="font-weight:  bold;">
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<!--script for portfolio-->
<script src="{{ asset('client/booking/js/jquery.min.js') }}"></script>
<script src="{{ asset('client/booking/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    @if (Session::has('error'))
        toastr['error']('', '{!! Session::get("error") !!}');
    @elseif(Session::has('success'))
        toastr['success']('', '{!! Session::get("success") !!}');
    @elseif(Session::has('warning'))
        toastr['warning']('', '{!! Session::get("danger") !!}');
    @endif
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });
</script>
<!--//script for portfolio-->
<!-- Calendar -->
<link rel="stylesheet" href="{{ asset('client/booking/css/jquery-ui.css') }}"/>
<script src="{{ asset('client/booking/js/jquery-ui.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#datepicker").datepicker({
            minDate: 'dateToday',
            onSelect: function (selected) {
                $("#datepicker1").datepicker("option", "minDate", selected)
            }
        });
        $("#datepicker1").datepicker({
            onSelect: function (selected) {
                $("#datepicker").datepicker("option", "maxDate", selected)
            }
        });
        $("#datepicker2").datepicker({
            minDate: 'dateToday'
        });
    });
    $(function () {
        $("#datepicker,#datepicker1,#datepicker2").datepicker();
    });
</script>
<!-- //Calendar -->
<!--quantity-->
<script>
    $('.value-plus').on('click', function () {
        let divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
        divUpd.text(newVal);
        $(this).parent().find('#quantity_child').val(newVal);
        $(this).parent().find('#quantity_adult').val(newVal);
    });

    $('.value-minus').on('click', function () {
        let divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
        if (newVal >= 0) {
            divUpd.text(newVal);
            $(this).parent().find('#quantity_child').val(newVal);
            $(this).parent().find('#quantity_adult').val(newVal);
        }
    });
</script>
<!--//quantity-->
<!--load more-->
<script>
    $(document).ready(function () {
        size_li = $("#myList li").size();
        x = 1;
        $('#myList li:lt(' + x + ')').show();
        $('#loadMore').click(function () {
            x = (x + 1 <= size_li) ? x + 1 : size_li;
            $('#myList li:lt(' + x + ')').show();
        });
        $('#showLess').click(function () {
            x = (x - 1 < 0) ? 1 : x - 1;
            $('#myList li').not(':lt(' + x + ')').hide();
        });
    });
</script>
<script>

</script>
</body>
</html>