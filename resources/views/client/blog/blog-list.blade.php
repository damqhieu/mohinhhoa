@extends('layouts.app')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"> <a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li> <strong>Blog List</strong></li>
                @if(isset($category)) <li> <strong>{{ $category->name }}</strong></li>  @endif
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="main">
            <div class="blog-list">
                @foreach($articles as $item)
                <div class="blog-list-item">
                    <div class="images-thumb"><img src="{{ $item->img_thumbnail }}" class="img-responsive" alt=""></div>
                    <div class="post-larger-detail-blog-list-wrap">
                        <div class="post-large-detail">
                            <i class="post-icon icon-camera"></i>
                            <h2><a href="{{ route('blog-detail', $item->slug) }}">{{ $item->title }}</a></h2>
                            <div class="post-meta">{{ date(' H:m d-m-Y', strtotime($item->created_at)) }} - by <a href="{{ route('blog-detail', $item->slug) }}">{{ $item->author }}</a>  <a href="#"></a>  <a href="{{ route('blog-detail', $item->slug) }}"></div>
                            <div class="post-desc">
                                {!! $item->description ?? $item->overview!!}
                            </div>
                            <a href="{{ route('blog-detail', $item->slug) }}" class="btn-view-post">View post</a>
                        </div>
                    </div>
                </div><!--- .blog-list-item-->
            @endforeach
            </div><!--- .blog-list --->
            <nav class="woocommerce-pagination">
                <ul class="page-numbers">
                    {!! $articles->links() !!}
                </ul>
            </nav><!--- .woocommerce-pagination-->
        </div><!--- .main-->
    </div><!--- .container-->

@endsection