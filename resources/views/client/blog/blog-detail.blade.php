@extends('layouts.app')
@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{route('index')}}" title="Go to Home Page">Home</a></li>
                <li><strong>Blog Detail</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="main">
            <div class="row">
                <div class="col-sm-9 col-left">
                    <div class="post-large-item">
                        <img src="{{$article->img_banner}}" alt="" class="img-responsive">
                        <div class="post-large-single-wrap">
                            <div class="post-large-detail">
                                <i class="post-icon icon-camera"></i>
                                <h2><a href="#">{{$article->title}}</a></h2>
                                <div class="post-meta">{{$article->create_at}} - by <a href="#">{{$article->author}}</a>
                                    <a href="#"</a>  <a href="#"></a></div>
                                <div class="post-desc">
                                    <div class="text-content-single">
                                        {!! $article->overview ?? $article->description!!}
                                    </div>
                                </div>
                                <div class="post-desc">
                                    <div class="text-content-single">
                                        {!! $article->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .post-large-item --->
                    <div class="social-blog-bottom">
                        <span>Share on</span>
                        <ul class="social-widget-list">
                            <li><a href="https://www.facebook.com/"><i class="icon-social-facebook"></i></a></li>
                            <li><a href="https://twitter.com"><i class="icon-social-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/"><i class="icon-social-youtube"></i></a></li>
                            <li><a href="https://dribbble.com/"><i class="icon-social-dribbble"></i></a></li>
                        </ul>
                    </div><!--- .social-blog-bottom-->

                </div>
                <div class="col-sm-3 col-left">
                    <div class="block widget-search">
                        <form role="search" class="searchform" method="get" action="http://theme-fusion.com/avada/">
                            <div class="search-table">
                                <div class="search-field"><input type="text" value="" name="s" class="s"
                                                                 placeholder="SEARCH ..."></div>
                                <div class="search-button">
                                    <button type="submit" class="searchsubmit" value=""><i class="icon-magnifier"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--- .widget-search-->
                    <div class="block widget-social">
                        <div class="block-title"><strong><span>Follow us</span></strong></div>
                        <ul class="social-widget-list">
                            <li><a href="https://www.facebook.com/"><i class="icon-social-facebook"></i></a></li>
                            <li><a href="https://twitter.com"><i class="icon-social-twitter"></i></a></li>
                            <li><a href="https://www.youtube.com/"><i class="icon-social-youtube"></i></a></li>
                            <li><a href="https://dribbble.com/"><i class="icon-social-dribbble"></i></a></li>
                        </ul>
                    </div><!--- .widget-social-->
                    <div class="block widget_categories">
                        <div class="block-title"><strong><span>Categories</span></strong></div>
                        <ul>
                            @foreach($dataCategories as $dataCategory)
                                @if($dataCategory->object_name == CMS_ARTICLES)
                                    <li>
                                        <a href="{{ route('blog-category', $dataCategory->slug) }}">{{$dataCategory->name}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div><!--- .widget_categories-->
                    <div class="block widget_recent_post">
                        <div class="block-title"><strong><span>Lastest Post</span></strong></div>
                        <ul class="recent-post-list">
                            @foreach($articleLast as $item)
                                <li>
                                    <div class="recent-post-item">
                                        <a href="{{ route('blog-detail', $item->slug)  }}" class="left">
                                            <img class="img-responsive"
                                                 src="{{ (isset($item->img_thumbnail)) ? asset($item->img_thumbnail) : 'http://placehold.it/80x102' }}">
                                        </a>
                                        <div class="right">
                                            <h2 class="product-name"><a
                                                        href="{{ route('blog-detail', $item->slug)  }}">{{ $item->title }}</a>
                                            </h2>
                                            <span class="date">{{ date('d-m-Y', strtotime($item->created_at)) }}</span>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div><!--- .widget_recent_post-->
                    {{--<div class="block widget_instagram">--}}
                        {{--<div class="block-title"><strong><span>From Instagram</span></strong></div>--}}
                        {{--<ul class="images_list">--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                            {{--<li><a href="#"><img src="http://placehold.it/76x97" alt=""></a></li>--}}
                        {{--</ul>--}}
                    {{--</div><!--- .widget_instagram-->--}}
                    <div class="block widget_tag_cloud">
                        <div class="block-title"><strong><span>Popular Tags</span></strong></div>
                        <div class="tagcloud">
                            @foreach($tags as $tag)
                                <a href="{{ $tag->url }}">{{ $tag->title }}</a>
                            @endforeach
                        </div>
                    </div><!--- .widget_tag_cloud-->
                    {{--<div class="block widget_text">--}}
                        {{--<div class="block-title"><strong><span>Text Widget</span></strong></div>--}}
                        {{--<p>Success is not the key to happiness. Happiness is the key to success. If you love what you--}}
                            {{--are doing, you will be successful.</p>--}}
                    {{--</div><!--- .widget_text-->--}}
                </div>
            </div>
        </div><!--- .main-->
    </div><!--- .container-->

    {{--<div class="alo-brands">--}}
    {{--<div class="container">--}}
    {{--<div class="main">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-12">--}}
    {{--<div id="footer-brand">--}}
    {{--<ul class="magicbrand">--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_01" title="brands_01" /> </a></li>--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_02" title="brands_02" /> </a></li>--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_03" title="brands_03" /> </a></li>--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_04" title="brands_04" /> </a></li>--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_05" title="brands_05" /> </a></li>--}}
    {{--<li> <a href="#"> <img class="brand img-responsive" src="http://placehold.it/190x80/ffffff" alt="brands_06" title="brands_06" /> </a></li>--}}
    {{--</ul>--}}
    {{--</div><!-- #footer-brand -->--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div><!-- .container-->--}}
    {{--</div><!-- .alo-brands -->--}}
@endsection