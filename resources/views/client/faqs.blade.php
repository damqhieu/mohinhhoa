@extends('layouts.app')
@section('content')
    @php
        $faq_banner = \App\Models\System\Setting::select('value')->where('key','faq_banner')->first();
    @endphp
    {{--<div class="about-us-top">--}}
    {{--<!---<div class="title">--}}
                {{--<div class="top">{{ isset($ isset($about_banner->value) ? $about_banner->value->value) ? $about_banner->value : '' }}</div>--}}
                {{--<h1>{{ isset($about_banner_title->value) ? $about_banner_title->value : '' }}</h1>--}}
                {{--<div class="bottom">{{ isset($about_banner_subtitle->value) ? $about_banner_subtitle->value : '' }}</div>--}}
            {{--</div>-->--}}
    {{--</div><!--- .about-us-top-->--}}
    <div class="banner">
        <img src="{{ (isset($faq_banner->value)) ? asset($faq_banner->value) : 'client/assets/images/bg-about-us.jpg' }}"
             class="img-responsive" width="100%" height="430" alt="FAQS">
    </div>

    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>FAQ</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="row">
            <div class="faqs">
                <div data-q_id="#bus" class="row" style=' text-align:left;'>
                    <div class="col-sm-12">
                        <h1 class="title"> Car-Asia FAQs</h1>
                        @foreach($faqs as $item)
                            <div class="answer"><div class="a">Q:</div><div class="message"><p>{{ $item->question }}</p></div>
                            </div>
                            <div class="answer">
                               <div class="a">A:</div><div class="message">{!! $item->answer !!}</div>
                            </div>
                        <div class="clearfix"></div>
                            <div class="separator  normal center  " style="background-color: #c3c1c1;height:1px;"></div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('style')
    <style>
        .faqs {
            box-shadow: 1px 1px 1px 2px #c7bebe;
            padding: 20px;
        }

        h1.title {
            padding-top: 30px;
            font-family: 'Open Sans', sans-serif;
            color: #ee1b24;
            font-size: 36px;
        }

        .question {
            color: #000;
            display: table;
            font-weight: bold;
            font-size: 16px;
            margin-bottom: 3px;
            padding: 9px 16px 8px 20px;
            position: relative;
            text-decoration: none;
        }

        .question span {
            font-size: 14px;
            color: #000;
            font-weight: bold;
            margin-right: 10px;
        }
        .a{
            margin-right: 8px;
            float: left;
            width: 4%;
            font-weight: bold;
        }
        .message{
            float: left;
            width: 92%;
        }
        .answer {
            padding: 20px 20px 0;
            position: relative;
            margin-bottom: 20px;
            font-size: 14px;
        }

        .answer p {
            font-size: 14px;
            color: #000;
            font-weight: bold;
            margin-right: 10px;
        }
        .about-us-top {
            background: url({{ (isset($page->img_banner)) ? asset($page->img_banner) : 'client/assets/images/bg-about-us.jpg' }}) top center no-repeat;
            padding: 325px 15px 105px;
        }

        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 360px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 480px) {
            .banner img{
                height: 100px;
            }
        }

    </style>
@endsection
