@extends('layouts.app')
@section('content')
    <div class="banner">
        <img src="{{ (isset($page->img_banner)) ? asset($page->img_banner) : 'client/assets/images/bg-about-us.jpg' }}"
             class="img-responsive" width="100%" height="430" alt="{{ $page->title }}">
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>Terms and Conditions</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <section class="container shades" style="padding:20px 20px;  margin-bottom:25px;">
      @php
         $html = '  <div><strong><span style="color: #ff0000; text-decoration: underline;">IMPORTANT:</span></strong></div>
        <p align="justify">Please read this agreement carefully and thoroughly. By browsing, accessing or using this website or by using any facilities or services made available through it or by transacting through or on it, you are deemed to have read, understood and agreed to observe, comply and be bound with the terms and conditions that appear below (also knows as the “Agreement”). This Agreement is made between you and us.</p>
        <h3 align="justify">General Conditions of Carriage</h3>
        <p align="justify">These General Conditions of Carriage are the conditions on which SkyBus carries any person and their property and these conditions shall apply to each ticket issued by SkyBus and each contract to carry any person entered into by SkyBus. Any person who travels on a SkyBus service shall be considered to have agreed to be carried on these General Conditions of Carriage.</p>
        <h3><strong><br>
            </strong><span style="text-decoration: underline;">1. INTERPRETATION</span></h3>
        <p>&nbsp;</p>
        <h4>1.1 Definitions</h4>
        <p>In these General Conditions of Carriage, the following words shall have the following meanings:-
            <br> “coach” means the coach, bus or other road vehicle or other means of transport provided by us, or any other carrier on which you are travelling; “journey” means each journey you are entitled to make on a service as set out in your ticket; “luggage” means any property, which you bring onto a coach or into a station, including any property, carried on your person; “service” means any journey to be made by a coach provided or arranged by us or on our behalf for the purpose of carrying persons and their luggage, which is set out in a timetable published by us; “Special Conditions” means any additional or special condition relating to a particular ticket or the method of delivery of a ticket (including any restrictions as to the services, dates, days of the week, and times in the day on which travel is permitted and conditions to advance reservations of seats) as set out in any notices, offers or publications from ourselves.;”station” means any coach or railway station or airport or stop where a service is to be commenced or completed ; “ticket” means any ticket issued by us or on our behalf, which sets out our agreement to carry or arrange for the carriage of any person, including the services on which travel is permitted and the fare payable;”we” , “us” and “our” refers to SkyBus Ventures Sdn. Bhd a company registered in Malaysia, with registered number 560449-D, whose registered office is at No.20-3A, Jalan Pandan 2/1, Pandan Jaya, 55100 Kuala Lumpur, Malaysia, including the trade name, SkyBus;&nbsp;“working day” means a day other than a Saturday or Sunday on which the clearing banks in Malaysia are open to the public for the transaction of business; “you” means the person who we have agreed to carry or arranged to be carried, being the person who purchased a ticket or for whom a ticket was purchased, or any person who travels on a service with or without a ticket.</p>
        <h4>1.2 References</h4>
        <p>In this Agreement, a reference to the singular shall include the plural and vice versa.</p>
        <p>&nbsp;</p>
        <h3><span style="text-decoration: underline;">2. CARRIAGE SERVICES</span></h3>
        <p>&nbsp;</p>
        <h4>2.1 Our agreement to carry you</h4>
        <p>We agree to carry you and your luggage on the journey permitted by your ticket, on and subject to these General Conditions of Carriage and any Special Conditions applicable to your ticket. The applicable Special Conditions shall take precedence over these General Conditions of Carriage.</p>
        <h4>2.2 Carriage of children and your persons</h4>
        <p>We will not be obliged to carry any child under 14 years of age unless that child is accompanied by a responsible person aged 18 or over. Children under 3 years of age and not occupying a seat may travel free if accompanied by a full fare paying passenger over the age of 18. Children over the age of 3 require a ticket to be able to travel on our services.</p>
        <h4>2.3 Your ticket</h4>
        <p>Your ticket is a record of our agreement to carry you or to arrange for your carriage. Your ticket is our property, and shall be returned to us on request. If your ticket was purchased by someone else, you agree that such person purchased the ticket as your agent. A ticket may only be used by the person(s) named in it or for whom it has been purchased, and may not be transferred to or used by anyone else.</p>
        <h4>2.4 Validity of your ticket</h4>
        <p><span style="color: #993300;"><strong><span style="color: #ff0000;">(a) Travel permitted by your ticket:</span> </strong>
            </span>Your ticket permits you to make the journeys and travel on the services stated on the ticket, subject to any restrictions or statements as to the services, dates, days of the week, and times within a day on which you may travel, set out on the ticket or in any Special Conditions applicable to the ticket.</p>
        <p><span style="color: #ff0000;"><strong>(b) Ownership of ticket:</strong></span> Your ticket remains our property at all times and if a ticket is defaced, damaged or tampered with, or lost, it is not valid for travel. We reserve the right to refuse to issue a replacement ticket in such circumstances. If we exercise our right to refuse to issue a replacement ticket, we will notify you within 7 days of so refusing setting out the reason for withdrawing the ticket.</p>
        <p><span style="color: #ff0000;"><strong>(c) e-ticket validity:</strong></span> The e-ticket is valid for the next departure time on the same day, considering that it is from the original location.
            <br> In case of delays, e-ticket is valid for the next departure time on the same day, considering that it is from the original location.
            <br> In the event the passenger misses the last departure of the scheduled date as a result of delays, the ticket is only valid for the next day only and only for the first departure.</p>
        <h3><span style="text-decoration: underline;">3. PASSENGER RESPONSIBILITIES</span></h3>
        <p>&nbsp;</p>
        <h4>3.1 You must travel with your ticket</h4>
        <p>You must take your ticket with you whenever you travel on a service, and you must produce your ticket for inspection when asked. If you do not take your ticket with you when you travel, or do not produce your ticket when asked then you will be considered to have travelled without a ticket.</p>
        <h4>3.2 You must travel with a valid ticket</h4>
        <p><span style="color: #ff0000;"><strong>(a) Travel without a valid ticket:</strong></span> You must travel with a valid ticket. You will be considered to have travelled without a valid ticket if you travel without a ticket at all or fail to purchase a ticket prior to the departure of the service, or you travel with a ticket which you are not entitled to or you travel without a ticket which you have purchased, or you travel on any service on which your ticket does not permit travel, or you travel in breach of the General Conditions of Carriage or any Special Conditions applicable to your ticket, or you travel further than your ticket permits, or you travel with a ticket which is declared by these General Conditions of Carriage or any Special Conditions to be invalid.</p>
        <p><span style="color: #ff0000;"><strong>(b) Effect of travelling without a valid ticket:</strong></span> We will not allow you to board a service if you do not have a valid ticket. If you do travel on any service without a valid ticket, you shall leave the service when asked, and we may remove you from the coach if you refuse, unless you immediately purchase a valid ticket for your journey and you pay the full appropriate fare for the journey which you are making.</p>
        <h4>3.3 You must take care of your ticket:</h4>
        <p><span style="color: #ff0000;"><strong>(a) Lost Tickets:</strong></span> We will not be obliged to replace your ticket if it is lost, mislaid or stolen, You will be required to purchase a new ticket to enable you to travel.</p>
        <p><span style="color: #ff0000;"><strong>(b) Spoiled or Tampered Tickets:</strong></span> If your ticket is spoiled or tampered with, it will be invalidated, and if you travel with it, you will be considered to have travelled without a ticket. If your ticket is spoiled or tampered with before you travel, then we will sell you another ticket if you ask for a replacement. We may refuse to replace your ticket if it is reasonable to do so. We may charge you a reasonable administration fee for replacing you ticket.</p>
        <h4>3.4 You must make sure you are on the correct service</h4>
        <p>You are responsible for making sure that you meet any service on which you are travelling at the relevant boarding point.</p>
        <h4>3.5 You should arrive at the boarding point at least 10 minutes before departure</h4>
        <p><span style="color: #ff0000;"><strong>(a) Arrival :</strong></span> You should arrive at the boarding point for a service at least 10 minutes prior to the timetable departure time for that service.</p>
        <h4>3.6 You must allow sufficient time for connections</h4>
        <p>You must allow plenty of time for a service to arrive in time to connect with any other services:
            <br> (a) airlines suggest that passengers be at the airport at least 2 hours before the departure of their scheduled flight.
            <br> (b) You must allow plenty of time for a service to arrive in time to connect with other forms of transport provided by other carriers on which you are planning to travel. Where such other form of transport involves air travel we recommend you allow at least 2 hours before your flight check in time;</p>
        <h4>3.7 Joining and leaving a service</h4>
        <p>You may not board or leave any service except at the starting, or finishing, point of your journey.</p>
        <h4>3.8 Breach of conditions applicable to your ticket</h4>
        <p>If you fail in a material respect to comply with any condition that governs your ticket, we may cancel the ticket, and refuse you further carriage, without any obligation to refund the fare or other liability to you.</p>
        <h4><strong>3.9 Onboard the vehicle</strong></h4>
        <p>No smoking is allowed.</p>
        <p>Fruits are permitted to be brought onboard so long it is nicely sealed and/or packed. However, durian, mangosteen, jackfruit and fruits with pungent smell which are universally banned are not permitted to be brought onboard.</p>
        <div></div>
        <h3><strong><br>
            </strong><span style="text-decoration: underline;">4. SKYBUS RESPONSIBILITIES</span></h3>
        <p>&nbsp;</p>
        <h4>4.1 Our obligation to carry you</h4>
        <p>It is our obligation to carry you and your permitted luggage on the journeys permitted by your ticket, on and subject to these General Conditions of Carriage and any Special Conditions. We will make reasonable efforts to carry you with the minimum discomfort and inconvenience.</p>
        <h4>4.2 We will not carry animals</h4>
        <p>We will not carry dogs or any other animals on any of our services.</p>
        <h4>4.3 Timetable of services</h4>
        <p>The published running times of any service are only stated approximately and we will use reasonable endeavours to minimise any disruption to your journey. In the event that our services are delayed or cancelled we will notify you of the delay and any alternative timetable as soon as reasonably practicable.</p>
        <h4>4.4 Our right to cancel</h4>
        <p>We reserve the right to alter any timetables or suspend, cancel or withdraw services, or terminate a service once it has commenced, without notice whether before or after you have reserved a seat on the service, and to substitute an alternative service.</p>
        <h4>4.5 Our liability for cancellations and withdrawals of services</h4>
        <p><span style="color: #ff0000;"><strong>(a) Our liability is limited to what is stated in these Conditions:</strong></span> Except as provided in these General Conditions of Carriage, we shall not be liable for any loss, damage, liability, or cost suffered by you as a result of any cancellation or withdrawal of any service by us, or any delay to any service, or termination of any service.</p>
        <p><span style="color: #ff0000;"><strong>(b) No liability if you have no reservation:</strong> </span>If we cancel or withdraw a service before it has commenced and you do not have a seat reserved on it, we shall have no liability to you.</p>
        <p><span style="color: #ff0000;"><strong>(c) Cancellation before service has begun:</strong> </span>If we cancel or withdraw a service before it has commenced motives or circumstances beyond our reasonable control and you have a seat reserved on it, our liability will be at our discretion to:</p>
        <p style="padding-left: 30px;">(i) carry you on another service with available seats and where necessary extend the validity of your ticket;
            <br> (ii) make suitable alternative arrangements to carry you to your destination on another coach, or other mode of transport as we deem fit; or</p>
        <p><span style="color: #ff0000;"><strong>(d) Cancellation after service has begun:</strong></span> If a service on which you are travelling commences and is terminated before reaching your destination, other than for a reason outside our control, our liability will be at our option to:</p>
        <p style="padding-left: 30px;">(i) make suitable alternative arrangements to carry you to your destination, such as another service, carrier, coach, train, private car, or taxi, which you shall not unreasonably refuse; or
            <br> (ii) provide a substitute coach, which may lack all of the advertised facilities; or
            <br> (iii) provide refund or reschedule.</p>
        <h4>4.6 We have no liability for circumstances beyond our control</h4>
        <p>You are strongly advised to remain seated throughout the journey. Any injury/damaged caused (physically, mentally or both) due to the passenger’s ignorance of his own safety in a moving vehicle renders us not liable.</p>
        <p>We shall have no liability for any delay or failure to carry you, or for breach of contract, where caused by a circumstance beyond our reasonable control. The following shall be considered to be circumstances beyond our reasonable control: war or threat of war, accidents causing delays on the service route, exceptional severe weather conditions, fire and/or damage at a station, compliance with requests of the police, customs or other government officials and security services, deaths and accidents on the road, vandalism and terrorism, unforeseen traffic delays, strike/industrial action, riot or local disturbance or unrest, problems caused by other customers, bankruptcy, insolvency or cessation of trade of any carrier used by us and other circumstances affecting passenger safety.</p>
        <h4>4.7 Our maximum liability to you</h4>
        <p>Our maximum liability to you for any reasonable and foreseeable loss, damage or liability (including but subject to the limitation set out in Clause 6.11 for loss or damage to your luggage) which you may suffer or incur as a result of our failure to carry you, our delay in carrying you, breach of our contract to carry you, our negligence in connection with carrying you, or the deliberate or negligent acts or omissions of any of our officers, employees, agents, representatives or sub-contractors, shall be limited to an aggregate of [RM1000].</p>
        <h4>4.8 Death and Personal Injury</h4>
        <p>We do not exclude or limit our liability for death or personal injury resulting from our negligence, nor where you deal as a consumer exclude your statutory rights.</p>
        <p>&nbsp;</p>
        <h3><span style="text-decoration: underline;">5. REFUNDS AND RESCHEDULES</span></h3>
        <p>&nbsp;</p>
        <h4>5.1 Delays</h4>
        <p>In the event of general delays, the ticket is valid for any other departure time from what is stated on the ticket for any date of travel and from the original location only.&nbsp;In the event the passenger misses the last departure of the scheduled date as a result of delays, the ticket is valid for any day and for any departure.</p>
        <h4>5.2 What refunds and reschedules are allowed</h4>
        <p>You are not entitled to cancel or reschedule your ticket, and we shall not be obliged to refund to you any fare for your ticket in any circumstances, except for any refunds expressly allowed in these General Conditions of Carriage or any Special Conditions, 4.5 (d).</p>
        <h4>5.3 Refunds and reschedules for delay or cancellation of service</h4>
        <p>If the coach you were booked to travel on is delayed or cancelled without notice or there are insufficient seats or a standby coach service is not available and as a consequence you reasonably decide not to travel, you may claim a refund or reschedule if you return the ticket to the SkyBus head office or in accordance with the general rules set out in Clause 5.5 below. If you have used the outward part of a return ticket we will refund 50% of the return fare, meaning you have used one journey of a round trip ticket. In case of late flight arrivals, the ticket is valid for any departure time from the original location and for any date the ticket was to be used for travel. In the event the passenger misses the last departure of the day because of a late flight arrival, the ticket is valid for any departure on any day from the original location only.</p>
        <p>&nbsp;</p>
        <h3><span style="text-decoration: underline;">6. LUGGAGE</span></h3>
        <p>&nbsp;</p>
        <h4>6.1 Permitted Luggage</h4>
        <p>We will carry your luggage on and subject to these General Conditions of Carriage and any applicable Special Conditions. You are allowed to take onto a service one or two medium sized suitcases or rucksacks (no more than 20kg per item, and one small piece of hand luggage). In this context, hand luggage means something that is capable of fitting in an overhead luggage rack or under seats. We shall have no obligation to carry luggage in excess of the permitted amount. We may agree to carry additional luggage such as skis and folding/dismantled bicycles, subject to available accommodation provided that they are packed in a purposely made, fully protective wrapping and collapsible manual wheelchairs. If we agree to carry any particular luggage on any journey this does not mean that we have agreed to carry that luggage on any subsequent journey you make. Fragile items such as electrical goods, portable televisions and radio will only be carried if they are of reasonable size and securely fastened. Drivers are not obliged to lift your luggage on or off coaches, but will offer reasonable assistance to you, except where in the reasonable opinion of the driver, your luggage exceeds the recommended weight, whereby you will be responsible for the lifting of your luggage, on and off the coach.</p>
        <h4>6.2 Prohibited contents</h4>
        <p><span style="color: #ff0000;"><strong>(a) Prohibited luggage:</strong></span> We are not obliged to carry any of the following items of luggage, and you may not bring them onto any coach without our permission: any weapons, drugs or solvents (other than medicines), live or dead animals, fish or insects, battery powered wheelchairs / disabled scooters, prams, non folding pushchairs, non-folding bicycles and surfboards, or any items which are in our opinion are unsafe, or may cause injury or damage to property, or which are considered by us to be unsuitable for carriage by reason of their weight, size, shape or character, or which are fragile or perishable, or items with sharp or protruding edges, or any item over 20kg in weight.</p>
        <p><span style="color: #ff0000;"><strong>(b) What happens if you take prohibited luggage:</strong> </span>If you take any of these items onto any coach, we may remove them from the coach immediately on discovery, and leave them outside the coach, wherever they may be situated. If you have any doubts as to whether we will carry any particular item, you should obtain our written confirmation before purchasing your ticket. If you take any prohibited items of luggage onto a coach or into a station, we shall not be liable for any loss or damage occurring to such items for any reason whatsoever.</p>
        <h4>6.3 Packing and identification of luggage</h4>
        <p>You must pack all of your luggage safely and securely, and lock and fasten it, with a view to protecting your luggage from loss, damage or interference, and to protecting any other property on a coach from being damaged by your luggage. All luggage which is given into our custody should be clearly and appropriately labelled and include a contact telephone number where possible. We will not be obliged to carry any luggage that has not been properly packed or labelled.</p>
        <h4>6.4 Inspection of luggage</h4>
        <p>We shall be entitled to inspect all of your luggage, for the purpose of ensuring compliance with the above requirements. We shall not be obliged to carry you or your luggage, and shall be entitled to remove you from any coach, if you refuse to submit to a search.</p>
        <h4>6.5 Storage of luggage</h4>
        <p>All luggage other than hand luggage will be stored in any hold or other storage compartment on the coach, and not in the passenger compartment of the coach.</p>
        <h4>6.6 Getting the luggage onto a service</h4>
        <p>You are responsible for getting your luggage onto and off a coach. It is your responsibility to see your luggage put on and taken off a coach, or checked-in at any station where check-in arrangements apply. Except for any luggage stored in the hold of a coach, you must also look after your luggage at all times, including at any station and your hand luggage whilst on a coach.</p>
        <h4>6.7 Small valuables and important items</h4>
        <p>Small valuable items should not be stowed in the luggage hold under any circumstances but should be taken on board a coach as hand luggage. Small valuable items includes money, medication, jewellery, precious metals, laptop computers, personal electronic devices, negotiable papers, securities or other valuables, business documents, passports, visas, tickets and identification documents. Small valuable items may be placed in the overhead racks or under your seat, but should not be left unattended by you. Where possible, small valuable items should be carried on your person.</p>
        <h4>6.8 Luggage</h4>
        <p><span style="color: #ff0000;"><strong>6.8.1 Our liability for luggage:</strong></span>
            <br> If you leave behind or lose any of your luggage on a coach or at a station we shall take reasonable care of that luggage.</p>
        <p><span style="color: #ff0000;"><strong>6.8.2 We will store luggage:</strong></span>
            <br> If we find any of your luggage on a coach or at a station, we will store it at such location as we may decide, but all such storage will be at your risk. We may charge you a reasonable administration fee for the storage and return of your lost luggage.</p>
        <p><span style="color: #ff0000;"><strong>6.8.3 Our right to dispose of luggage:</strong></span>
            <br> If you have not collected your lost luggage within 1 month of our receiving or finding it, we may dispose of your luggage in any manner we wish, including by destruction or sale, and we shall be entitled to keep any sale proceeds. We shall be entitled to open and examine any left or lost luggage. If there are any items which we consider are dangerous or perishable or otherwise unsuitable for storage, we shall be entitled to dispose of those perishable items within 48 hours of our receiving or finding such items.</p>
        <p><span style="color: #ff0000;"><strong>6.8.4 You must hand over lost luggage of other people:</strong></span>
            <br> If you find any property of any other person on any coach or at any station, you must hand it over to us immediately. If you find any property on any coach you shall hand it over to the coach driver where possible.</p>
        <h4>6.9 Notification of loss or damage</h4>
        <p>If during any journey you should lose any of your luggage or any of your luggage is damaged you must notify a member of our staff as soon as possible after your discover the loss or damage. You must confirm any loss or damage of your luggage within 72 hours after the end of your journey through&nbsp;<a href="www.skybus.com.my/contact-us.html">www.skybus.com.my/contact-us</a>&nbsp;. If you do not notify us of any loss or damage to your luggage as required, then we will not be liable for that loss or damage.</p>
        <h4>6.10 Our liability for loss or damage to luggage</h4>
        <p>Your luggage shall be at your risk at all times, and we will only be liable for any loss of or damage to your luggage caused by our negligence. Our maximum liability to you for any loss of or damage to your luggage, whether for breach of contract, breach of any duty of care in relation to the luggage, our negligence, or any deliberate or negligent acts of any of our employees, agents or representatives, shall be limited to RM500 for all such loss or damage. You should insure your luggage with reputable insurers against all usual and normal risks of loss or damage, to the full replacement value of the luggage, with no excess.</p>
        <p>&nbsp;</p>
        <h3><span style="text-decoration: underline;">7. PASSENGER BEHAVIOUR</span></h3>
        <p>&nbsp;</p>
        <h4>7.1 Required behaviour and prohibited behaviour</h4>
        <p>You shall behave in a reasonable, sensible and lawful manner on a coach and at any station, comply with any request from a member of staff concerning the availability of certain seats reserved for disabled passengers, use mobile phones considerately with the comfort of other passengers in mind and you shall not: be abusive or threatening to any staff or any other person; or conduct yourself in a way which may endanger yourself, any coach or station or any person or property on board any coach or at any station; or obstruct any driver, crew, officer or staff in the performance of their duties or fail to comply with their instructions; or behave in a manner which causes discomfort, inconvenience, damage or injury to other persons; or obstruct or allow any of your luggage to obstruct any aisle or emergency exit; or play any radios, cassette or CD players, personal stereos, musical instruments or radio or electronic devices that are audible and distracting or annoying to any person or which interfere with or render less audible any public address system or other equipment, or take onto any coach or into any station any alcoholic drinks or drugs (other than medicines) for the purpose of consuming them, or consume them on any coach or in any station, or consume or take any hot food onto the coach, such as chips, or other takeaway foods; or board any coach whilst under the influence of alcoholic drinks or drugs; or smoke; or board any coach whilst you are seriously ill or suffering from any serious contagious illness.</p>
        <h4>7.2 Consequences of bad behaviour</h4>
        <p>If you fail to comply with any of the behaviour rules in Clause 8.1, or we have reason to believe you may continue any conduct in breach of those behaviour rules, we shall be entitled to restrain you, remove you from the coach or station, refuse you further carriage, cancel your ticket without refund, and take any other measures as we consider necessary to prevent continuation of such conduct.</p>
        <h4>7.3 Our liability for behaviour of other passengers</h4>
        <p>Whilst we will use our reasonable efforts to control the behaviour of other passengers on a coach or at a station, we will not be liable to you for any act or omission of any other passenger on a coach or at a station.</p>
        <p>&nbsp;</p>
        <h3><span style="text-decoration: underline;">8. GENERAL</span></h3>
        <p>&nbsp;</p>
        <h4>8.1 Governing Law</h4>
        <p>Your ticket, the contract with us to carry you, and any carriage we provide to you shall be governed by Malaysia law, and the Malaysia courts shall have exclusive jurisdiction.</p>
        <h4>8.2 Severability</h4>
        <p>Each of the provisions of these General Conditions of Carriage shall be separate and severable. Should any provision be invalid or unenforceable, it shall be severed from these General Conditions of Carriage, and the remaining provisions of these General Conditions of Carriage shall continue in full force and effect and be amended as far as possible to give valid effect to the intentions of the parties under the severed provision.</p>
        <h4>8.3 Your personal data</h4>
        <p>We are in compliance with&nbsp;<a href="uploads/Malaysian-Personal-Data-Protection-Act-2010-Policy.pdf" target="_blank">Malaysian Personal Data Protection Act 2010</a> Policy. <a href="uploads/Malaysian-Personal-Data-Protection-Act-2010-Policy.pdf" target="_blank">Click here</a> to read a full Policy which we are applying to protect your personal data.</p>
        <p>Your personal data (including details of and copies of your travel documentation) given to us by you or our agents or representatives may be kept on record by us, and used and disclosed by us for the purposes of administering your ticket, purchasing tickets for your carriage by carriers other than us, making seat reservations for you for travel on any service, administering, performing and exercising any rights under your carriage contract with us, these General Conditions of Carriage and any Special Conditions, and complying with any legal obligations we may have to make available such data to government agencies or other persons in connection with your travel. In carrying out this purpose, we may disclose your personal data to our own offices, our agents and representatives, sub-contractors, government agencies, and any other carriers. You consent to this receipt, use, disclosure and transfer of your personal data.</p>
        <h4>8.4 Amendments and waivers</h4>
        <p>None of our employees, agents, or representatives, has authority to alter, modify or waive any provision of these General Conditions of Carriage or any Special Conditions.</p>
        <h4>8.5 Third Party Rights</h4>
        <p>Unless otherwise stated in these General Conditions of Carriage, no person other than you and us shall have the benefit of or be entitled to rely upon or enforce any term of these General Conditions of Carriage or any other term of the contract to carry you and the Contracts (Rights of Third Parties) shall not apply</p>
        <h2>9. Promotions &amp; Special Offers</h2>
        <p>All promotions and discounts are valid only for bookings completed via www.skybus.com.my unless indicated otherwise. </p>
        <p>Any tickets purchased via different sources than the www.SkyBus.com.my domain itself are not eligible to fall under the SkyBus terms &amp; conditions. For example, if SkyBus decided to sell its tickets at a discount, then only the tickets bought on SkyBus.com.my are entitled to this discount. If you buy your ticket via AirAsia.com or any other SkyBus reseller, you are not entitled to receive a discount.</p>
        <p>All promotional offers are time-limited and Skybus has the right to end the promotion at any time without prior notice. </p>
        <p>The same terms &amp; conditions as mentioned on this page apply.</p>'
      @endphp

        {!! (isset($page->content)) ? $page->content : $html !!}
    </section>

@endsection

@section('style')
    <style>
        .shades {
            font-family: Arimo, Arial, sans-serif;
            color:#333;
        }
        .shades {
            box-shadow:1px 3px 4px 5px #ccc;
        }
        .shades h1, .shades h2, .shades h3, .shades h4, .shades h5, .shades h6 {
            font-family: 'Open Sans', sans-serif  !important ;
            color: #ee1b24 !important;
            font-weight: 100;
        }
        .shades h3{
            font-size: 22px !important ;
        }

        .about-us-top {
            background: url({{ (isset($page->img_banner)) ? asset($page->img_banner) : 'client/assets/images/bg-about-us.jpg' }}) top center no-repeat;
            padding: 325px 15px 105px;
        }

        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 360px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 480px) {
            .banner img{
                height: 100px;
            }
        }
    </style>
@endsection
