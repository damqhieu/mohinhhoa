@extends('layouts.app')

@section('content')
    @php
        $most_popular_routes = \App\Models\System\Setting::select('value')->where('key','most_popular_routes')->first();
        $new_trending = \App\Models\System\Setting::select('value')->where('key','new_trending')->first();
        $new_blog = \App\Models\System\Setting::select('value')->where('key','new_blog')->first();
    @endphp
    <div class="alo-block-slide">
        <div class="container-none flex-index">
            <div class="flexslider">
                <div id="slider-index" class="slides">
                    @if($cmsSlide->count() != 0)
                        @foreach($cmsSlide as $item)
                            <div class="item">
                                <a href="javascript:;"  title="{{ $item->title }}"><img
                                            src="{{ (isset($item->image)) ? asset($item->image) : 'client/assets/images/slide1.jpg' }}"
                                            alt="{{ $item->title }}" ></a>
                            </div>
                        @endforeach
                    @else
                        <div class="item">
                            <a href="javascript:;"  title="Car-Asia"><img src="client/assets/images/slide1.jpg" alt="Car-Asia"></a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div><!--- .alo-block-slide-->
    <div class="booking">
        <div class="main-agileinfo">
            <div class="sap_tabs">
                <div id="horizontalTab">
                    <ul class="resp-tabs-list">
                        <li class="resp-tab-item"><span>Round Trip</span></li>
                        <li class="resp-tab-item"><span>One way</span></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="resp-tabs-container">
                        <div class="tab-1 resp-tab-content roundtrip">
                            <form action="{{ route('payment.handle-booking') }}" method="post">
                                @csrf
                                <input type="hidden" name="type" value="return">
                                <div class="trip">
                                    <h3>Trip</h3>
                                    <select name="trip_id" class="frm-field trip required">
                                        @foreach($trips as $item)
                                            <option  value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="clear"></div>
                                <div class="date">
                                    <div class="depart">
                                        <h3>Depart</h3>
                                        <input id="datepicker" name="departure_date" type="text" value="{{ date('m/d/Y') }}"
                                               onfocus="this.value = '';"
                                               onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                                        <span class="checkbox1">
										<label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Flexible with date</label>
									</span>
                                    </div>
                                    <div class="return">
                                        <h3>Return</h3>
                                        <input id="datepicker1" name="return_date" type="text" value="{{ date('m/d/Y', strtotime('+1 day')) }}"
                                               onfocus="this.value = '';"
                                               onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                                        <span class="checkbox1">
										<label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Flexible with date</label>
									</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                {{--<div class="clear"></div>--}}
                                <div class="numofppl">
                                    <div class="adults">
                                        <h3>Adult:(7+ yrs)</h3>
                                        <div class="quantity">
                                            <div class="quantity-select">
                                                <div class="entry value-minus">&nbsp;</div>
                                                <div class="entry value"><span>1</span></div>
                                                <input type="hidden" id="quantity_adult" name="quantity_adult" value="1">
                                                <div class="entry value-plus active">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="child">
                                        <h3>Child:(2-6 yrs)</h3>
                                        <div class="quantity">
                                            <div class="quantity-select">
                                                <div class="entry value-minus">&nbsp;</div>
                                                <div class="entry value"><span>0</span></div>
                                                <input type="hidden" id="quantity_child" name="quantity_child" value="0">
                                                <div class="entry value-plus active">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                                <span class="blink"><input type="submit" value="Booking"></span>
                            </form>
                        </div>
                        <div class="tab-1 resp-tab-content oneway">
                            <form action="{{ route('payment.handle-booking') }}" method="post">
                                @csrf
                                <input type="hidden" name="type" value="oneway">
                                <div class="trip">
                                    <h3>Trip</h3>
                                    <select name="trip_id" class="frm-field trip required">
                                        @foreach($trips as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="clear"></div>
                                <div class="date">
                                    <div class="depart">
                                        <h3>Depart</h3>
                                        <input class="date" id="datepicker2" name="departure_date" type="text"
                                               value="{{ date('m/d/Y') }}"
                                               onfocus="this.value = '';"
                                               onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required>
                                        <span class="checkbox1">
										<label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Flexible with date</label>
									</span>
                                    </div>
                                </div>
                                {{--<div class="clear"></div>--}}
                                <div class="numofppl">
                                    <div class="adults">
                                        <h3>Adult:(7+ yrs)</h3>
                                        <div class="quantity">
                                            <div class="quantity-select">
                                                <div class="entry value-minus">&nbsp;</div>
                                                <div class="entry value"><span>1</span></div>
                                                <input type="hidden" id="quantity_adult" name="quantity_adult" value="1">
                                                <div class="entry value-plus active">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="child">
                                        <h3>Child:(2-6 yrs)</h3>
                                        <div class="quantity">
                                            <div class="quantity-select">
                                                <div class="entry value-minus">&nbsp;</div>
                                                <div class="entry value"><span>0</span></div>
                                                <input type="hidden" id="quantity_child" name="quantity_child" value="0">
                                                <div class="entry value-plus active">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                                <span class="blink"><input type="submit" value="Booking"></span>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="main">
            <div class="row">
                <div class="col-main col-lg-12">
                    <div class="std">
                        <div class="row">
                            <div class="pa-lines">
                            </div>
                        </div>
                        <div class="block-custom block-custom1">
                            <div class="block-title" style="padding-top:0px">
                                <h2>
                                    <span class="title-top">{{ (isset($most_popular_routes->value)) ? $most_popular_routes->value : 'Most Popular Route' }}</span>
                                </h2>
                            </div>
                            <div class="featured-product-tab">
                                <div class="magicproduct">
                                    {{--<div class="block-title-tabs">--}}
                                        {{--<ul class="magictabs">--}}
                                        {{--<li class="item a   ctive" data-type ="mc-featured"><span class ="title">Featured</span></li>--}}
                                        {{--<li class="item" data-type ="mc-bestseller"><span class ="title">Top Rated</span></li>--}}
                                        {{--<li class="item" data-type ="mc-latest"><span class ="title">Latest</span></li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                    <div class="content-products">
                                        <div class="mage-magictabs mc-featured active">
                                            <div class="flexisel-content products-grid featured zoomOut play">
                                                <ul class="products-grid-rows">
                                                    @foreach($trips as $item)
                                                        <li class="item item-animate">
                                                            <div class="per-product">
                                                                <div class="images-container">
                                                                    <div class="product-hover">
                                                                        @if($item->is_hot)
                                                                            <span class="sticker top-right">
																					<span class="labelhot">Hot</span>
																				</span>
                                                                        @elseif($item->is_new)
                                                                            <span class="sticker top-right">
																					<span class="labelnew">New</span>
																				</span>
                                                                        @elseif($item->is_top)
                                                                            <span class="sticker top-right">
																					<span class="labelsale">Sale</span>
																				</span>
                                                                        @endif
                                                                        <a href="{{ route('booking', str_slug($item->name)) }}"
                                                                           target="_blank" title="{{ $item->name }}"
                                                                           class="product-image">
                                                                            <img class="img-responsive"
                                                                                 src="{{ (isset($item->image)) ? asset($item->image) : 'placehold/278x355.png' }}"
                                                                                 width="278" height="355"
                                                                                 alt="{{ $item->name }}"/>
                                                                        </a>
                                                                    </div>
                                                                    <div class="actions-no hover-box">
                                                                        <div class="actions">
                                                                            <a href="{{ route('booking', str_slug($item->name)) }}" title="{{ $item->name }}"> <button style="height: 35px; width: 100%" type="button" title="Book Now"
                                                                                                class="button btn-cart pull-left">
                                                                                    <span><i class="icon-handbag icons"></i><span>Book Now</span></span>
                                                                                </button></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="products-textlink clearfix">
                                                                    <div class="price-box">
                                                                        @if(!empty($item->price_sale))
                                                                            <p class="old-price">
                                                                                <span class="price-label">Regular Price:</span>
                                                                                <span class="price"> {!! $item->currency  . "<span style='color:red;'>$item->price</span>" !!} </span>
                                                                            </p>
                                                                            <p class="special-price">
                                                                                <span class="price-label">Special Price</span>
                                                                                <span class="price"> {!!  $item->currency  . "<span style='color:red;'>$item->price_sale</span>"  !!} </span>
                                                                            </p>
                                                                        @else
                                                                            <p class="special-price">
                                                                                <span class="price-label">Special Price</span>
                                                                                <span class="price"> {!! $item->currency  . "<span style='color:red;'>$item->price</span>" !!} </span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    <h3 class="product-name">
                                                                        <a href="{{ route('booking', str_slug($item->name)) }}"
                                                                           target="_blank" title="{{ $item->name }}"><span class="blink">{{ $item->name }}</span></a>
                                                                    </h3>

                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div><!-- .mc-featured -->


                                    </div><!-- .content-products -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--- .main-->
    </div><!--- .container-->

    <div class="block-static block_trending">
        <div class="container">
            <div class="main">
                <div class="row">
                    <div class="trending col-lg-12">
                        <div class="block-custom block-custom2">
                            <div class="block-title">
                                <h2>
                                    <span class="title-top">{{ (isset($new_trending->value)) ? $new_trending->value : '	NEW TRENDING'}}</span>
                                    <span class="title-backgruond icon-badge icons">
													<span class=" hidden">owesome</span>
												</span>
                                </h2>
                            </div>
                            <div>
                                <div class="magicproduct_trending magicproduct">
                                    <div class="block-title-tabs">
                                        <ul class="magictabs">
                                            <li class="item active loaded single" data-type="newproduct">
                                                {{--<span class="title">New Products</span>--}}
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="content-products" data-margin="20" data-slider='null'
                                         data-options='{"480":"1","640":"2","768":"3","992":"4","993":"5"}'>
                                        <div class="mage-magictabs mc-newproduct">
                                            <ul class="flexisel-content products-grid newproduct zoomOut play">
                                                @foreach($trips as $item)
                                                    @if($loop->iteration < 6)
                                                        <li class="item item-animate">
                                                            <div class="per-product">
                                                                <div class="images-container">
                                                                    <div class="product-hover">
                                                                        <a href="{{ route('booking', str_slug($item->name)) }}"
                                                                           target="_blank" title="{{ $item->name }}"
                                                                           class="product-image">
                                                                            <img class="img-responsive"
                                                                                 src="{{ (isset($item->image)) ? asset($item->image) : 'placehold/212x270.png' }}"
                                                                                 width="212"
                                                                                 height="270" alt="{{ $item->name }}"/>
                                                                            <span class="product-img-back">
																					<img class="img-responsive"
                                                                                         src="{{ (isset($item->image)) ? asset($item->image) : 'placehold/212x270.png?text=hover' }}"
                                                                                         width="212" height="270"
                                                                                         alt="{{ $item->name }}"/>
																				</span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                                <div class="products-textlink clearfix">
                                                                    <div class="price-box">
                                                                        @if(!empty($item->price_sale))
                                                                            <p class="old-price">
                                                                                <span class="price-label">Regular Price:</span>
                                                                                <span class="price"> {!! $item->currency  . "<span style='color:red;'>$item->price</span>" !!} </span>
                                                                            </p>
                                                                            <p class="special-price">
                                                                                <span class="price-label">Special Price</span>
                                                                                <span class="price"> {!!  $item->currency  . "<span style='color:red;'>$item->price_sale</span>"  !!} </span>
                                                                            </p>
                                                                        @else
                                                                            <p class="special-price">
                                                                                <span class="price-label">Special Price</span>
                                                                                <span class="price"> {!! $item->currency  . "<span style='color:red;'>$item->price</span>" !!} </span>
                                                                            </p>
                                                                        @endif
                                                                    </div>
                                                                    <div class="ratings">
                                                                        <div class="rating-box">
                                                                            <div class="rating"
                                                                                 style="width:100%"></div>
                                                                        </div>

                                                                    </div>
                                                                    <h3 class="product-name">
                                                                        <a href="{{ route('booking',str_slug($item->name)) }}"
                                                                           target="_blank"
                                                                           title="{{ $item->name }}"><span class="blink">{{ $item->name }}</span></a>
                                                                    </h3>


                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul><!-- .mc-newproduct -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .trending -->
                </div>
            </div><!--- .main-->
        </div><!-- .container-->
    </div><!-- .block_trending -->
    @if($services->count() === 0)
        <div class="block-static block_policy">
            <div class="policy">
                <div class="container">
                    <div class="support-policy">
                        <div class="support-footer-inner row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row-normal">
                                    <div class="col-icon">
                                        <div class="support-icon square-round"><i
                                                    class="icon-stroke icon-Truck icons"></i></div>
                                    </div>
                                    <div class="col-text">
                                        <div class="support-info">
                                            <div class="info-title">Free Delivery Worldwide</div>
                                            <div class="info-details">We are free shipping worldwide, you can buy and
                                                receive goods anywhere. Fast and safe
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row-normal">
                                    <div class="col-icon">
                                        <div class="support-icon square-round"><i class="icon-wallet icons"></i></div>
                                    </div>
                                    <div class="col-text">
                                        <div class="support-info">
                                            <div class="info-title">100% Money Back</div>
                                            <div class="info-details">If you are not satisfied with the quality of
                                                products, our services, you can return the goods &amp; receive a 100%
                                                money back
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row-normal">
                                    <div class="col-icon">
                                        <div class="support-icon square-round"><i
                                                    class="icon-Users icon-stroke icons"></i></div>
                                    </div>
                                    <div class="col-text">
                                        <div class="support-info">
                                            <div class="info-title">Member Discount</div>
                                            <div class="info-details">Please register with us to receive friendly member
                                                gifts and discounts on purchases weekly
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--- .policy-->
        </div><!-- .block_policy -->
    @else
        <div class="block-static block_policy">
            <div class="policy">
                <div class="container">
                    <div class="support-policy">
                        <div class="support-footer-inner row">
                            @foreach($services as $item)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="row-normal">
                                        <div class="col-icon">
                                            <div class="support-icon square-round"><i class="{{$item->icon }}"></i>
                                            </div>
                                        </div>
                                        <div class="col-text">
                                            <div class="support-info">
                                                <h3><div class="info-title">{{$item->title}}</div></h3>
                                                <div class="info-details">{!! $item->content ?? $item->description ?? $item->overview !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div><!--- .policy-->
        </div><!-- .block_policy -->
    @endif
    <div class="block-static alo-blog">
        <div class="container">
            <div class="main">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block-custom block-custom3">
                            <div class="block-title">
                                <h2>
                                    <span class="title-top">{{ (isset($new_blog->value)) ? $new_blog->value : 'News & Blog' }}</span>
                                    <span class="title-backgruond icon-note icons">
													<span class=" hidden">owesome</span>
												</span>
                                </h2>
                            </div>
                            <div class="magicproduct_news">
                                <div class="blogtabs block-title-tabs">
                                    <h3 class="item active loaded section-title title_left" data-type="blog">
                                        <span>Latest Posts Blog</span>
                                    </h3>
                                </div>
                                <div class="content-blog" data-margin="60"
                                     data-slider='{"moveSlides":2,"infiniteLoop":0,"maxSlides":"2","slideWidth":"555","slideMargin":60,"pager":1,"controls":0,"speed":800,"visibleItems":2,"responsiveBreakpoints":{"480":1,"640":1,"768":1,"992":2}}'
                                     data-options='{"480":"1","640":"1","768":"1","992":"2","993":"2"}'>
                                    <div class="mc-blog">
                                        <ul class="flexisel-content">
                                            @foreach($articles as $item)
                                                <li class="item">
                                                    <div class="postcontent">
                                                        <div class="blog-image image-container">
                                                            <a href="{{ route('blog-detail',$item->slug) }}">
                                                                <img class="img-responsive"
                                                                     src="{{ asset($item->img_thumbnail) }}"
                                                                     alt="{{ $item->title }}"/>
                                                            </a>
                                                        </div>
                                                        <div class="blog_short_text clearfix">
                                                            <div class="icon-blog">
                                                                <i class="icon-camera icons"></i>
                                                            </div>
                                                            <div class="blog_text">
                                                                <a href="{{ route('blog-detail',$item->slug) }}" title="{{ $item->title }}">
                                                                    <span class="title">{{$item->title}}</span>
                                                                </a>
                                                                <div class="post-date">
                                                                    <span>{{ date(' H:m d-m-Y', strtotime($item->created_at)) }}</span>-By<span
                                                                            class="title"> {{$item->author}}</span>

                                                                </div>
                                                                <div class="shortcontent-text">
                                                                    <p class="short-text">{!! $item->description !!}</p>
                                                                </div>
                                                                <a class="read-more-blog" title="{{ $item->title }}"
                                                                   href="{{ route('blog-detail',$item->slug)  }}">read
                                                                    more</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- .magicproduct_news -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .alo-blog -->


@endsection
@section('style')
    <style>

        .booking{
            padding-top:10px;
padding-bottom:10px;
            background: url(http://car-asia-booking.local/upload/media/image/1534323198hanoi-airport.jpg) no-repeat 0px 0px;
        }
        ul.flex-direction-nav {
            margin-bottom: 0px !important;
        }
        .main-agileinfo {
            margin: 50px auto;
            width: 53%;
            font-family: 'Open Sans', sans-serif !important;
        }

        .main-agileinfo  .sap_tabs {
            clear: both;
            padding: 0;
        }


        .main-agileinfo .resp-tabs-list {
            list-style: none;
            padding: 15px 0px;
            margin: 0 auto;
            text-align: center;
            background: #ee1b24;
        }
        .trip {
            padding: 12px 0 12px 0;
            width: 100%;
            font-size: 15px;
            margin-bottom: 8px;
        }
        .main-agileinfo span{
            font-weight: 600;
        }
        .main-agileinfo .resp-tab-item {
            font-size: 1.1em;
            font-weight: 500;
            cursor: pointer;
            display: inline-block;
            margin: 0;
            text-align: center;
            list-style: none;
            outline: none;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -ms-transition: all 0.3s;
            -o-transition: all 0.3s;
            transition: all 0.3s;
            text-transform: uppercase;
            margin: 0 1.2em 0;
            color: #b1b1b1;
            padding: 7px 15px;
        }

        .main-agileinfo .resp-tab-active {
            color: #fff;
        }

        .main-agileinfo .resp-tabs-container {
            padding: 0px;
            clear: left;
        }

        .main-agileinfo  h2.resp-accordion {
            cursor: pointer;
            padding: 5px;
            display: none;
        }

        .main-agileinfo .resp-tab-content {
            display: none;
        }

        .main-agileinfo .resp-content-active, .main-agileinfo .resp-accordion-active {
            display: block;
        }

        .main-agileinfo form {
            background: rgba(3, 3, 3, 0.57);
            padding: 25px;
        }

        .main-agileinfo h3 {
            font-size: 18px;
            color: #ee1b24;
            margin-bottom: 7px;
            font-family: 'Open Sans', sans-serif !important;
            font-weight: bold;
        }
        .main-agileinfo form {
            display: inherit !important;
        }
        .from, .to, .date, .depart, .return {
            width: 48%;
            float: left;
        }

        .from, .to, .date {
            margin-bottom: 18px !important;
        }
        .main-agileinfo  .date{
            width: 45%;
            margin-right: 5%;
        }
        .from, .date, .depart {
            margin-right: 4%;
        }
        .clear {
            clear: both;
        }
        .main-agileinfo input[type="text"] {
            padding: 10px;
            width: 93%;
            float: left;
        }

        input#datepicker, input#datepicker1, input#datepicker2 {
            width: 93%;
            margin-bottom: 0px !important;
        }


        .main-agileinfo  input[type="submit"] {
            font-size: 14px;
            color: #fff;
            background: #ee1b24;
            border: none;
            outline: none;
            padding: 12px 40px;
            margin-top: 5px;
            cursor: pointer;
            transition: 0.5s all ease;
            -webkit-transition: 0.5s all ease;
            -moz-transition: 0.5s all ease;
            -o-transition: 0.5s all ease;
            -ms-transition: 0.5s all ease;
        }
        .main-agileinfo label.checkbox {
            display: inline-block;
        }

        .main-agileinfo .checkbox {
            position: relative;
            font-size: 0.95em;
            font-weight: normal;
            color: #dedede;
            padding: 0em 0.5em 0em 2em;
        }

        .main-agileinfo .checkbox i {
            position: absolute;
            bottom: 3px;
            left: 2px;
            display: block;
            width: 18px;
            height: 18px;
            outline: none;
            background: #fff;
            border: 1px solid #6A67CE;
        }

        .main-agileinfo .checkbox i {
            font-size: 20px;
            font-weight: 400;
            color: #999;
            font-style: normal;
        }

        .main-agileinfo .checkbox input:checked + i:after {
            opacity: 1;
        }

        .main-agileinfo .checkbox input + i:after {
            position: absolute;
            opacity: 0;
            transition: opacity 0.1s;
            -o-transition: opacity 0.1s;
            -ms-transition: opacity 0.1s;
            -moz-transition: opacity 0.1s;
            -webkit-transition: opacity 0.1s;
        }

        .main-agileinfo .checkbox input + i:after {
            content: '';
            /*background: url("../images/tick.png") no-repeat 5px 5px;*/
            top: -1px;
            left: -1px;
            width: 15px;
            height: 15px;
            font: normal 12px/16px FontAwesome;
            text-align: center;
        }

        .main-agileinfo input[type="checkbox"] {
            opacity: 0;
            margin: 0;
            display: none;
        }

        .from, .to, .date {
            margin-bottom: 40px;
        }

        .quantity-select .entry.value-minus {
            margin-left: 0;
        }

        .value-minus, .value-plus {
            height: 40px;
            line-height: 24px;
            width: 40px;
            margin-right: 3px;
            display: inline-block;
            cursor: pointer;
            position: relative;
            font-size: 18px;
            color: #fff;
            text-align: center;
            -webkit-user-select: none;
            -moz-user-select: none;
            border: 1px solid #b2b2b2;
            vertical-align: bottom;
        }

        .value {
            cursor: default;
            width: 40px;
            height: 40px;
            color: #000;
            line-height: 24px;
            border: 1px solid #E5E5E5;
            background-color: #fff;
            text-align: center;
            display: inline-block;
            margin-right: 3px;
            padding-top: 7px;
        }

        .quantity-select .entry.value-minus:hover, .quantity-select .entry.value-plus:hover {
            background: #E5E5E5;
        }

        .value-minus, .value-plus {
            height: 40px;
            line-height: 24px;
            width: 40px;
            margin-right: 3px;
            display: inline-block;
            cursor: pointer;
            position: relative;
            font-size: 18px;
            color: #fff;
            text-align: center;
            -webkit-user-select: none;
            -moz-user-select: none;
            border: 1px solid #b2b2b2;
            vertical-align: bottom;
        }

        .quantity-select .entry.value-minus:before, .quantity-select .entry.value-plus:before {
            content: "";
            width: 13px;
            height: 2px;
            background: #000;
            left: 50%;
            margin-left: -7px;
            top: 50%;
            margin-top: -0.5px;
            position: absolute;
        }

        .quantity-select .entry.value-plus:after {
            content: "";
            height: 13px;
            width: 2px;
            background: #000;
            left: 50%;
            margin-left: -1.4px;
            top: 50%;
            margin-top: -6.2px;
            position: absolute;
        }

        .numofppl, .adults, .child {
            width: 50%;
            float: left;
        }


        .main-agileinfo input[type="submit"]:hover {
            background: #ee1b24;
            color: #fff;
        }

        /*-- load-more --*/
        #myList li {
            display: none;
            list-style-type: none;
        }




        /*-- //load-more --*/

        /*-- responsive --*/
        @media (max-width: 1440px) {
            .checkbox {
                font-size: 0.9em;
            }
        }

        @media (max-width: 1366px) {
            .main-agileinfo {
                width: 53%;
            }
        }

        @media (max-width: 1280px) {
            .main-agileinfo {
                width: 57%;
            }
        }

        @media (max-width: 1080px) {
            .main-agileinfo h1 {
                color: #fff;
                font-size: 47px;
            }

            .main-agileinfo {
                width: 68%;
            }
        }

        @media (max-width: 1024px) {
            .main-agileinfo {
                width: 71%;
            }
        }

        @media (max-width: 991px) {
            .from, .to, .date, .depart, .return {
                width: 49%;
                float: left;
            }

            .from, .date, .depart {
                margin-right: 2%;
            }
        }

        @media (max-width: 966px) {
            .main-agileinfo {
                width: 73%;
            }

        }

        @media (max-width: 900px) {
            .main-agileinfo .checkbox {
                font-size: 0.82em;
            }
        }

        @media (max-width: 800px) {
            .main-agileinfo {
                width: 81%;
            }
        }

        @media (max-width: 768px) {
            .main-agileinfo {
                width: 85%;
            }

            .checkbox i {
                width: 15px;
                height: 15px;
            }

            .checkbox input + i:after {
                top: -3px;
                left: -3px;
            }
        }

        @media (max-width: 736px) {
            .main-agileinfo {
                width: 88%;
                margin: 40px auto;
            }

            .main-agileinfo h1 {
                color: #fff;
                font-size: 43px;
                margin-top: 40px;
            }

            .main-agileinfo input[type="text"] {
                padding: 10px;
                width: 90%;
                float: left;
            }

            input#datepicker, input#datepicker1, input#datepicker2, input#datepicker3 {
                width: 79%;
            }

            .value-minus, .value-plus {
                height: 30px;
                width: 30px;
            }

            .value {
                width: 30px;
                height: 30px;
                padding-top: 6px;
            }
        }

        @media (max-width: 667px) {
            .numofppl {
                width: 60%;
            }

            .roundtrip .date {
                width: 68%;
                float: left;
            }

            .roundtrip .class {
                width: 30%;
                float: left;
            }

            .oneway .depart, .multicity .depart {
                width: 68%;
            }
            .main-agileinfo  input[type="submit"] {
                 margin-top: 20px;
            }
        }

        @media (max-width: 600px) {
            select#w3_country1 {
                width: 100%;
            }
        }

        @media (max-width: 568px) {
            .main-agileinfo h1 {
                font-size: 40px;
            }

            .resp-tab-item {
                padding: 7px 13px;
                margin: 0 1em 0;
            }

            .numofppl {
                width: 70%;
            }
            .main-agileinfo  input[type="submit"] {
                margin-top: 20px;
            }
        }

        @media (max-width: 480px) {
            .resp-tab-item {
                padding: 7px 7px;
                margin: 0 0.7em 0;
            }

            .main-agileinfo input[type="text"] {
                padding: 10px;
                width: 86%;
                float: left;
            }

            .roundtrip .date {
                width: 100%;
                float: left;
            }

            input#datepicker, input#datepicker1, input#datepicker2, input#datepicker3 {
                width: 86%;
            }

            .roundtrip .class {
                width: 100%;
                float: left;
                margin-bottom: 40px;
            }

            .numofppl {
                width: 80%;
            }

            .oneway .depart, .multicity .depart {
                width: 100%;
            }
            .main-agileinfo  input[type="submit"] {
                margin-top: 20px;
            }
        }

        @media (max-width: 414px) {
            .main-agileinfo h1 {
                font-size: 35px;
                margin-top: 30px;
            }

            .resp-tab-item {
                padding: 7px 7px;
                margin: 0 0.5em 0;
                font-size: 15px;
            }

            .numofppl {
                width: 100%;
            }
            .main-agileinfo  input[type="submit"] {
                margin-top: 20px;
            }
        }

        @media (max-width: 384px) {
            .main-agileinfo h1 {
                font-size: 32px;
            }

            .main-agileinfo h3 {
                font-size: 15px;
            }

            .from, .to, .date, .depart, .return {
                width: 100%;
                float: left;
                margin-bottom: 25px;
            }

            .date {
                margin-bottom: 0;
            }

            .resp-tab-item {
                padding: 7px 7px;
                margin: 0 0em 0;
                font-size: 15px;
            }

            .main-agileinfo .class {
                width: 100%;
                float: left;
                margin-bottom: 40px;
            }

            .main-agileinfo input[type="text"] {
                padding: 10px;
                width: 91.5%;
            }

            input#datepicker, input#datepicker1, input#datepicker2 {
                width: 100%;
            }
            .main-agileinfo  input[type="submit"] {
                margin-top: 20px;
            }
        }

        @media (max-width: 320px) {
            .main-agileinfo h1 {
                font-size: 26px;
                margin-top: 25px;
            }

            .main-agileinfo form {
                padding: 15px;
            }

            .resp-tab-item {
                padding: 7px 5px;
                margin: 0 0em 0;
                font-size: 14px;
            }

            .adults, .child {
                width: 100%;
                float: left;
            }

            .adults {
                margin-bottom: 25px;
            }
            .main-agileinfo  input[type="submit"] {
                margin-top: 20px;
            }
        }
    </style>
    <style>
        .template-default .products-grid .product-name {
            white-space: normal !important;
        }
        .template-default .item .actions-no .button.btn-cart {
            background-color: #e42021;
        }
        .template-default .block-custom .block-title span {
            color: #e42021;
        }
        .template-default .block-custom .block-title .title-backgruond {
            color: #e42021;
        }
        .template-default .block-custom.block-custom1 .bx-wrapper .bx-controls-direction a {
            top: -38px !important;
        }

        .template-default .labelhot {
            color: #fff;
            background-color: #e42021;
        }

        .price-box .price {
            font-size: 30px;
            font-weight: 600;
        }
        .blink{
            color:red;
            /*padding: 15px;*/
            text-align: center;
        }
        span.blink{
            font-size: 18px;
            animation: blink 1.0s linear infinite;
        }
        @keyframes blink{
            0%{opacity: 0;}
            50%{opacity: .75;}
            100%{opacity: 1;}
        }
        .blog_text .read-more-blog:hover {
            border: 1px solid #e42021;
            background-color: #e42021;
            color: #fff;
        }
        .template-default body a:hover {
            color: #e42021;
        }
    </style>
@endsection
@section('js')
    <script src="{{ asset('client/booking/js/jquery.min.js') }}"></script>
    <script src="{{ asset('client/booking/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        @if (Session::has('error'))
            toastr['error']('', '{!! Session::get("error") !!}');
        @elseif(Session::has('success'))
            toastr['success']('', '{!! Session::get("success") !!}');
        @elseif(Session::has('warning'))
            toastr['warning']('', '{!! Session::get("danger") !!}');
        @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <!--//script for portfolio-->
    <!-- Calendar -->
    <link rel="stylesheet" href="{{ asset('client/booking/css/jquery-ui.css') }}"/>
    <script src="{{ asset('client/booking/js/jquery-ui.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#datepicker").datepicker({
                minDate: 'dateToday',
                onSelect: function (selected) {
                    $("#datepicker1").datepicker("option", "minDate", selected)
                }
            });
            $("#datepicker1").datepicker({
                onSelect: function (selected) {
                    $("#datepicker").datepicker("option", "maxDate", selected)
                }
            });
            $("#datepicker2").datepicker({
                minDate: 'dateToday'
            });
        });
        $(function () {
            $("#datepicker,#datepicker1,#datepicker2").datepicker();
        });
    </script>
    <!-- //Calendar -->
    <!--quantity-->
    <script>
        $('.value-plus').on('click', function () {
            let divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
            divUpd.text(newVal);
            $(this).parent().find('#quantity_child').val(newVal);
            $(this).parent().find('#quantity_adult').val(newVal);
        });

        $('.value-minus').on('click', function () {
            let divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
            if (newVal >= 0) {
                divUpd.text(newVal);
                $(this).parent().find('#quantity_child').val(newVal);
                $(this).parent().find('#quantity_adult').val(newVal);
            }
        });
    </script>
    <!--//quantity-->
    <!--load more-->
    <script>
        $(document).ready(function () {
            size_li = $("#myList li").size();
            x = 1;
            $('#myList li:lt(' + x + ')').show();
            $('#loadMore').click(function () {
                x = (x + 1 <= size_li) ? x + 1 : size_li;
                $('#myList li:lt(' + x + ')').show();
            });
            $('#showLess').click(function () {
                x = (x - 1 < 0) ? 1 : x - 1;
                $('#myList li').not(':lt(' + x + ')').hide();
            });
        });
    </script>

@endsection