@extends('layouts.app')
@section('content')

    @php
        $about_banner_name = \App\Models\System\Setting::select('value')->where('key','about_banner_name')->first();
        $about_banner_title = \App\Models\System\Setting::select('value')->where('key','about_banner_title')->first();
        $about_banner_subtitle = \App\Models\System\Setting::select('value')->where('key','about_banner_subtitle')->first();
        $about_banner = \App\Models\System\Setting::select('value')->where('key','about_banner')->first();
        $about_testimoninal_image = \App\Models\System\Setting::select('value')->where('key','about_testimoninal_image')->first();
        $about_content = \App\Models\System\Setting::select('value')->where('key','about_content')->first();
        $about_title_stand = \App\Models\System\Setting::select('value')->where('key','about_title_stand')->first();
        $about_subtitle_stand = \App\Models\System\Setting::select('value')->where('key','about_subtitle_stand')->first();
    @endphp
    {{--<div class="about-us-top">--}}
    {{--<!---<div class="title">--}}
                {{--<div class="top">{{ isset($about_banner_name->value) ? $about_banner_name->value : '' }}</div>--}}
                {{--<h1>{{ isset($about_banner_title->value) ? $about_banner_title->value : '' }}</h1>--}}
                {{--<div class="bottom">{{ isset($about_banner_subtitle->value) ? $about_banner_subtitle->value : '' }}</div>--}}
            {{--</div>-->--}}
    {{--</div><!--- .about-us-top-->--}}
    <div class="banner">
        <img src="{{ (isset($page->img_banner)) ? asset($page->img_banner) : 'client/assets/images/bg-about-us.jpg' }}"
             class="img-responsive" width="100%" height="430" alt="{{ isset( $carasia_rental_name->value) ?  $carasia_rental_name->value : 'About Car-Asia' }}">
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="{{ route('index') }}" title="Go to Home Page">Home</a></li>
                <li><strong>About Us</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content-text">
                    <h1>{{ (isset( $page->title)) ? $page->title : '' }}</h1>
                    <p>{!! (isset($page->content)) ? $page->content : '' !!} </p>
                    <a href="{{ route('contact') }}" class="read-more-btn">Contact Us</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about-bx-slider-wrap">
                    <ul class="about-bx-slider">
                        <li>
                            <img src="{{ (isset($page->img_thumbnail)) ? asset($page->img_thumbnail) : 'http://placehold.it/550x400' }}"
                                 alt="{{ $page->title }}"><span class="title">{{ (isset( $page->title)) ? $page->title : '' }}</span></li>
                        <li>
                            <img src="{{ (isset($page->img_thumbnail)) ? asset($page->img_thumbnail) : 'http://placehold.it/550x400' }}"
                                 alt="{{ $page->title }}"><span class="title">{{ (isset( $page->title)) ? $page->title : '' }}</span></li>

                    </ul>
                </div>
            </div>
        </div>
    </div><!--- .container-->
    <div class="container">

        <div class="row content">
            <h2>{!! isset($about_title_stand->value) ? $about_title_stand->value : 'What we stand for' !!}</h2>

            <h4>{!! isset($about_subtitle_stand->value) ? $about_subtitle_stand->value : 'We stand for providing the lowest prices for the most comfortable and safe bus ride possible, and always
                making sure you as a passenger are priority'  !!}</h4>
            @foreach($abouts as $item)
                @if($loop->iteration % 2 ==1 )
                    <div class="col-md-4">
                        <p><img alt="{{ $item->title }}" src="{{ asset($item->img_thumbnail) }}"
                                class="img-responsive"/></p>
                    </div>
                    <div class="col-md-8">
                        <h3>{{ $item->title }}</h3>

                        <p>{!! $item->overview ?? $item->description !!}</p>
                    </div>
                    <div class="clearfix" style="margin-bottom: 30px;margin-top: 30px;"></div>
                @else
                    <div class="col-md-8">
                        <h3>{{ $item->title }}</h3>

                        <p>{!! $item->overview ?? $item->description !!}</p>
                    </div>
                    <div class="col-md-4">
                        <p><a href="{{ route('booking') }}"><img alt="{{ $item->title }}" src="{{ asset($item->img_thumbnail) }}"
                                           class="img-responsive"/></a></p>
                    </div>
                    <div class="clearfix" style="margin-bottom: 30px;"></div>
                @endif
            @endforeach
        </div>
    </div>
    <div class="block-static block_testimonials">
        <div class="testimonials-about" style="">
            <div class="container">
                <ul class="bx-testimonial">
                    @foreach($cmsTestimonial as $item)
                        <li>
                            <div class="testimonial_text">
                                <p class="fa-over"><i class="fa fa-quote-right"></i></p>
                                <p class="title-name">{!! $item->title !!}</p>
                                <span class="sub-text">{!!  $item->evaluate !!}</span>
                                <p class="name">{!! $item->name !!}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <style>
        .content-text h1 {
            font-size: 39px;
            font-family: 'Montserrat';
            margin-bottom: 33px;
        }
        .content h2 {
            font-size: 40px;
            color: red;
            text-align: center;
            font-weight: bold;
        }

        .content h3:first-child {
            font-size: 30px;
            text-align: center;
        }

        .content h4 {
            max-width: 675px;
            margin: auto;
            margin-bottom: 15px;
            font-size: 19px;
            text-align: center;
        }

        .content h3,
        .content h5 {
            color: red;
            font-size: 15px;
        }

        .content p img:nth-child(even) {
            float: left;
            clear: both;
        }

        .about-us-top {
            background: url({{ (isset($page->img_banner)) ? asset($page->img_banner) : 'client/assets/images/bg-about-us.jpg' }}) top center no-repeat;
            padding: 325px 15px 105px;
        }

        .testimonials-about {
            background: url({{ (isset($about_testimoninal_image->value)) ? $about_testimoninal_image->value  : 'client/assets/images/bg-tes-about-us.png' }}) top center no-repeat;
            background-size: cover;
            padding: 90px 0;
            text-align: center;
        }

        .testimonials-about .name {
            color: white !important;
            font-size: 14px;
            font-weight: 300;
        }

        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 338px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 360px) {
            .banner img{
                height: 80px;
            }
        }
        @media only screen and (max-width: 480px) {
            .banner img{
                height: 100px;
            }
        }
    </style>
@endsection