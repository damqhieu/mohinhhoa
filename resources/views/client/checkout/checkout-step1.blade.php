@extends('layouts.app')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="#" title="Go to Home Page">Home</a></li>
                <li><strong>Checkout</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="woocommerce">
        <div class="container">
            <div class="content-top">
                <h2>Checkout</h2>
                <p>Need to Help? Call us: {{ $general->hotline }} or Email: <a
                            href="mailto:{{ $general->email }}">{{ $general->email }}</a></p>
            </div>
            <div class="checkout-step-process">
                <ul>
                    <li>
                        <div class="step-process-item active"><i class="redirectjs step-icon icon-wallet"></i><span
                                    class="text">Delivery & Payment</span></div>
                    </li>
                    <li>
                        <div class="step-process-item"><i class="redirectjs step-icon icon-notebook"></i><span
                                    class="text">Order Review</span></div>
                    </li>
                </ul>
            </div>

            <div class="checkout-info-text">
                <h3>Delivery</h3>
            </div>
            <div class="row">
                <div class="content-radio">
                    <label for="delivery-radio-1" class="label-radio">Information</label>
                    <form method="post" class="form-in-checkout">
                        @csrf
                        <ul class="row">
                            <li class="col-md-9">
                                <ul class="row">
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerName" class="">Buyer Name <abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerName" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerName'] : '' }}"
                                                   id="buyerName">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerEmail" class="">Buyer Email <abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerEmail" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerEmail'] : '' }}"
                                                   id="buyerEmail">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerPhone" class="">Buyer Phone <abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerPhone"  value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerPhone'] : '' }}"
                                                   id="buyerPhone">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerCity" class="">Buyer City <abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerCity" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerCity'] : '' }}"
                                                   id="buyerCity">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerCountry" class="">Buyer Country <abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerCountry" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerCountry'] : '' }}"
                                                   id="buyerCountry">
                                        </p>
                                    </li>
                                    <li class="col-md-6 col-left-6">
                                        <p class="form-row validate-required">
                                            <label for="buyerAddress" class="">Buyer Address<abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="buyerAddress" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['buyerAddress'] : '' }}"
                                                   id="buyerAddress">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="flightNumber" class="">Flight number<abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="flightNumber" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['flightNumber'] : '' }}"
                                                   id="flightNumber">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="timeDeparture" class="">Time departure<abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="timeDeparture" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['timeDeparture'] : '' }}"
                                                   id="timeDeparture">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="timeArrival" class="">Time arrival<abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <input type="text" class="input-text form-control"
                                                   name="timeArrival" value="{{ isset($dataBooking) ? $dataBooking['infoPerson']['timeArrival'] : '' }}"
                                                   id="timeArrival">
                                        </p>
                                    </li>
                                    <li class="col-md-6">
                                        <p class="form-row validate-required">
                                            <label for="note" class="">Note<abbr
                                                        class="required"
                                                        title="required"> *</abbr></label>
                                            <textarea class="form-control" name="note" id="note">{{ isset($dataBooking) ? $dataBooking['infoPerson']['note'] : 'I have been sure' }}</textarea>
                                        </p>
                                    </li>
                                </ul>
                            </li>
                            <li class="col-md-3">
                                <div class="checkout-info-text">
                                    <h3>Payment Method</h3>
                                </div>
                                <div class="content-radio">
                                    <input type="radio" name="paymentCard" checked>
                                    <label for="pr2" class="label-radio">Visa/Master Card</label>
                                </div>
                                <div class="checkout-col-footer">
                                    <a href="{{ route('booking') }}" class="btn-step">Back</a>
                                    <input type="submit" value="Continue" class="btn-step btn-highligh">
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
            <div class="line-bottom"></div>
        </div>
    </div><!--- .woocommerce-->
@endsection
@section('style')
    <style>
        .checkout-step-process .step-process-item.active .step-icon {
            background-color: #e42021;
            color: #fff;
            border-color: #e42021;
        }
        .checkout-step-process .active .text {
            color: #e42021;
        }
        .btn-step.btn-highligh {
            background-color: #e42021;
            border-color: #e42021;
            color: #fff;
        }
        .btn-step.btn-highligh:hover {
            background-color: #e42021;
            border-color: #e42021;
        }
        .template-default body a:hover {
            color: #fff;
        }
    </style>
@endsection