@extends('layouts.app')
@section('content')
    <div class="breadcrumbs">
        <div class="container">
            <ul>
                <li class="home"><a href="#" title="Go to Home Page">Home</a></li>
                <li><strong>Checkout</strong></li>
            </ul>
        </div>
    </div><!--- .breadcrumbs-->

    <div class="woocommerce">
        <div class="container">
            <div class="content-top">
                <h2>Checkout</h2>
                <p>Need to Help? Call us: +9 123 456 789 or Email: <a
                            href="mailto:Support@Rosi.com">Support@Rosi.com</a></p>
            </div>
            <div class="checkout-step-process">
                <ul>
                    <li>
                        <div class="step-process-item"><i class="redirectjs fa fa-check step-icon"></i><span
                                    class="text">Delivery & Payment</span></div>
                    </li>
                    <li>
                        <div class="step-process-item active"><i class="redirectjs step-icon icon-notebook"></i><span
                                    class="text">Order Review</span></div>
                    </li>
                </ul>
            </div><!--- .checkout-step-process-->

            <ul class="row">
                <li class="col-md-12 col-padding-right">
                    <table class="table-order table-order-review">
                        <thead>
                        <tr>
                            <td width="68">Trip Name</td>
                            <td width="14">price</td>
                            <td width="14">Qty Adult</td>
                            <td width="14">Qty Child</td>
                            <td width="14">Total QTY</td>
                            <td width="14">Currency</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="name">{{ $dataBooking['trip']->name }}</td>
                            <td>{{ $dataBooking['amount'] }}</td>
                            <td>{{ $dataBooking['quantity_adult'] }}</td>
                            <td>{{ $dataBooking['quantity_child'] }}</td>
                            <td>{{ $dataBooking['totalItem'] }}</td>
                            <td>{{ $dataBooking['trip']->currency }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table-order table-order-review-bottom">
                        <tr>
                            <td class="first large">Total Payment</td>
                            <td class="price large">{{ ($dataBooking['trip']->currency === 'USD') ? '$' : 'VNĐ'  }}{{ $dataBooking['amount'] }}</td>
                        </tr>
                        <tfoot>
                        <td colspan="2">
                            <div class="left">Empty booking? <a href="{{ route('payment.empty-booking') }}">Empty Your Booking</a></div>
                            <div class="right">
                                <a href="{{ route('payment.checkout-step1') }}" class="btn-step">Back</a>
                                <a href="{{ route('payment.handle-payment') }}" class="btn-step btn-highligh">Payment</a>
                            </div>
                        </td>
                        </tfoot>
                    </table>
                </li>
            </ul>

            <div class="line-bottom"></div>
        </div><!--- .container-->
    </div><!--- .woocommerce-->
 @endsection
@section('style')
    <style>
        .checkout-step-process .step-process-item.active .step-icon {
            background-color: #e42021;
            color: #fff;
            border-color: #e42021;
        }
        .table-order tbody td.price {
            color: #e42021;
        }
        .checkout-step-process .active .text {
            color: #e42021;
        }
        .btn-step.btn-highligh {
            background-color: #e42021;
            border-color: #e42021;
            color: #fff;
        }
        .btn-step.btn-highligh:hover {
            background-color: #e42021;
            border-color: #e42021;
        }
        .template-default body a:hover {
            color: #fff;
        }
    </style>
@endsection