<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!--[if IE]>
<html xmlns="http://www.w3.org/1999/xhtml" class="ie"><![endif]--><!--[if !IE]><!-->
<html style="margin: 0;padding: 0;" xmlns="http://www.w3.org/1999/xhtml"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <meta name="viewport" content="width=device-width">

    <style type="text/css">
        @media only screen and (min-width: 620px) {
            .wrapper {
                min-width: 600px !important
            }

            .wrapper h1 {
            }

            .wrapper h1 {
                font-size: 64px !important;
                line-height: 63px !important
            }

            .wrapper h2 {
            }

            .wrapper h2 {
                font-size: 30px !important;
                line-height: 38px !important
            }

            .wrapper h3 {
            }

            .wrapper h3 {
                font-size: 22px !important;
                line-height: 31px !important
            }

            .column {
            }

            .wrapper .size-8 {
                font-size: 8px !important;
                line-height: 14px !important
            }

            .wrapper .size-9 {
                font-size: 9px !important;
                line-height: 16px !important
            }

            .wrapper .size-10 {
                font-size: 10px !important;
                line-height: 18px !important
            }

            .wrapper .size-11 {
                font-size: 11px !important;
                line-height: 19px !important
            }

            .wrapper .size-12 {
                font-size: 12px !important;
                line-height: 19px !important
            }

            .wrapper .size-13 {
                font-size: 13px !important;
                line-height: 21px !important
            }

            .wrapper .size-14 {
                font-size: 14px !important;
                line-height: 21px !important
            }

            .wrapper .size-15 {
                font-size: 15px !important;
                line-height: 23px !important
            }

            .wrapper .size-16 {
                font-size: 16px !important;
                line-height: 24px !important
            }

            .wrapper .size-17 {
                font-size: 17px !important;
                line-height: 26px !important
            }

            .wrapper .size-18 {
                font-size: 18px !important;
                line-height: 26px !important
            }

            .wrapper .size-20 {
                font-size: 20px !important;
                line-height: 28px !important
            }

            .wrapper .size-22 {
                font-size: 22px !important;
                line-height: 31px !important
            }

            .wrapper .size-24 {
                font-size: 24px !important;
                line-height: 32px !important
            }

            .wrapper .size-26 {
                font-size: 26px !important;
                line-height: 34px !important
            }

            .wrapper .size-28 {
                font-size: 28px !important;
                line-height: 36px !important
            }

            .wrapper .size-30 {
                font-size: 30px !important;
                line-height: 38px !important
            }

            .wrapper .size-32 {
                font-size: 32px !important;
                line-height: 40px !important
            }

            .wrapper .size-34 {
                font-size: 34px !important;
                line-height: 43px !important
            }

            .wrapper .size-36 {
                font-size: 36px !important;
                line-height: 43px !important
            }

            .wrapper
            .size-40 {
                font-size: 40px !important;
                line-height: 47px !important
            }

            .wrapper .size-44 {
                font-size: 44px !important;
                line-height: 50px !important
            }

            .wrapper .size-48 {
                font-size: 48px !important;
                line-height: 54px !important
            }

            .wrapper .size-56 {
                font-size: 56px !important;
                line-height: 60px !important
            }

            .wrapper .size-64 {
                font-size: 64px !important;
                line-height: 63px !important
            }
        }
    </style>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        [x-apple-data-detectors],
        [href^="tel"],
        [href^="sms"] {
            color: inherit !important;
            text-decoration: none !important;
        }

        .wrapper .footer__share-button a:hover,
        .wrapper .footer__share-button a:focus {
            color: #ffffff !important;
        }

        .btn a:hover,
        .btn a:focus,
        .footer__share-button a:hover,
        .footer__share-button a:focus,
        .email-footer__links a:hover,
        .email-footer__links a:focus {
            opacity: 0.8;
        }

        .preheader,
        .header,
        .layout,
        .column {
            transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;
        }

        .preheader td {
            padding-bottom: 8px;
        }

        .layout,
        div.header {
            max-width: 400px !important;
            -fallback-width: 95% !important;
            width: calc(100% - 20px) !important;
        }

        div.preheader {
            max-width: 360px !important;
            -fallback-width: 90% !important;
            width: calc(100% - 60px) !important;
        }

        .snippet,
        .webversion {
            Float: none !important;
        }

        .column {
            max-width: 400px !important;
            width: 100% !important;
        }

        .fixed-width.has-border {
            max-width: 402px !important;
        }

        .fixed-width.has-border .layout__inner {
            box-sizing: border-box;
        }

        .snippet,
        .webversion {
            width: 50% !important;
        }

        .ie .btn {
            width: 100%;
        }

        [owa] .column div,
        [owa] .column button {
            display: block !important;
        }

        .ie .column,
        [owa] .column,
        .ie .gutter,
        [owa] .gutter {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie div.preheader,
        [owa] div.preheader,
        .ie .email-footer,
        [owa] .email-footer {
            max-width: 560px !important;
            width: 560px !important;
        }

        .ie .snippet,
        [owa] .snippet,
        .ie .webversion,
        [owa] .webversion {
            width: 280px !important;
        }

        .ie div.header,
        [owa] div.header,
        .ie .layout,
        [owa] .layout,
        .ie .one-col .column,
        [owa] .one-col .column {
            max-width: 600px !important;
            width: 600px !important;
        }

        .ie .fixed-width.has-border,
        [owa] .fixed-width.has-border,
        .ie .has-gutter.has-border,
        [owa] .has-gutter.has-border {
            max-width: 602px !important;
            width: 602px !important;
        }

        .ie .two-col .column,
        [owa] .two-col .column {
            max-width: 300px !important;
            width: 300px !important;
        }

        .ie .three-col .column,
        [owa] .three-col .column,
        .ie .narrow,
        [owa] .narrow {
            max-width: 200px !important;
            width: 200px !important;
        }

        .ie .wide,
        [owa] .wide {
            width: 400px !important;
        }

        .ie .two-col.has-gutter .column,
        [owa] .two-col.x_has-gutter .column {
            max-width: 290px !important;
            width: 290px !important;
        }

        .ie .three-col.has-gutter .column,
        [owa] .three-col.x_has-gutter .column,
        .ie .has-gutter .narrow,
        [owa] .has-gutter .narrow {
            max-width: 188px !important;
            width: 188px !important;
        }

        .ie .has-gutter .wide,
        [owa] .has-gutter .wide {
            max-width: 394px !important;
            width: 394px !important;
        }

        .ie .two-col.has-gutter.has-border .column,
        [owa] .two-col.x_has-gutter.x_has-border .column {
            max-width: 292px !important;
            width: 292px !important;
        }

        .ie .three-col.has-gutter.has-border .column,
        [owa] .three-col.x_has-gutter.x_has-border .column,
        .ie .has-gutter.has-border .narrow,
        [owa] .has-gutter.x_has-border .narrow {
            max-width: 190px !important;
            width: 190px !important;
        }

        .ie .has-gutter.has-border .wide,
        [owa] .has-gutter.x_has-border .wide {
            max-width: 396px !important;
            width: 396px !important;
        }

        .ie .fixed-width .layout__inner {
            border-left: 0 none white !important;
            border-right: 0 none white !important;
        }

        .ie .layout__edges {
            display: none;
        }

        .mso .layout__edges {
            font-size: 0;
        }

        .layout-fixed-width,
        .mso .layout-full-width {
            background-color: #ffffff;
        }

        @media only screen and (min-width: 620px) {
            .column,
            .gutter {
                display: table-cell;
                Float: none !important;
                vertical-align: top;
            }

            div.preheader,
            .email-footer {
                max-width: 560px !important;
                width: 560px !important;
            }

            .snippet,
            .webversion {
                width: 280px !important;
            }

            div.header,
            .layout,
            .one-col .column {
                max-width: 600px !important;
                width: 600px !important;
            }

            .fixed-width.has-border,
            .fixed-width.ecxhas-border,
            .has-gutter.has-border,
            .has-gutter.ecxhas-border {
                max-width: 602px !important;
                width: 602px !important;
            }

            .two-col .column {
                max-width: 300px !important;
                width: 300px !important;
            }

            .three-col .column,
            .column.narrow {
                max-width: 200px !important;
                width: 200px !important;
            }

            .column.wide {
                width: 400px !important;
            }

            .two-col.has-gutter .column,
            .two-col.ecxhas-gutter .column {
                max-width: 290px !important;
                width: 290px !important;
            }

            .three-col.has-gutter .column,
            .three-col.ecxhas-gutter .column,
            .has-gutter .narrow {
                max-width: 188px !important;
                width: 188px !important;
            }

            .has-gutter .wide {
                max-width: 394px !important;
                width: 394px !important;
            }

            .two-col.has-gutter.has-border .column,
            .two-col.ecxhas-gutter.ecxhas-border .column {
                max-width: 292px !important;
                width: 292px !important;
            }

            .three-col.has-gutter.has-border .column,
            .three-col.ecxhas-gutter.ecxhas-border .column,
            .has-gutter.has-border .narrow,
            .has-gutter.ecxhas-border .narrow {
                max-width: 190px !important;
                width: 190px !important;
            }

            .has-gutter.has-border .wide,
            .has-gutter.ecxhas-border .wide {
                max-width: 396px !important;
                width: 396px !important;
            }
        }

        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
            .fblike {
                background-image: url(https://i10.createsend1.com/static/eb/master/13-the-blueprint-3/images/fblike@2x.png) !important;
            }

            .tweet {
                background-image: url(https://i7.createsend1.com/static/eb/master/13-the-blueprint-3/images/tweet@2x.png) !important;
            }

            .linkedinshare {
                background-image: url(https://i8.createsend1.com/static/eb/master/13-the-blueprint-3/images/lishare@2x.png) !important;
            }

            .forwardtoafriend {
                background-image: url(https://i9.createsend1.com/static/eb/master/13-the-blueprint-3/images/forward@2x.png) !important;
            }
        }

        @media (max-width: 321px) {
            .fixed-width.has-border .layout__inner {
                border-width: 1px 0 !important;
            }

            .layout,
            .column {
                min-width: 320px !important;
                width: 320px !important;
            }

            .border {
                display: none;
            }
        }

        .mso div {
            border: 0 none white !important;
        }

        .mso .w560 .divider {
            Margin-left: 260px !important;
            Margin-right: 260px !important;
        }

        .mso .w360 .divider {
            Margin-left: 160px !important;
            Margin-right: 160px !important;
        }

        .mso .w260 .divider {
            Margin-left: 110px !important;
            Margin-right: 110px !important;
        }

        .mso .w160 .divider {
            Margin-left: 60px !important;
            Margin-right: 60px !important;
        }

        .mso .w354 .divider {
            Margin-left: 157px !important;
            Margin-right: 157px !important;
        }

        .mso .w250 .divider {
            Margin-left: 105px !important;
            Margin-right: 105px !important;
        }

        .mso .w148 .divider {
            Margin-left: 54px !important;
            Margin-right: 54px !important;
        }

        .mso .size-8,
        .ie .size-8 {
            font-size: 8px !important;
            line-height: 14px !important;
        }

        .mso .size-9,
        .ie .size-9 {
            font-size: 9px !important;
            line-height: 16px !important;
        }

        .mso .size-10,
        .ie .size-10 {
            font-size: 10px !important;
            line-height: 18px !important;
        }

        .mso .size-11,
        .ie .size-11 {
            font-size: 11px !important;
            line-height: 19px !important;
        }

        .mso .size-12,
        .ie .size-12 {
            font-size: 12px !important;
            line-height: 19px !important;
        }

        .mso .size-13,
        .ie .size-13 {
            font-size: 13px !important;
            line-height: 21px !important;
        }

        .mso .size-14,
        .ie .size-14 {
            font-size: 14px !important;
            line-height: 21px !important;
        }

        .mso .size-15,
        .ie .size-15 {
            font-size: 15px !important;
            line-height: 23px !important;
        }

        .mso .size-16,
        .ie .size-16 {
            font-size: 16px !important;
            line-height: 24px !important;
        }

        .mso .size-17,
        .ie .size-17 {
            font-size: 17px !important;
            line-height: 26px !important;
        }

        .mso .size-18,
        .ie .size-18 {
            font-size: 18px !important;
            line-height: 26px !important;
        }

        .mso .size-20,
        .ie .size-20 {
            font-size: 20px !important;
            line-height: 28px !important;
        }

        .mso .size-22,
        .ie .size-22 {
            font-size: 22px !important;
            line-height: 31px !important;
        }

        .mso .size-24,
        .ie .size-24 {
            font-size: 24px !important;
            line-height: 32px !important;
        }

        .mso .size-26,
        .ie .size-26 {
            font-size: 26px !important;
            line-height: 34px !important;
        }

        .mso .size-28,
        .ie .size-28 {
            font-size: 28px !important;
            line-height: 36px !important;
        }

        .mso .size-30,
        .ie .size-30 {
            font-size: 30px !important;
            line-height: 38px !important;
        }

        .mso .size-32,
        .ie .size-32 {
            font-size: 32px !important;
            line-height: 40px !important;
        }

        .mso .size-34,
        .ie .size-34 {
            font-size: 34px !important;
            line-height: 43px !important;
        }

        .mso .size-36,
        .ie .size-36 {
            font-size: 36px !important;
            line-height: 43px !important;
        }

        .mso .size-40,
        .ie .size-40 {
            font-size: 40px !important;
            line-height: 47px !important;
        }

        .mso .size-44,
        .ie .size-44 {
            font-size: 44px !important;
            line-height: 50px !important;
        }

        .mso .size-48,
        .ie .size-48 {
            font-size: 48px !important;
            line-height: 54px !important;
        }

        .mso .size-56,
        .ie .size-56 {
            font-size: 56px !important;
            line-height: 60px !important;
        }

        .mso .size-64,
        .ie .size-64 {
            font-size: 64px !important;
            line-height: 63px !important;
        }
    </style>

    <style type="text/css">
        body {
            background-color: #fff
        }

        .logo a:hover, .logo a:focus {
            color: #859bb1 !important
        }

        .mso .layout-has-border {
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc
        }

        .mso .layout-has-bottom-border {
            border-bottom: 1px solid #ccc
        }

        .mso .border, .ie .border {
            background-color: #ccc
        }

        .mso h1, .ie h1 {
        }

        .mso h1, .ie h1 {
            font-size: 64px !important;
            line-height: 63px !important
        }

        .mso h2, .ie h2 {
        }

        .mso h2, .ie h2 {
            font-size: 30px !important;
            line-height: 38px !important
        }

        .mso h3, .ie h3 {
        }

        .mso h3, .ie h3 {
            font-size: 22px !important;
            line-height: 31px !important
        }

        .mso .layout__inner, .ie .layout__inner {
        }

        .mso .footer__share-button p {
        }

        .mso .footer__share-button p {
            font-family: sans-serif
        }
    </style>
    <meta name="robots" content="noindex,nofollow"></meta>
    <meta property="og:title" content="Car-asia"></meta>
    <style>
        .section2 h2{
            font-size: 20px !important;
            color: #fff !important;
            text-transform: uppercase;
            font-weight: 300;
            text-align: center;
            margin-bottom: 15px;
            padding: 20px;
        }
        .tbl-header{
            background-color: rgba(255,255,255,0.3);
        }
        .section2 .tbl-content{
            height:300px;
            overflow-x:auto !important;
            margin-top: 0px;
            border: 1px solid rgba(255,255,255,0.3);
        }
        .section2 th{
            padding: 20px 15px;
            text-align: left;
            font-weight: 500;
            font-size: 12px;
            color: #fff;
            text-transform: uppercase;
        }
        .section2 td{
            padding: 15px;
            text-align: left;
            vertical-align:middle;
            font-weight: 300;
            font-size: 16px;
            color: #fff;
            border-bottom: solid 1px rgba(255,255,255,0.1);
        }
        .section2 table{
            width:100%;
            table-layout: fixed;
        }

        /* demo styles */

        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,300,700);
        .section2{
            background-image: linear-gradient(to right, #ff8177 0%, #ff867a 0%, #ff8c7f 21%, #f99185 52%, #cf556c 78%, #b12a5b 100%);
            font-family: 'Roboto', sans-serif;
        }
        .section2{
            margin: 0px 20px;
        }


        /* follow me template */
        .made-with-love {
            margin-top: 40px;
            padding: 10px;
            clear: left;
            text-align: center;
            font-size: 10px;
            font-family: arial;
            color: #fff;
        }
        .made-with-love i {
            font-style: normal;
            color: #F50057;
            font-size: 14px;
            position: relative;
            top: 2px;
        }
        .made-with-love a {
            color: #fff;
            text-decoration: none;
        }
        .made-with-love a:hover {
            text-decoration: underline;
        }


        /* for custom scrollbar for webkit browser*/

        ::-webkit-scrollbar {
            width: 6px;
        }
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        }
        ::-webkit-scrollbar-thumb {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        }
    </style>
</head>
<!--[if mso]>
<body class="mso">
<![endif]-->
<!--[if !mso]><!-->
<body class="no-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;">
<!--<![endif]-->
<table class="wrapper"
       style="border-collapse: collapse;table-layout: fixed;min-width: 320px;width: 100%;background-color: #fff;"
       cellpadding="0" cellspacing="0" role="presentation">
    <tbody>
    <tr>
        <td>
            <div role="banner">
                <div class="preheader"
                     style="Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 167440px);">
                    <div style="border-collapse: collapse;display: table;width: 100%;">
                        <!--[if (mso)|(IE)]>
                        <table align="center" class="preheader" cellpadding="0" cellspacing="0" role="presentation">
                            <tr>
                                <td style="width: 280px" valign="top"><![endif]-->
                        <div class="snippet"
                             style="display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #adb3b9;font-family: sans-serif;">

                        </div>
                        <!--[if (mso)|(IE)]></td>
                        <td style="width: 280px" valign="top"><![endif]-->

                        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                    </div>
                </div>
                <div class="header"
                     style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);"
                     id="emb-email-header-container">
                    <!--[if (mso)|(IE)]>
                    <table align="center" class="header" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td style="width: 600px"><![endif]-->
                    <div class="logo emb-logo-margin-box"
                         style="font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,Tahoma,sans-serif;Margin-left: 20px;Margin-right: 20px;"
                         align="center">
                        <div class="logo-center" align="center" id="emb-email-header"><img
                                    style="display: block;height: auto;width: 100%;border: 0;max-width: 203px;"
                                    src="https://i1.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/logo-asia.png" alt=""
                                    width="203"></div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </div>
            </div>
            <div role="section">
                <div style="background-color: #4b5462;background-position: 0px 0px;background-image: url(https://i1.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/header-img361.jpg);background-repeat: repeat;">
                    <div class="layout one-col"
                         style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                <tr class="layout-full-width"
                                    style="background-color: #4b5462;background-position: 0px 0px;background-image: url(https://i1.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/header-img361.jpg);background-repeat: repeat;">
                                    <td class="layout__edges">&nbsp;</td>
                                    <td style="width: 600px" class="w560"><![endif]-->
                            <div class="column"
                                 style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 110px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <h1 class="size-64"
                                            style="Margin-top: 0;Margin-bottom: 20px;font-style: normal;font-weight: normal;color: #000;font-size: 44px;line-height: 50px;font-family: avenir,sans-serif;text-align: center;"
                                            lang="x-size-64"><span class="font-avenir"><strong><span
                                                            style="color:#ffffff">Thank you for using our service</span></strong></span>
                                        </h1>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 5px;font-size: 1px;">&nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div class="btn btn--flat btn--medium"
                                         style="Margin-bottom: 20px;text-align: center;">
                                        <![if !mso]><a
                                                style="border-radius: 4px;display: inline-block;font-size: 12px;font-weight: bold;line-height: 22px;padding: 10px 20px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #ffffff !important;background-color: #e31212;font-family: Avenir, sans-serif;"
                                                href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-r/">Book My
                                            Adventure</a><![endif]>
                                        <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p>
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                     href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-r/"
                                                     style="width:152px" arcsize="10%" fillcolor="#E31212" stroke="f">
                                            <v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px">
                                                <center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Avenir,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">
                                                    Book My Adventure
                                                </center>
                                            </v:textbox>
                                        </v:roundrect><![endif]--></div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 85px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                            </div>
                            <!--[if (mso)|(IE)]></td>
                            <td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>

                <div style="mso-line-height-rule: exactly;line-height: 15px;font-size: 15px;">&nbsp;</div>








                <div class="section2">
                    <h2>Transaction Info</h2>
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                            <tr>
                                <td>Rental</td>
                                <td>{{ $rental[0]['name'] }}</td>
                                <td>Name</td>
                                <td>{{ $model->name }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ $model->name }}</td>
                                <td>Phone</td>
                                <td>{{ $model->phone }}</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>{{ $model->address }}</td>
                                <td>Tim departure</td>
                                <td>{{ $model->time_departure }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>$1.02</td>
                                <td>Booking Type</td>
                                <td>+2.36%</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>$1.38</td>
                                <td>Booking Status</td>
                                <td>-0.36%</td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>$2.38</td>
                                <td>Booking Note</td>
                                <td>-1.36%</td>
                            </tr>
                            <tr>
                                <td>Time Departure</td>
                                <td>$3.22</td>
                                <td>Payment Method</td>
                                <td>+1.36%</td>
                            </tr>
                            <tr>
                                <td>Time Arrival</td>
                                <td>$1.02</td>
                                <td>Payment Status</td>
                                <td>+2.36%</td>
                            </tr>
                            <tr>
                                <td>Flight Number</td>
                                <td>$1.38</td>
                                <td>Bank Code</td>
                                <td>-0.36%</td>
                            </tr>
                            <tr>
                                <td>Quantity Adult</td>
                                <td>$2.38</td>
                                <td>Bank Name</td>
                                <td>-1.36%</td>
                            </tr>
                            <tr>
                                <td>Payer Fee</td>
                                <td>$3.22</td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>






                <div class="layout one-col fixed-width"
                     style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                    <div class="layout__inner"
                         style="border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;">
                        <!--[if (mso)|(IE)]>
                        <table align="center" cellpadding="0" cellspacing="0" role="presentation">
                            <tr class="layout-fixed-width" style="background-color: #ffffff;">
                                <td style="width: 600px" class="w560"><![endif]-->
                        <div class="column"
                             style="text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);">

                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div style="mso-line-height-rule: exactly;line-height: 50px;font-size: 1px;">&nbsp;
                                </div>
                            </div>

                            <div style="Margin-left: 20px;Margin-right: 20px;">
                                <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                    <h2 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #e31212;font-size: 26px;line-height: 34px;font-family: Avenir,sans-serif;text-align: center;">
                                        <strong>Great prices on the great outdoors</strong></h2>
                                </div>
                            </div>

                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                    </div>
                </div>


                <div style="background-color: #fff;">
                    <div class="layout two-col"
                         style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                <tr class="layout-full-width" style="background-color: #fff;">
                                    <td class="layout__edges">&nbsp;</td>
                                    <td style="width: 300px" valign="top" class="w260"><![endif]-->
                            <div class="column"
                                 style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 25px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="font-size: 12px;font-style: normal;font-weight: normal;line-height: 19px;Margin-bottom: 20px;"
                                         align="center">
                                        <img style="border: 0;display: block;height: auto;width: 100%;max-width: 450px;"
                                             alt="Scuba diving" width="260"
                                             src="https://i1.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/travel-feature12.jpg">
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <h3 style="Margin-top: 0;Margin-bottom: 12px;font-style: normal;font-weight: normal;color: #281557;font-size: 18px;line-height: 26px;font-family: Avenir,sans-serif;">
                                            <strong>Scuba Diving for Two</strong></h3>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="font-size: 12px;font-style: normal;font-weight: normal;line-height: 19px;Margin-bottom: 20px;"
                                         align="left">
                                        <img style="border: 0;display: block;height: auto;width: 100%;max-width: 226px;"
                                             alt="" width="226"
                                             src="https://i2.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/ScreenShot2016-06-14at10.53.58AM1.png">
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <p style="Margin-top: 0;Margin-bottom: 20px;">If you&#8217;re called to explore
                                            the deep blue sea but have never been under before, this is the perfect way
                                            to get started. Beginners welcome!</p>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div class="btn btn--flat btn--medium"
                                         style="Margin-bottom: 20px;text-align: left;">
                                        <![if !mso]><a
                                                style="border-radius: 4px;display: inline-block;font-size: 12px;font-weight: bold;line-height: 22px;padding: 10px 20px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff !important;background-color: #281557;font-family: Avenir, sans-serif;"
                                                href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-y/">Book Now</a><![endif]>
                                        <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p>
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                     href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-y/"
                                                     style="width:99px" arcsize="10%" fillcolor="#281557" stroke="f">
                                            <v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px">
                                                <center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Avenir,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">
                                                    Book Now
                                                </center>
                                            </v:textbox>
                                        </v:roundrect><![endif]--></div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 15px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                            </div>
                            <!--[if (mso)|(IE)]></td>
                            <td style="width: 300px" valign="top" class="w260"><![endif]-->
                            <div class="column"
                                 style="Float: left;max-width: 320px;min-width: 300px; width: 320px;width: calc(12300px - 2000%);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 25px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="font-size: 12px;font-style: normal;font-weight: normal;line-height: 19px;Margin-bottom: 20px;"
                                         align="center">
                                        <img style="border: 0;display: block;height: auto;width: 100%;max-width: 450px;"
                                             alt="Plane in flight" width="260"
                                             src="https://i3.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/travel-feature22.jpg">
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <h3 style="Margin-top: 0;Margin-bottom: 12px;font-style: normal;font-weight: normal;color: #281557;font-size: 18px;line-height: 26px;font-family: Avenir,sans-serif;">
                                            <strong>Double&nbsp;Flying Lesson</strong></h3>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="font-size: 12px;font-style: normal;font-weight: normal;line-height: 19px;Margin-bottom: 20px;"
                                         align="left">
                                        <img style="border: 0;display: block;height: auto;width: 100%;max-width: 213px;"
                                             alt="" width="213"
                                             src="https://i4.createsend1.com/ei/j/C0/2CF/8D3/135930/csfinal/ScreenShot2016-06-14at10.54.54AM2.png">
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <p style="Margin-top: 0;Margin-bottom: 20px;">Feel the thrill of piloting a
                                            light aircraft, then sit back, and observe as somebody else takes the
                                            controls in this Land Away Double Flying Lesson.</p>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div class="btn btn--flat btn--medium" style="text-align:left;">
                                        <![if !mso]><a
                                                style="border-radius: 4px;display: inline-block;font-size: 12px;font-weight: bold;line-height: 22px;padding: 10px 20px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff !important;background-color: #281557;font-family: Avenir, sans-serif;"
                                                href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-j/">Book Now</a><![endif]>
                                        <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p>
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                     href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-j/"
                                                     style="width:99px" arcsize="10%" fillcolor="#281557" stroke="f">
                                            <v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px">
                                                <center style="font-size:12px;line-height:22px;color:#FFFFFF;font-family:Avenir,sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">
                                                    Book Now
                                                </center>
                                            </v:textbox>
                                        </v:roundrect><![endif]--></div>
                                </div>

                            </div>
                            <!--[if (mso)|(IE)]></td>
                            <td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>

                <div style="mso-line-height-rule: exactly;line-height: 50px;font-size: 50px;">&nbsp;</div>

                <div style="background-color: #281557;">
                    <div class="layout one-col"
                         style="Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;">
                        <div class="layout__inner" style="border-collapse: collapse;display: table;width: 100%;">
                            <!--[if (mso)|(IE)]>
                            <table width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                <tr class="layout-full-width" style="background-color: #281557;">
                                    <td class="layout__edges">&nbsp;</td>
                                    <td style="width: 600px" class="w560"><![endif]-->
                            <div class="column"
                                 style="max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);text-align: left;color: #8e959c;font-size: 14px;line-height: 21px;font-family: sans-serif;">

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 50px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;mso-text-raise: 4px;">
                                        <h1 class="size-48"
                                            style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #000;font-size: 36px;line-height: 43px;font-family: avenir,sans-serif;text-align: center;"
                                            lang="x-size-48"><span class="font-avenir"><strong><span
                                                            style="color:#ffffff">20% OFF</span></strong></span></h1>
                                        <h2 class="size-28"
                                            style="Margin-top: 20px;Margin-bottom: 16px;font-style: normal;font-weight: normal;color: #e31212;font-size: 24px;line-height: 32px;font-family: Avenir,sans-serif;text-align: center;"
                                            lang="x-size-28"><strong><span style="color:#fff">All outdoor trips to Yosemite</span></strong>
                                        </h2>
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 15px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div class="btn btn--flat btn--medium"
                                         style="Margin-bottom: 20px;text-align: center;">
                                        <![if !mso]><a
                                                style="border-radius: 4px;display: inline-block;font-size: 12px;font-weight: bold;line-height: 22px;padding: 10px 20px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #e31212 !important;background-color: #ffffff;font-family: sans-serif;"
                                                href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-t/">Book Now</a><![endif]>
                                        <!--[if mso]><p style="line-height:0;margin:0;">&nbsp;</p>
                                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                     href="https://carasia.createsend1.com/t/j-l-ndtdhz-l-t/"
                                                     style="width:98px" arcsize="10%" fillcolor="#FFFFFF" stroke="f">
                                            <v:textbox style="mso-fit-shape-to-text:t" inset="0px,9px,0px,9px">
                                                <center style="font-size:12px;line-height:22px;color:#E31212;font-family:sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px">
                                                    Book Now
                                                </center>
                                            </v:textbox>
                                        </v:roundrect><![endif]--></div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 35px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                                <div style="Margin-left: 20px;Margin-right: 20px;">
                                    <div style="mso-line-height-rule: exactly;line-height: 20px;font-size: 1px;">
                                        &nbsp;
                                    </div>
                                </div>

                            </div>
                            <!--[if (mso)|(IE)]></td>
                            <td class="layout__edges">&nbsp;</td></tr></table><![endif]-->
                        </div>
                    </div>
                </div>


            </div>
        </td>
    </tr>
    </tbody>
</table>
<script>
    // '.tbl-content' consumed little space for vertical scrollbar, scrollbar width depend on browser/os/platfrom. Here calculate the scollbar width .
    $(window).on("load resize ", function() {
        var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
        $('.tbl-header').css({'padding-right':scrollWidth});
    }).resize();
</script>
</body>
</html>