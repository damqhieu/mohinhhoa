@extends('layouts.app')
@section('content')
    <div class="main-container col1-layout content-color color color-light-gray">
        <div class="breadcrumbs">
            <div class="container">
                <ul>
                    <li class="home"><a href="{{ route('home') }}" title="Go to Home Page">Home</a></li>
                    <li><strong>Booking detail</strong></li>
                </ul>
            </div>
        </div><!--- .breadcrumbs-->

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ $imgQRCode }}" style="margin: 0 auto;" class="img-responsive">
                </div>
                <br>
                <div class="col-md-6 pull-left">
                    <div class="info-portfolio-box">
                        <h2>Transaction Info</h2>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Client:</div>
                            <div class="right pull-right"><a href="javascript:;">{{ $transactionBooking->name }}</a>
                            </div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Order Code:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->order_code }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Transaction Code:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->transaction_code }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Email:</div>
                            <div class="right pull-right"><a href="javascript:;">{{ $transactionBooking->email }}</a>
                            </div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Phone:</div>
                            <div class="right pull-right"><a href="javascript:;">{{ $transactionBooking->phone }}</a>
                            </div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Address:</div>
                            <div class="right pull-right"><a href="javascript:;">{{ $transactionBooking->address }}</a>
                            </div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Time Departure:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->time_departure }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Time Arrival:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->time_arrival }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Flight Number:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->flight_number }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Quantity Adult:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->quantity_adult }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Quantity Child:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->quantity_child }}</a></div>
                        </div>
                    </div><!--- .info-portfolio-box-->
                </div>
                <div class="col-md-6 pull-right">
                    <div class="info-portfolio-box">
                        <h2>Transaction Info</h2>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Departure Date:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->departure_date }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Return Date:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->return_date ?? 'No' }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Booking Type:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->booking_type }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Booking Status:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ ucfirst($transactionBooking->booking_status) }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Booking Note:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->booking_note }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Payment Method:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ $transactionBooking->payment_method }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Payment Status:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ ucfirst($transactionBooking->payment_status) }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Bank Code:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ ucfirst($transactionBooking->bank_code) }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Bank Name:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ ucfirst($transactionBooking->bank_name) }}</a></div>
                        </div>
                        <div class="row-content">
                            <div class="left pull-left"><i class="fa fa-user"></i> Payer Fee:</div>
                            <div class="right pull-right"><a
                                        href="javascript:;">{{ ucfirst($transactionBooking->payer_fee) }}</a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!--- .container -->
    </div><!--- .main-container -->
@endsection