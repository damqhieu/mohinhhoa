<label class="switch">
    <input type="checkbox" {{ ($active) ? 'checked' : '' }}>
    <span class="slider round"></span>
</label>