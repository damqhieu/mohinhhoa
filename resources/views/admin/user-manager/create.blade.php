@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('user-admins.store') }}" method="post">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Name') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="name"
                                                       required
                                                       value="{{ old('name') }}"
                                                       placeholder="{{ __('Enter name') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Email') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                <input type="email" class="form-control"
                                                       name="email"
                                                       required
                                                       value="{{ old('email') }}"
                                                       placeholder="{{ __('Enter email') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Type') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                <select name="type" class="form-control">
                                                    @foreach(ROLES as $item)
                                                        <option value="{{ $item }}">{{ $item }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('user-admins.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
