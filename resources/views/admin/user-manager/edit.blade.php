@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::model($user, ['method' => 'PATCH', 'route' => ['user-admins.update', $user->id]]) !!}
                            @csrf
                            @method('PUT')
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Name') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                {!! Form::text('name', old('name'),['class' => 'form-control']) !!}
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Email') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                {!! Form::text('email', old('email'),['class' => 'form-control']) !!}
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Type') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                {!! Form::select('type', ROLES, old('type'), ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('user-admins.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
