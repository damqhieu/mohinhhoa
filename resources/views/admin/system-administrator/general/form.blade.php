@extends('layouts.app-admin')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('cms-categories.index') }}" class="btn btn-success "><i
                                        class="fa fa-arrow-circle-left"></i> <strong>{{ __('Back') }}</strong></a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('system-general.save') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#meta">{{ __('SEO') }}</a></li>
                                <li><a data-toggle="tab" href="#media">{{ __('Media') }}</a></li>
                                <li><a data-toggle="tab" href="#link-social">{{ __('Link Social') }}</a></li>
                                <li><a data-toggle="tab" href="#google-component">{{ __('Google and Component') }}</a>
                                </li>
                                <li><a data-toggle="tab" href="#style-script">{{ __('Style and Script') }}</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Name Website') !!}
                                                <input type="text" value="{{ $model->name }}" name="name" class="form-control">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Domain') !!}
                                                <input type="text" value="{{ $model->domain }}" name="domain" class="form-control">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Author') !!}
                                                <input type="text" value="{{ $model->author }}" name="author" class="form-control">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Email') !!}
                                                <input type="text" class="form-control" name="email" value="{{ $model->email }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Hotline') !!}
                                                <input type="text" class="form-control" name="hotline" value="{{ $model->hotline }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('title', 'Address') !!}
                                                <input type="text" class="form-control" name="address" value="{{ $model->address }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="meta" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Tag Title') }}</label>
                                        <textarea class="form-control" name="tag_title" rows="3"
                                                  placeholder="Enter Tag Title">{{ $model->tag_title }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta_Author') }}</label>
                                        <textarea class="form-control" name="meta_author" rows="3"
                                                  placeholder="Enter Meta_Author">{{ $model->meta_author }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Keywords') }}</label>
                                        <textarea class="form-control" name="meta_keywords" rows="3"
                                                  placeholder="Enter Meta Keywords">{{ $model->meta_keywords }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Description') }}</label>
                                        <textarea class="form-control" name="meta_description" rows="3"
                                                  placeholder="Enter Meta Description">{{ $model->meta_description }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Canonical') }}</label>
                                        <textarea class="form-control" name="meta_canonical" rows="3"
                                                  placeholder="Enter Meta Canonical">{{ $model->meta_canonical }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                </div>
                                <div id="google-component" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Google Analytics') }}</label>
                                        <textarea class="form-control" name="google_analytics" rows="3"
                                                  placeholder="Enter Tag Title">{{  $model->google_analytics }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Google Webmaster') }}</label>
                                        <textarea class="form-control" name="google_webmaster" rows="3"
                                                  placeholder="Enter Meta_Author">{{ $model->google_webmaster }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Google Adsense') }}</label>
                                        <textarea class="form-control" name="google_adsense" rows="3"
                                                  placeholder="Enter Meta Keywords">{{ $model->google_adsense }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Google Map') }}</label>
                                        <textarea class="form-control" name="google_map" rows="3"
                                                  placeholder="Enter Meta Description">{{ $model->google_map }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Social Share') }}</label>
                                        <textarea class="form-control" name="social_share" rows="3"
                                                  placeholder="Enter Meta Canonical">{{ $model->social_share }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Live Chat') }}</label>
                                        <textarea class="form-control" name="live_chat" rows="3"
                                                  placeholder="Enter Meta Canonical">{{ $model->live_chat }}</textarea>
                                    </div>
                                </div>
                                <div id="media" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Logo') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_logo">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($model->img_logo) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Banner') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_banner">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($model->img_banner) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Favicon') }}: </label>
                                        <input type="file" class="form-control"
                                               name="favicon">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="50" src="{{ $model->favicon }}" alt="">
                                    </div>
                                </div>
                                <div id="style-script" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Style') }}: </label>
                                        <textarea name="style" cols="30" rows="10"
                                                  class="form-control"> {{ $model->style }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Style') }}: </label>
                                        <textarea name="script" cols="30" rows="10"
                                                  class="form-control"> {{ $model->script }}</textarea>
                                    </div>
                                </div>
                                <div id="link-social" class="tab-pane fade">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Facebook') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_facebook" value="{{ $model->link_facebook }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Google Plus') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_google_plus" value="{{ $model->link_google_plus }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Twitter') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_twitter" value="{{ $model->link_twitter }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Linkedin') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_linkedin" value="{{ $model->link_linkedin }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Vimeo') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_vimeo" value="{{ $model->link_vimeo }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Skype') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="link_skype" value="{{ $model->link_skype }}">
                                                <span class="help-block"> Max length 191 chars  </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                </div>
                            </div>
                        </form>
                        {{--</form>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
