@extends('layouts.app-admin')
@section('style')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <form method="post" action="{{ route('system-setting.save', $model->id ?? null) }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('title', 'Key') !!}
                                        <input @if(!empty($model->key)) readonly @endif type="text" name="key"
                                               class="form-control" value="{{ $model->key ?? '' }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ __('Locale') }}: <span class="required text-danger"> * </span></label>
                                        <select name="locale" id="Locale" class="form-control">
                                            @foreach(locale() as $key => $value)
                                                <option @if(isset($model->locale) && $model->locale == $key) selected
                                                        @endif value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('title', 'Value') !!}
                                        <textarea name="value" id="value"
                                                  class="form-control">{{ $model->value ?? '' }}</textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" value="Submit" name="btnSubmit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('STT') }}</th>
                                <th>{{ __('Key') }}</th>
                                <th>{{ __('Value') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datas as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->key }}</td>
                                    <td>{!! str_limit( $item->value,200) !!} </td>
                                    <td class="text-center">
                                        <a href="{{ route('system-setting.save', $item->id) }}"
                                           class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            CKEDITOR.replace('value');
        });
    </script>
    <script>
        $(function () {
            $('#db_table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        });
    </script>
@endsection
