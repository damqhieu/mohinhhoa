@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('transport-schedules.create') }}" class="btn btn-success "><i class="fa fa-plus-square"></i> <strong>Create</strong></a>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>{{ __('Trip') }}</th>
                                <th>{{ __('Start time') }}</th>
                                <th>{{__('End time') }}</th>
                                <th>{{__('Is_Active') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($schedules as $schedule)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><strong>{{ $schedule->transport_trip->departure_place->name . " - " . $schedule->transport_trip->destination_place->name }}</strong></td>
                                    <td>{{ $schedule->start_time }}</td>
                                    <td>{{ $schedule->end_time }}</td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" {{ ($schedule->is_active ) ? 'checked' : '' }}>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('transport-schedules.edit', $schedule->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('transport-schedules.destroy', $schedule->id) }}" class="btn btn-danger btn-xs destroy"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#db_table').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        });
    </script>
@endsection
