@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transport-schedules.store') }}" method="post">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Trip') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <select name="transport_trip_id" class="form-control ">
                                                @foreach($trips as $trip)
                                                    <option value="{{ $trip->id }}"> {{ $trip->departure_place->name }}
                                                        - {{ $trip->destination_place->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>{{ __('Start time') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="start_time"  value="{{ old('start_time') }}"
                                                       class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('End time') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="end_time" value="{{ old('end_time') }}" class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Description') }}:</label>
                                            <textarea name="description" class="overview"
                                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                      placeholder="Place some text here">{{ old('overview') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class=" control-label">{{ __('Active') }}: </label>
                                            <div>
                                                <input type="checkbox" name="is_active" checked>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                        <input class="btn btn-default" type="reset" value="Reset">
                                        <a href="{{ route('transport-schedules.index') }}"
                                           class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">

    <link rel="stylesheet" href="{{ asset('backend/timepicker/stylesheets/wickedpicker.css') }}">


@section('script')
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="{{ asset('backend/timepicker/src/wickedpicker.js') }}"></script>

    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $('.timepicker').wickedpicker({
            twentyFour: true, title:
                'Time   ', showSeconds: false
        });
    </script>


@endsection
