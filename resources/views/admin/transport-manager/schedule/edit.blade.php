@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transport-schedules.update', $schedule->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Trip') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <select name="transport_trip_id" class="form-control ">
                                                @foreach($trips as $trip)
                                                    <option @if($trip->id == $schedule->transport_trip_id) selected
                                                            @endif value="{{ $trip->id }}"> {{ $trip->departure_place->name }}
                                                        - {{ $trip->destination_place->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>{{ __('Start time') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="start_time" value="{{ $schedule->start_time }}" class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('End time') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="end_time" value="{{ $schedule->end_time }}" class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Description') }}:</label>
                                            <textarea name="description" class="overview"
                                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                      placeholder="Place some text here">{{ $schedule->description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Active') }}: </label>

                                                <input type="checkbox" name="is_active"
                                                       @if($schedule->is_active) checked @endif>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="text-center">
                                        <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                        <input class="btn btn-default" type="reset" value="Reset">
                                        <a href="{{ route('transport-schedules.index') }}"
                                           class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('backend/timepicker/stylesheets/wickedpicker.css') }}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border-color: #367fa9 !important;
            padding: 1px 10px !important;
            color: #fff;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-weight: bold;
            margin-right: 2px;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script type="text/javascript" src="{{ asset('backend/timepicker/src/wickedpicker.js') }}"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script>
        $('.timepicker').wickedpicker({
            twentyFour: false, title:
                'Time   ', showSeconds: false
        });
    </script>

@endsection
