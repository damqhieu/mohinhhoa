@section('style')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('transport-trips.create') }}" class="btn btn-success "><i
                                        class="fa fa-plus-square"></i> <strong>Create</strong></a>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Departure') }}</th>
                                <th>{{ __('Destination') }}</th>
                                <th>{{__('Price') }}</th>
                                <th>{{__('Is_Active') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trips as $trip)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $trip->name  }}</td>
                                    <td>
                                        @foreach($places as $place)
                                            @if($trip->departure == $place->id) {{$place->name}} @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($places as $place)
                                            @if($trip->destination == $place->id) {{$place->name}} @endif
                                        @endforeach
                                    </td>
                                    <td>{{ $trip->price . " " . $trip->currency }}</td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" {{ ($trip->is_active ) ? 'checked' : '' }}>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('transport-trips.edit', $trip->id) }}"
                                           class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('transport-trips.destroy', $trip->id) }}"
                                           class="btn btn-danger btn-xs destroy"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#db_table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
        });
    </script>
@endsection
