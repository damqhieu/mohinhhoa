@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transport-trips.update', $trip->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#meta">{{ __('SEO') }}</a></li>
                                <li><a data-toggle="tab" href="#media">{{ __('Media') }}</a></li>
                                <li><a data-toggle="tab" href="#component">{{ __('Component') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Name') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <input type="text" class="form-control" name="name"
                                                   value="{{ $trip->name }}">
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Departure') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <select class="select form-control" name="departure" id="">
                                                @foreach($places as $place)
                                                    <option @if($trip->departure == $place->id) selected
                                                            @endif value="{{$place->id}}">{{ $place->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Destination') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <select class="select form-control" name="destination" id="">
                                                @foreach($places as $place)
                                                    <option @if($trip->destination == $place->id) selected
                                                            @endif value="{{$place->id}}">{{ $place->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('Price') }}:</label>
                                            <input type="number" class="form-control" name="price"
                                                   value="{{ $trip->price }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('Price Sale') }}:</label>
                                            <input type="number" class="form-control" name="price_sale"
                                                   value="{{ $trip->price_sale }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('Discount') }}:</label>
                                            <input type="number" class="form-control" name="discount"
                                                   value="{{ $trip->discount }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>{{ __('Currency') }}:</label>
                                            <select class="form-control" name="currency" id="">
                                                <option value="USD">USD</option>
                                                <option value="VND">VND</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Locale') }}:</label>
                                            <select name="locale" id="Locale" class="form-control">
                                                @foreach(locale() as $key => $value)
                                                    <option @if($trip->locale === $key) selected
                                                            @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="contents" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Overview') }}:</label>
                                        <textarea name="overview" class="overview"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                  placeholder="Place some text here">
                                            {{ $trip->overview }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Description') }}:</label>
                                        <textarea name="description" class="description"
                                                  placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                            {{ $trip->description }}
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Content') }}:</label>
                                        <textarea name="content" id="content" cols="30" rows="10">
                                            {{ $trip->content }}
                                        </textarea>
                                    </div>
                                </div>
                                <div id="meta" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label for="tags">Tags:</label>
                                        <select class="form-control select2" id="tags" name="tags[]"
                                                multiple="multiple" placeholder="Select multiple tags"
                                                style="width: 100%;">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Tag Title') }}</label>
                                        <textarea class="form-control" name="tag_title" rows="3"
                                                  placeholder="Enter Tag Title">{{ $trip->tag_title }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta_Author') }}</label>
                                        <textarea class="form-control" name="meta_author" rows="3"
                                                  placeholder="Enter Meta_Author">{{ $trip->meta_author }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Keywords') }}</label>
                                        <textarea class="form-control" name="meta_keywords" rows="3"
                                                  placeholder="Enter Meta Keywords">{{ $trip->meta_keywords }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Description') }}</label>
                                        <textarea class="form-control" name="meta_description" rows="3"
                                                  placeholder="Enter Meta Description">{{ $trip->meta_description }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Canonical') }}</label>
                                        <textarea class="form-control" name="meta_canonical" rows="3"
                                                  placeholder="Enter Meta Canonical">{{ $trip->meta_canonical }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                </div>
                                <div id="media" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Image ') }}: </label>
                                        <input type="file" class="form-control"
                                               name="image">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($trip->image) }}" alt="">
                                    </div>

                                </div>
                                <div id="component" class="tab-pane fade">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Active') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_active"
                                                           @if($trip->is_active) checked @endif>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Hot') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_hot"
                                                           @if($trip->is_hot) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Top') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_top"
                                                           @if($trip->is_top) checked @endif>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('New') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_new"
                                                           @if($trip->is_new) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('transport-trips.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border-color: #367fa9 !important;
            padding: 1px 10px !important;
            color: #fff;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-weight: bold;
            margin-right: 2px;
        }

    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $("[name='is_top']").bootstrapSwitch();
        $("[name='is_hot']").bootstrapSwitch();
        $("[name='is_new']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
    </script>
@endsection
