@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transport-rentals.update', $rental->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Name') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="name" value="{{ $rental->name }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Image') }}: </label>
                                        <input type="file" class="form-control"
                                               name="image">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($rental->image) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Description') }}:</label>
                                        <textarea name="description" class="description form-control" id="" cols="30" rows="10">{{ $rental->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Price') }}:</label>
                                        <input type="number" class="form-control" name="price" value="{{ $rental->price }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Currency') }}:</label>
                                        <select name="currency" class="form-control" id="">
                                            <option selected value="USD">USD</option>
                                            <option value="VND">VND</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('transport-rentals.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $(function () {
            //bootstrap WYSIHTML5 - text editor
            $('.description').wysihtml5();
        })
    </script>
@endsection
