@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transport-places.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#media">{{ __('Media') }}</a></li>
                                <li><a data-toggle="tab" href="#component">{{ __('Component') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Name') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="name">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Address') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control" name="address">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Latitude') }}:</label>
                                        <input type="text" class="form-control" name="latitude">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Longitude') }}:</label>
                                        <input type="text" class="form-control" name="longitude">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Locale') }}:</label>
                                        <select name="locale" id="Locale" class="form-control">
                                            @foreach(locale() as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="media" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Image') }}: </label>
                                        <input type="file" class="form-control" name="image">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                    </div>
                                </div>
                                <div id="component" class="tab-pane fade">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Active') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_active" checked>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Hot') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_hot">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Top') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_top">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('New') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_new">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('transport-places.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border-color: #367fa9 !important;
            padding: 1px 10px !important;
            color: #fff;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-weight: bold;
            margin-right: 2px;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $("[name='is_top']").bootstrapSwitch();
        $("[name='is_hot']").bootstrapSwitch();
        $("[name='is_new']").bootstrapSwitch();
    </script>


@endsection
