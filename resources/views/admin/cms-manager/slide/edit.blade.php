@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-slides.update', $cmsSlide->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#slide-element">{{ __('Slide Element') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{ __('Title') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="title"
                                                       value="{{ $cmsSlide->title }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{ __('Alt') }}:</label>
                                                <input type="text" class="form-control"
                                                       name="alt"
                                                       value="{{ $cmsSlide->alt }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Image') }}: <span class="required text-danger"> * </span></label>
                                        <input type="file" class="form-control"
                                               name="image">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($cmsSlide->image) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-1 control-label">{{ __('Active') }}: </label>
                                        <div class="col-md-11">
                                            <input @if($cmsSlide->is_active ) checked @endif type="checkbox"
                                                   name="is_active">
                                        </div>
                                    </div>
                                </div>
                                <div id="slide-element" class="tab-pane fade">
                                    <br/>
                                    <table class="table table-bordered table-responsive table-hover table-slide-element">
                                        <tr>
                                            <th>{{ __('STT') }}</th>
                                            <th style="width: 35%;">{{ __('Title') }}</th>
                                            <th class="text-center" style="width: 15%;">{{ __('Url') }}</th>
                                            <th class="text-center">{{ __('Target URL') }}</th>
                                            <th class="text-center">{{ __('Action') }}</th>
                                        </tr>
                                        @foreach($cmsSlide->slide_elements as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td class="text-center">{{ $item->url }}</td>
                                                <td class="text-center">{{ $item->target }}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('cms-slide-elements.edit', $item->id) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:;" data-url-element="{{ route('cms-slide-elements.destroy', $item->id) }}"  onclick="destroyElement(this)" class="btn btn-xs btn-danger"><i class="fa fa-pencil"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a class="btn btn-danger" href="{{ route('cms-slides.index') }}">Back to list</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function destroyElement(_this) {
            let url = $(_this).attr('data-url-element');

            bootbox.confirm({
                title: "Destroy?",
                message: "Are you sure?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            success: function (rs) {
                                if (rs.status === 204) {
                                    $(_this).parents().eq(1).remove();
                                    bootbox.alert("Destroy success!");
                                } else {
                                    bootbox.alert("Destroy not success!");
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("Status: " + textStatus);
                                alert("Error: " + errorThrown);
                            }
                        })
                    }
                }
            });
        }
    </script>
@endsection
