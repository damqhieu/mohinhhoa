@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-slide-elements.update', $slideElement->id) }}" method="post">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="tags">Slide: <span class="required text-danger"> * </span></label>
                                                <select class="form-control select2" id="cms_slide_id"
                                                        name="cms_slide_id" placeholder="Select multiple tags"
                                                        style="width: 100%;">
                                                    <option value="">Chose slide</option>
                                                </select>
                                                <span class="help-block"> Chose slide not null </span>
                                            </div>
                                            <div class="form-group">
                                                <label>{{ __('Title') }}:
                                                    <span class="required text-danger"> * </span>
                                                </label>
                                                <input type="text" class="form-control"
                                                       name="title"
                                                       value="{{ $slideElement->title }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{ __('Url') }}:</label>
                                                <input type="text" class="form-control"
                                                       name="url"
                                                       value="{{ $slideElement->url }}">
                                                <span class="help-block"> It must is link </span>
                                            </div>
                                            <div class="form-group">
                                                <label>{{ __('Target') }}:</label>
                                                <select name="target" class="form-control">
                                                    @foreach(target_link() as $item)
                                                        <option @if($slideElement->target == $item) selected @endif value="{{ $item }}">{{ $item }}</option>
                                                    @endforeach
                                                </select>
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a class="btn btn-info" href="{{ route('cms-slides.edit', $slideElement->cms_slide_id) }}">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $(function () {
            $('#cms_slide_id').select2({
                data: {!! json_encode($slide, true) !!},
                tags: true
            }).val({{json_encode($slideElement->cms_slide_id)}}).trigger('change');
        })
    </script>
@endsection
