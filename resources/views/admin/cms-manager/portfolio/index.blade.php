@section('style')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('cms-portfolios.create') }}" class="btn btn-success "><i
                                        class="fa fa-plus-square"></i> <strong>Create</strong></a>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>{{ __('STT') }}</th>
                                <th>{{ __('Category') }}</th>
                                <th>{{ __('Image') }}</th>
                                <th>{{__('Title') }}</th>
                                <th>{{__('Active') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cmsPortfolios as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->CmsCategory->name }}</td>
                                    <td><img width="200px" src="{{ asset($item->image) }}" alt=""></td>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" {{ ($item->is_active ) ? 'checked' : '' }}>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:;" class="btn btn-success btn-xs"
                                           onclick="showSlideElement(this)" data-slider-id="{{ $item->id }}"
                                           data-toggle="modal" data-target="#slide-element" title="Slide element"><i
                                                    class="fa fa-plus-circle"></i></a>
                                        <a href="{{ route('cms-portfolios.edit', $item->id) }}" class="btn btn-info btn-xs"><i
                                                    class="fa fa-pencil"></i></a>
                                        <a href="{{ route('cms-portfolios.destroy', $item->id) }}"
                                           class="btn btn-danger btn-xs destroy"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <div class="modal fade" id="slide-element" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">List Slide Element </h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-responsive table-hover table-slide-element">
                        <tr>
                            <th style="width: 35%;">{{ __('Category') }}</th>
                            <th class="text-center" style="width: 15%;">{{ __('Url') }}</th>
                            <th class="text-center">{{ __('Target') }}</th>
                            <th class="text-center">{{ __('Action') }}</th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('cms-slide-elements.create') }}" class="btn btn-info">Create</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#db_table').DataTable()
            ({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function showSlideElement(_this) {
            $.ajax({
                url: '{{ route('cms-slide-elements.index') }}',
                type: 'post',
                dataType: 'json',
                data: {id: $(_this).attr('data-slider-id')},
                success: function (rs) {
                    if (rs.data) {
                        let html = '';
                        for (let i = 0; i < rs.data.length; i++) {
                            url = "{{ route('cms-slide-elements.destroy') }}" + '/' + rs.data[i]['id'];
                            html += '<tr>';
                            html += '<td>' + rs.data[i].title + '</td>';
                            html += '<td class="text-center">' + rs.data[i].url + '</td>';
                            html += '<td class="text-center">' + rs.data[i].target + '</td>';
                            html += '<td class="text-center">' + '<a href="javascript:;" data-url-element="' + url + '" class="btn btn-warning" onclick="destroyElement(this)" style="color: #ffffff;">Delete</a></td>';
                            html += '</tr>';
                        }
                        $('.table-slide-element tr:not(":nth-child(1)")').remove();
                        $('.table-slide-element').append(html);
                    } else {
                        $('.table-slide-element tr:not(":nth-child(1)")').remove();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                }
            })
        }

        function destroyElement(_this) {
            let url = $(_this).attr('data-url-element');

            bootbox.confirm({
                title: "Destroy?",
                message: "Are you sure?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            success: function (rs) {
                                if (rs.status === 204) {
                                    $(_this).parents().eq(1).remove();
                                    bootbox.alert("Destroy success!");
                                } else {
                                    bootbox.alert("Destroy not success!");
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("Status: " + textStatus);
                                alert("Error: " + errorThrown);
                            }
                        })
                    }
                }
            });
        }

        $('.destroy').click(function (e) {
            e.preventDefault();
            var _this = this;
            var url = $(this).attr('href');

            bootbox.confirm({
                title: "Destroy?",
                message: "Are you sure?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            success: function (rs) {
                                if (rs.status === 204) {
                                    $(_this).parents().eq(1).remove();
                                    bootbox.alert("Destroy success!");
                                } else {
                                    bootbox.alert("Destroy not success!");
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("Status: " + textStatus);
                                alert("Error: " + errorThrown);
                            }
                        })
                    }
                }
            });
        })
    </script>
@endsection
