@extends('layouts.app-admin')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <br/>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::model($cmsFeeback, ['route' => ['cms-feedbacks.update', $cmsFeeback->id]],['id'=>'cms_update']) !!}
                        @csrf
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" class="tab-pane fade in active">
                                <div class="box-header">
                                    <h2 class="card-title">Tiêu đề: <strong>{{ $cmsFeeback->title }}</strong></h2>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('title', 'Name:') !!}
                                            <span class="required text-danger"> * </span>
                                            {!! form::text('name', old('name'),['class'=>'form-control',
                                            'placeholder'=>'Enter name','required'])!!}
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('title', 'Phone:') !!}
                                            {!! form::text('phone', old('phone'),['class'=>'form-control',
                                            'placeholder'=>'Enter phone'])!!}
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{{ __('Active') }}: </label>
                                            <br/>
                                            <input @if($cmsFeeback->is_active ) checked @endif type="checkbox" name="is_active">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('title', 'Email:') !!}
                                            {!! form::text('email', old('email'),['class'=>'form-control',
                                            'placeholder'=>'Enter email'])!!}
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('title', 'Address:') !!}
                                            {!! form::text('address', old('address'),['class'=>'form-control',
                                            'placeholder'=>'Enter address'])!!}
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                        <?php
                                        use App\Models\Cms\CmsFeedback;
                                        $status = array(
                                            CmsFeedback::STATUS_APPROVE => "Approve",
                                            CmsFeedback::STATUS_PENDING => "Pending",
                                        );
                                        ?>
                                        <div class="form-group">
                                            {!! Form::label('title', 'Status:') !!}
                                            <span class="required text-danger"> * </span>
                                            {!! form::select('status', $status, old('status'),['class'=>'form-control'])!!}
                                            <span class="help-block"> Max length 191 chars </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Content') }}:</label>
                                            {!! form::textarea('content', old('content'),['class'=>'form-control', 'id' => 'content'])!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                <input class="btn btn-default" type="reset" value="Reset">
                                <a class="btn btn-danger" href="{{ route('cms-feedbacks.index') }}">Back to list</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        {{--</form>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            CKEDITOR.replace('content');
        });
    </script>
@endsection
