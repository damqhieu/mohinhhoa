@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-pages.update', $cmsPage->id) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#contents">{{ __('Content') }}</a></li>
                                <li><a data-toggle="tab" href="#meta">{{ __('SEO') }}</a></li>
                                <li><a data-toggle="tab" href="#media">{{ __('Media') }}</a></li>
                                <li><a data-toggle="tab" href="#component">{{ __('Component') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Code page') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control" readonly
                                               value="{{ $cmsPage->code_page }}">
                                        <span class="help-block"> Max length 50 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Title') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control"
                                               name="title"
                                               id="title"
                                               required
                                               value="{{ $cmsPage->title }}"
                                               onkeyup="generateSlug('title', 'slug')"
                                               placeholder="{{ __('Enter title') }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Slug') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control"
                                               name="slug"
                                               id="slug"
                                               value="{{ $cmsPage->slug }}"
                                               required
                                               placeholder="{{ __('Enter slug') }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Author') }}:
                                        </label>
                                        <input type="text" class="form-control"
                                               name="author"
                                               value="{{ $cmsPage->author }}"
                                               placeholder="{{ __('Enter author') }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Locale') }}:</label>
                                        <select name="locale" id="Locale" class="form-control">
                                            @foreach(locale() as $key => $value)
                                                <option @if($cmsPage->locale === $key) selected
                                                        @endif value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="contents" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Overview') }}:</label>
                                        <textarea name="overview" class="overview"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                  placeholder="Place some text here">{{ $cmsPage->overview }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Description') }}:</label>
                                        <textarea name="description" class="description"
                                                  placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $cmsPage->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Content') }}:</label>
                                        <textarea name="content" id="content" cols="30" rows="10">{{ $cmsPage->content }}</textarea>
                                    </div>
                                </div>
                                <div id="meta" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label for="tags">Tags:</label>
                                        <select class="form-control select2" id="tags" name="tag[]" multiple="multiple"
                                                placeholder="Select multiple tags" style="width: 100%;">
                                            @php
                                                $tags = json_decode($cmsPage->tags)
                                            @endphp
                                            @if(!empty($tags))
                                                @foreach($tags as $item)
                                                    <option selected value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Tag Title') }}</label>
                                        <textarea class="form-control" name="tag_title" rows="3"
                                                  placeholder="Enter Tag Title">{{ $cmsPage->tag_title }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta_Author') }}</label>
                                        <textarea class="form-control" name="meta_author" rows="3"
                                                  placeholder="Enter Meta_Author">{{ $cmsPage->meta_author }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Keywords') }}</label>
                                        <textarea class="form-control" name="meta_keywords" rows="3"
                                                  placeholder="Enter Meta Keywords">{{ $cmsPage->meta_keywords }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Description') }}</label>
                                        <textarea class="form-control" name="meta_description" rows="3"
                                                  placeholder="Enter Meta Description">{{ $cmsPage->meta_description }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Canonical') }}</label>
                                        <textarea class="form-control" name="meta_canonical" rows="3"
                                                  placeholder="Enter Meta Canonical">{{ $cmsPage->meta_canonical }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                </div>
                                <div id="media" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Image Thumbnail') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_thumbnail">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($cmsPage->img_thumbnail) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Image Banner') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_banner">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($cmsPage->img_banner) }}" alt="">
                                    </div>

                                </div>
                                <div id="component" class="tab-pane fade">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Active') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_active"
                                                           @if($cmsPage->is_active) checked @endif>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Hot') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_hot"
                                                           @if($cmsPage->is_hot) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Top') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_top"
                                                           @if($cmsPage->is_top) checked @endif>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('New') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_new"
                                                           @if($cmsPage->is_new) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('cms-pages.index') }}" class="btn btn-danger">Back to list</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #3c8dbc !important;
            border-color: #367fa9 !important;
            padding: 1px 10px !important;
            color: #fff;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-weight: bold;
            margin-right: 2px;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $("[name='is_top']").bootstrapSwitch();
        $("[name='is_hot']").bootstrapSwitch();
        $("[name='is_new']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            $('#tags').select2({
                tags: true
            });
            {{--}).val({{json_encode($cmsPage->cms_tags->pluck('id'))}}).trigger('change');--}}
            CKEDITOR.replace('content');
            //bootstrap WYSIHTML5 - text editor
            $('.overview').wysihtml5();
            $('.description').wysihtml5();
        })
    </script>
@endsection
