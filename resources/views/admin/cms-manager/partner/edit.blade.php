@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-partners.update', $cmsPartner->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Name') }}: <span class="required text-danger"> * </span></label>
                                                <input type="text" class="form-control"
                                                       name="name"
                                                       value="{{ $cmsPartner->name }}"
                                                       placeholder="{{ __('Enter name') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Email') }}: </label>
                                                <input type="email" class="form-control"
                                                       name="email"
                                                       value="{{ $cmsPartner->email }}"
                                                       placeholder="{{ __('Enter email') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Phone') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="phone"
                                                       value="{{ $cmsPartner->phone }}"
                                                       placeholder="{{ __('Enter phone') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Address') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="address"
                                                       value="{{ $cmsPartner->address }}"
                                                       placeholder="{{ __('Enter address') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Website') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="website"
                                                       value="{{ $cmsPartner->website }}"
                                                       placeholder="{{ __('Enter website') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Image') }}: </label>
                                                <input type="file" class="form-control"
                                                       name="image">
                                                <span class="help-block"> Max:2MB|mimes:jpeg,png,jpg,gif,svg </span>
                                                <img src="{{ asset($cmsPartner->image) }}" width="100px">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Locale') }}: <span class="required text-danger"> * </span></label>
                                                <select name="locale" id="Locale" class="form-control">
                                                    @foreach(locale() as $key => $value)
                                                        <option @if($cmsPartner->locale == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Active') }}: </label>
                                                <br>
                                                <input type="checkbox" name="is_active" @if($cmsPartner->is_active) checked @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('Overview') }}: </label>
                                                <textarea name="overview" class="overview"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                          placeholder="Place some text here">{{ $cmsPartner->overview }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('cms-partners.index') }}" class="btn btn-danger">Back to list</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script>
        $(function () {
            $('.overview').wysihtml5();
        })
    </script>
@endsection
