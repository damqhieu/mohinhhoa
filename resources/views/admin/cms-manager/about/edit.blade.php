@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{route('cms-abouts.update', $cmsAbout->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#contents">{{ __('Content') }}</a></li>
                                <li><a data-toggle="tab" href="#meta">{{ __('SEO') }}</a></li>
                                <li><a data-toggle="tab" href="#media">{{ __('Media') }}</a></li>
                                <li><a data-toggle="tab" href="#component">{{ __('Component') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Title') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control"
                                               name="title"
                                               id="title"
                                               required
                                               value="{{ $cmsAbout->title }}"
                                               onkeyup="generateSlug('title', 'slug')"
                                               placeholder="{{ __('Enter title') }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Slug') }}:
                                            <span class="required text-danger"> * </span>
                                        </label>
                                        <input type="text" class="form-control"
                                               name="slug"
                                               id="slug"
                                               value="{{ $cmsAbout->slug }}"
                                               required
                                               placeholder="{{ __('Enter slug') }}">
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    {{--<div class="form-group">--}}
                                        {{--<label>{{ __('Category') }}:</label>--}}
                                        {{--<select name="cms_category_id" id="cms_category_id" class="form-control">--}}
                                            {{--<option value="">...</option>--}}
                                            {{--@foreach($dataCategories as $dataCategory)--}}
                                                {{--@if($dataCategory->object_name == CMS_ABOUTS)--}}
                                                    {{--<option @if($dataCategory->id === $cmsAbout->cms_category_id) selected @endif value="{{ $dataCategory->id }}">--}}
                                                        {{--{{ $dataCategory->name }}--}}
                                                    {{--</option>--}}
                                                {{--@endif--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                    <div class="form-group">
                                        <label>{{ __('Locale') }}:</label>
                                        <select name="locale" id="Locale" class="form-control">
                                            @foreach(locale() as $key => $value)
                                                <option @if($key === $cmsAbout->locale) selected @endif value="{{ $key }}">
                                                    {{ $value }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="contents" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Overview') }}:</label>
                                        <textarea name="overview" class="overview"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                  placeholder="Place some text here">{{ $cmsAbout->overview }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Description') }}:</label>
                                        <textarea name="description" class="description"
                                                  placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $cmsAbout->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Content') }}:</label>
                                        <textarea name="content" id="content" cols="30" rows="10">{{ $cmsAbout->content }}</textarea>
                                    </div>
                                </div>
                                <div id="meta" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Tag Title') }}</label>
                                        <textarea class="form-control" name="tag_title">{{ $cmsAbout->tag_title }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta_Author') }}</label>
                                        <textarea class="form-control" name="meta_author">{{ $cmsAbout->meta_author }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Keywords') }}</label>
                                        <textarea class="form-control" name="meta_keywords">{{ $cmsAbout->meta_keywords }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Description') }}</label>
                                        <textarea class="form-control" name="meta_description">{{ $cmsAbout->meta_description }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Meta Canonical') }}</label>
                                        <textarea class="form-control" name="meta_canonical">{{ $cmsAbout->meta_canonical }}</textarea>
                                        <span class="help-block"> Max length 191 chars </span>
                                    </div>
                                </div>
                                <div id="media" class="tab-pane fade">
                                    <br>
                                    <div class="form-group">
                                        <label>{{ __('Image Thumbnail') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_thumbnail">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($cmsAbout->img_thumbnail) }}" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Image Banner') }}: </label>
                                        <input type="file" class="form-control"
                                               name="img_banner">
                                        <span class="help-block"> The file under validation must be an image (jpeg, png, bmp, gif, or svg) </span>
                                        <img width="200px" src="{{ asset($cmsAbout->img_banner) }}" alt="">
                                    </div>

                                </div>
                                <div id="component" class="tab-pane fade">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Active') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_active" @if( $cmsAbout->is_active ) checked @endif />
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Hot') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_hot" @if( $cmsAbout->is_hot ) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('Top') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_top" @if( $cmsAbout->is_top ) checked @endif>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-md-5 control-label">{{ __('New') }}: </label>
                                                <div class="col-md-7">
                                                    <input type="checkbox" name="is_new" @if( $cmsAbout->is_new ) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('cms-abouts.index') }}" class="btn btn-danger">Back to list</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>--}}
    <script>
        $("[name='is_active']").bootstrapSwitch();
        $("[name='is_top']").bootstrapSwitch();
        $("[name='is_hot']").bootstrapSwitch();
        $("[name='is_new']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            CKEDITOR.replace('content');
            //bootstrap WYSIHTML5 - text editor
            $('.overview').wysihtml5();
            $('.description').wysihtml5();
        })
        {{--$(function () {--}}
            {{--$("#cms_abouts_update").validate({--}}
                {{--rules: {--}}
                    {{--title: { required: true, maxlength: 191 },--}}
                    {{--slug: { required: true, maxlength: 250 },--}}
                    {{--tag_title: { maxlength: 191 },--}}
                    {{--meta_author: { maxlength: 191 },--}}
                    {{--meta_keywords: { maxlength: 191 },--}}
                    {{--meta_description: { maxlength: 191 },--}}
                    {{--meta_canonical: { maxlength: 191 }--}}
                {{--},--}}
                {{--submitHandler: function (form) {--}}
                    {{--$.ajaxSetup({--}}
                        {{--headers: {--}}
                            {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                        {{--}--}}
                    {{--});--}}

                    {{--var dataForm = $(form).serializeArray();--}}
                    {{--var formData = new FormData();--}}

                    {{--$.each(dataForm, function(i, field){--}}
                        {{--if (field.value === 'on')--}}
                            {{--field.value = 1;--}}
                        {{--formData.append(field.name, field.value);--}}
                    {{--});--}}

                    {{--$.ajax({--}}
                        {{--url: '{{ route('cms-abouts.update', $cmsAbout->id) }}',--}}
                        {{--type: 'post', // Use POST with X-HTTP-Method-Override or a straight PUT if appropriate.--}}
                        {{--headers: {"X-HTTP-Method-Override": "PUT"}, // X-HTTP-Method-Override set to PUT.--}}
                        {{--contentType: false,--}}
                        {{--processData: false,--}}
                        {{--cache: false,--}}
                        {{--dataType: 'json',--}}
                        {{--data: formData,--}}
                        {{--success: function (rs) {--}}
                            {{--console.log(rs);--}}
                            {{--if (rs.status === 200) {--}}
                                {{--$.notify(rs.msg, "success");--}}
                            {{--} else {--}}
                                {{--$.notify(rs.msg, "error");--}}
                            {{--}--}}
                        {{--},--}}
                        {{--error: function(XMLHttpRequest, textStatus, errorThrown) {--}}
                            {{--alert("Status: " + textStatus); alert("Error: " + errorThrown);--}}
                        {{--}--}}
                    {{--})--}}
                {{--}--}}
            {{--});--}}
        {{--})--}}
    </script>
@endsection
