@extends('layouts.app-admin')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('cms-categories.index') }}" class="btn btn-success "><i
                                        class="fa fa-arrow-circle-left"></i> <strong>{{ __('Back') }}</strong></a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['route' => 'cms-categories.store'],['id'=>'cms_store']) !!}
                        @csrf
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            <li><a data-toggle="tab" href="#contents">{{ __('Content') }}</a></li>
                            <li><a data-toggle="tab" href="#meta">{{ __('SEO') }}</a></li>
                            <li><a data-toggle="tab" href="#component">{{ __('Component') }}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" class="tab-pane fade in active">
                                <br>
                                <div class="form-group">
                                    {!! Form::label('title', 'Name:') !!}
                                    <span class="required text-danger"> * </span>
                                    {!! form::text('name',old('name'),['class'=>'form-control',
                                    'placeholder'=>'Enter name','required','onkeyup'=>"generateSlug('name','slug')",'id'=>'name'])!!}
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('title', 'Slug:') !!}
                                    <span class="required text-danger"> * </span>
                                    {!! form::text('slug',old('slug'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'slug']) !!}
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label>{{ __('In the category group') }}:</label>
                                        <select name="object_name" id="object_name" class="form-control" required>
                                            <option value="">pleasse choose</option>
                                            @foreach(object_name() as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{--<div class="col-sm-6">
                                        <label>{{ __('Parent_id') }}:</label>
                                        <select class="form-control" name="parent_id" id="parent_id">
                                            <option class='choose' value="0">please choose category</option>
                                        </select>
                                    </div>--}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('locale','Locale:') !!}
                                            <select name="locale" id="Locale" class="form-control">
                                                @foreach(locale() as $key => $value)
                                                    <option value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="contents" class="tab-pane fade">
                                <br>
                                <div class="form-group">
                                    {!! Form::label('overview','Overview:') !!}
                                    <textarea name="overview" class="overview"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                              placeholder="Place some text here">{{ old('overview') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Description') }}:</label>
                                    <textarea name="description" class="description"
                                              placeholder="Place some text here"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('description') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Content') }}:</label>
                                    <textarea name="content" id="content" cols="30" rows="10">{{ old('content') }}</textarea>
                                </div>
                            </div>
                            <div id="meta" class="tab-pane fade">
                                <br>
                                <div class="form-group">
                                    <label>{{ __('Tag Title') }}</label>
                                    <textarea class="form-control" name="tag_title" rows="3"
                                              placeholder="Enter Tag Title">{{ old('tag_title') }}</textarea>
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Meta_Author') }}</label>
                                    <textarea class="form-control" name="meta_author" rows="3"
                                              placeholder="Enter Meta_Author">{{ old('meta_author') }}</textarea>
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Meta Keywords') }}</label>
                                    <textarea class="form-control" name="meta_keywords" rows="3"
                                              placeholder="Enter Meta Keywords">{{old('meta_keywords') }}</textarea>
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Meta Description') }}</label>
                                    <textarea class="form-control" name="meta_description" rows="3"
                                              placeholder="Enter Meta Description">{{ old('meta_description') }}</textarea>
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                                <div class="form-group">
                                    <label>{{ __('Meta Canonical') }}</label>
                                    <textarea class="form-control" name="meta_canonical" rows="3"
                                              placeholder="Enter Meta Canonical">{{ old('meta_canonical') }}</textarea>
                                    <span class="help-block"> Max length 191 chars </span>
                                </div>
                            </div>
                            <div id="component" class="tab-pane fade">
                                <br>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="col-md-5 control-label">{{ __('Active') }}: </label>
                                            <div class="col-md-7">
                                                <input type="checkbox" name="is_active" checked>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                <input class="btn btn-default" type="reset" value="Reset">
                                <a href="{{ route('cms-categories.index') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                        {{--</form>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function () {
            CKEDITOR.replace('content');
            //bootstrap WYSIHTML5 - text editor
            $('.overview').wysihtml5();
            $('.description').wysihtml5();
        })

        /*$(function () {
            $('#object_name').change(function () {
                // $('#object_name option:nth-child(1)').addClass('hidden');
                obj = $(this).find(':selected').val();
                url = "{{--{{ route('get-cate-by-objectname') }}--}}";
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: { object_name: obj },
                    success: function (rs) {
                        $('#parent_id').html(rs.option);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    }
                })
            });
        });*/

    </script>
@endsection
