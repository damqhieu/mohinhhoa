@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-statistics.update', $cmsStatistic->id) }}" method="post">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Name') }}: <span class="required text-danger"> * </span></label>
                                                <input type="text" class="form-control"
                                                       name="name"
                                                       value="{{ $cmsStatistic->name }}"
                                                       placeholder="{{ __('Enter name') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ __('Icon') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="icon"
                                                       value="{{ $cmsStatistic->icon }}"
                                                       placeholder="{{ __('Enter icon') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ __('Value') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="value"
                                                       value="{{ $cmsStatistic->value }}"
                                                       placeholder="{{ __('Enter value') }}">
                                                <span class="help-block"> Value must be a number</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{ __('Locale') }}: <span class="required text-danger"> * </span></label>
                                                <select name="locale" id="Locale" class="form-control">
                                                    @foreach(locale() as $key => $value)
                                                        <option @if($cmsStatistic->locale == $key) selected @endif value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Active') }}: </label>
                                                <br>
                                                <input type="checkbox" name="is_active" @if($cmsStatistic->is_active) checked @endif>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a class="btn btn-danger" href="{{ route('cms-statistics.index') }}">Back to list</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script>
        $(function () {
            $('.answer').wysihtml5();
        })
    </script>
@endsection
