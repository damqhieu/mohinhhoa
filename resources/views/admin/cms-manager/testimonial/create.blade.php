@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('cms-testimonials.store') }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Name') }}: <span
                                                            class="required text-danger"> * </span></label>
                                                <input type="text" class="form-control"
                                                       name="name"
                                                       value="{{ old('name') }}"
                                                       placeholder="{{ __('Enter name') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Position') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="position"
                                                       value="{{ old('position') }}"
                                                       placeholder="{{ __('Enter position') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Title') }}: </label>
                                                <input type="text" class="form-control"
                                                       name="title"
                                                       value="{{ old('title') }}"
                                                       placeholder="{{ __('Enter title') }}">
                                                <span class="help-block"> Max length 191 chars </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Image') }}: </label>
                                                <input type="file" class="form-control"
                                                       name="image">
                                                <span class="help-block"> Max:2MB|mimes:jpeg,png,jpg,gif,svg </span>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{ __('Locale') }}: <span class="required text-danger"> * </span></label>
                                                <select name="locale" id="Locale" class="form-control">
                                                    @foreach(locale() as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">{{ __('Active') }}: </label>
                                                <br>
                                                <input type="checkbox" name="is_active" checked>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{ __('Evaluate') }}: </label>
                                                <textarea name="evaluate" class="evaluate"
                                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"
                                                          placeholder="Place some text here">{{ old('evaluate') }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('cms-testimonials.index') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script>
        $("[name='is_active']").bootstrapSwitch();
    </script>
    <script>
        $(function () {
            $('.evaluate').wysihtml5();
        })
    </script>
@endsection
