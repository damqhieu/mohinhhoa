@section('style')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('transaction-booking-rentals.create') }}" class="btn btn-success "><i
                                        class="fa fa-plus-square"></i> <strong>Create</strong></a>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Phone') }}</th>
                                <th>{{ __('Rental') }}</th>
                                <th>{{__('Time Departure ') }}</th>
                                <th>{{__('Time Arrival  ') }}</th>
                                <th>{{__('Booking Time') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bookingRentals as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->name  }}</td>
                                    <td>{{ $item->phone  }}</td>
                                    <td>{{ $item->transport_rental->name  }}</td>
                                    <td>
                                        {{ $item->time_departure  }}
                                    </td>
                                    <td>
                                        {{ $item->time_arrival  }}
                                    </td>
                                    <td>
                                        {{ date('H:i m-d-Y ',$item->booking_time)  }}
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('transaction-booking-rentals.edit', $item->id) }}"
                                           class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a href="{{ route('transaction-booking-rentals.destroy', $item->id) }}"
                                           class="btn btn-danger btn-xs destroy"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $(function () {
            $('#db_table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
        });
    </script>
@endsection
