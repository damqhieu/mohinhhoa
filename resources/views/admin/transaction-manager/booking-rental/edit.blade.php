@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('transaction-booking-rentals.update',$bookingRental->id) }}" method="post">
                            @csrf
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#general">{{ __('General') }}</a></li>
                                <li><a data-toggle="tab" href="#time">{{ __('Time') }}</a></li>
                                <li><a data-toggle="tab" href="#booking">{{ __('Booking') }}</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="general" class="tab-pane fade in active">
                                    <br>

                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Rental') }}:
                                                <span class="required text-danger"> * </span>
                                            </label>
                                            <select name="transport_rental_id" class="form-control ">
                                                @foreach($rentals as $item)
                                                    <option @if($item->name == $bookingRental->name) selected @endif value="{{ $item->id }}"> {{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Name') }}:
                                            </label>
                                            <input type="text" name="name" value="{{ $bookingRental->name }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Email') }}:
                                            </label>
                                            <input type="email" name="email" value="{{ $bookingRental->email }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Phone') }}:
                                            </label>
                                            <input type="phone" name="phone" value="{{ $bookingRental->phone }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{ __('Address') }}:
                                            </label>
                                            <input type="address" name="address" value="{{ $bookingRental->address }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('Flight number') }}:
                                            </label>
                                            <input type="text" class="form-control" name="flight_number"
                                                   value="{{ $bookingRental->flight_number }}">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <label>{{ __('Quantity adult') }}:
                                        </label>
                                        <div class="input-group">
                                             <span class="input-group-btn">
                                                 <button type="button" class="btn btn-default btn-number"
                                                         data-type="minus" data-field="quantity_adult">
                                                    <span class="glyphicon glyphicon-minus"></span>
                                                </button>
                                             </span>
                                            <input type="text" name="quantity_adult" class="form-control input-number"
                                                   value="{{ $bookingRental->quantity_adult }}" min="0" max="1000">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number"
                                                        data-type="plus" data-field="quantity_adult">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <label>{{ __('Quantity child') }}:
                                        </label>
                                        <div class="input-group">
                                             <span class="input-group-btn">
                                                 <button type="button" class="btn btn-default btn-number"
                                                         data-type="minus" data-field="quantity_child">
                                                    <span class="glyphicon glyphicon-minus"></span>
                                                </button>
                                             </span>
                                            <input type="text" name="quantity_child" class="form-control input-number"
                                                   value="{{ $bookingRental->quantity_child }}" min="0" max="1000  ">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-default btn-number"
                                                        data-type="plus" data-field="quantity_child">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div id="time" class="tab-pane fade">
                                    <br>

                                    <div class="col-md-3 ">
                                        <div class="form-group">
                                            <label>{{ __('Time departure') }}:
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="time_departure"
                                                       value="{{ $bookingRental->time_departure }}"
                                                       class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-md-offset-3">
                                        <div class="form-group">
                                            <label>{{ __('Time arrival') }}:
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="time_arrival" value="{{ $bookingRental->time_arrival }}"
                                                       class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                            <span class="help-block"> Enter the correct format </span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Departure date:</label>

                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="departure_date"
                                                       value="{{ $bookingRental->departure_date }}"
                                                       class="form-control pull-right" id="datepicker">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-md-offset-3">
                                        <div class="form-group">
                                            <label>Return date:</label>

                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="return_date"
                                                       value="{{ $bookingRental->return_date }}" class="form-control pull-right"
                                                       id="datepicker1">
                                            </div>
                                            <!-- /.input group -->
                                        </div>
                                    </div>
                                </div>
                                <div id="booking" class="tab-pane fade">
                                    <br>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>{{ __('Booking note') }}:
                                            </label>
                                            <textarea name="booking_note" id="" cols="30" rows="3"
                                                      class="form-control">{{ $bookingRental->booking_note }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('Payment Method') }}:
                                            </label>
                                            <select name="payment_method" id="" class="form-control">
                                                <option value="VISA">VISA</option>
                                                <option value="MASTERCARD">MASTERCARD</option>
                                                <option value="NORMAL">NORMAL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('Payment Status') }}:
                                            </label>
                                            <select name="payment_status" id="" class="form-control">
                                                @foreach(payment_status() as $key => $value)
                                                    <option @if($key == $bookingRental->payment_status) selected @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>{{ __('Booking status') }}:</label>
                                            <select name="booking_status" id="" class="form-control">
                                                @foreach(payment_method() as $key => $value)
                                                    <option @if($key == $bookingRental->booking_status) selected @endif value="{{ $key }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="text-center">
                                    <br><br>
                                    <button class="btn btn-success" type="submit" name="btnSubmit">Submit</button>
                                    <input class="btn btn-default" type="reset" value="Reset">
                                    <a href="{{ route('transaction-booking-rentals.index') }}"
                                       class="btn btn-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>
@endsection
@section('style')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css">

    {{--<link rel="stylesheet" href="{{asset('backend/plugins/timepicker/bootstrap-timepicker.min.css')}}">--}}
    {{--time picker--}}
    <link rel="stylesheet" href="{{ asset('backend/timepicker/stylesheets/wickedpicker.css') }}">

    <style>
        .input-group-btn:last-child > .btn {
            padding: 6px 12px;
        }
    </style>
@endsection

@section('script')
    <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>
    <script type="text/javascript" src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    {{--time picker--}}
    <script type="text/javascript" src="{{ asset('backend/timepicker/src/wickedpicker.js') }}"></script>
    <script>
        $("[name='is_3d']").bootstrapSwitch();
    </script>
    <script type="text/javascript">
        //time picker
        $('.timepicker').wickedpicker({
            twentyFour: true, title:
                'Time   ', showSeconds: false
        });
        $('#datepicker').datepicker({
            autoclose: true
        })
        $('#datepicker2').datepicker({
            autoclose: true
        })

        $('#booking_type').change(function () {
            if($('#booking_type').val() =="oneway"){
                $('#datepicker2').attr("disabled","true");
            }
            if($('#booking_type').val() =="return"){
                $('#datepicker2').removeAttr("disabled");
            }
        })
    </script>
    <script>
        $('.btn-number').click(function (e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function () {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function () {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            var name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    </script>

@endsection
