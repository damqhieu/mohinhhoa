@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('transaction-bookings.create') }}" class="btn btn-success "><i class="fa fa-plus-square"></i> <strong>Create</strong></a>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ __('Order_code') }}</th>
                                <th>{{ __('Transaction Code') }}</th>
                                <th>{{ __('Trip') }}</th>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Phone') }}</th>
                                <th>{{__('Time departure') }}</th>
                                <th>{{__('Time arrival') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>--}}
    <script>
        $(document).ready(function () {
            $('#db_table').DataTable({
                "order": [[0, "asc"]],
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('transaction-bookings.index') }}",
                "columns": [
                    {"data": "order_code"},
                    {"data": "transaction_code"},
                    {"data": "transport_trip_id"},
                    {"data": "name"},
                    {"data": "phone"},
                    {"data": "time_departure"},
                    {"data": "time_arrival"}
                ],
                "columnDefs": [{
                    "targets": 7, // đây là cột thứ n  tổng cột trong datatable
                    "data": "description",
                    render: function (data, type, row) {

                        let urlEdit = "{{ route('transaction-bookings.edit', 'id') }}";
                        urlEdit = urlEdit.replace('id', row['id']);

                        let urlDelete = "{{ route('transaction-bookings.destroy', 'id') }}";
                        urlDelete = urlDelete.replace('id', row['id']);

                        if (type === "display") {

                            let btnEdit = "<a title='Sửa' href=\"" + urlEdit + "\" class=\"btn btn-xs btn-info\"><i\n" +
                                "                                            class=\"fa fa-pencil\"></i></a>";
                            let btnDelete = "<a title='Xóa' href=\"" + urlDelete + "\" class=\"btn btn-xs btn-danger\"><i\n" +
                                "                                            class=\"fa fa-times\"></i></a>";


                            return   btnEdit + btnDelete
                        }
                        return "";
                    }
                }]
            })
        })
    </script>
@endsection
