@section('style')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('media.create') }}" class="btn btn-success "><i
                                        class="fa fa-plus-square"></i> <strong>Create</strong></a>

                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="db_table" class="table table-bordered table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>{{ __('STT') }}</th>
                                <th>{{ __('Image') }}</th>
                                <th>{{ __('Value') }}</th>
                                <th>{{__('Action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($images as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><img width="300px" src="{{ asset($item->image) }}" alt="" class="img-responsive"></td>
                                    <td>
                                        <input type="text" value="{{ asset($item->image) }}" style="width: 80%"
                                               readonly id="value{{ $item->id }}" class="form-control">
                                        <button class="form-control btn btn-info"  style="width: 60px" onclick="myFunction({{$item->id}})">Copy</button>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('media.destroy', $item->id) }}"
                                           class="btn btn-danger btn-xs destroy"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('style')
    <style>
        tr, td, th {
            text-align: center;
        }
    </style>
@endsection
@section('script')
    <!-- DataTables -->
    <script src="{{ asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        function myFunction(id) {
            var value = "value"+id;
            var copyText = document.getElementById(value);
            copyText.select();
            document.execCommand("copy");
            toastr['success']('', 'Copy success!');
        }
    </script>
    <script>
        $(function () {
            $('#db_table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });

        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
    <script>
        $('.destroy').click(function (e) {
            e.preventDefault();
            var _this = this;
            var url = $(this).attr('href');

            bootbox.confirm({
                title: "Destroy?",
                message: "Are you sure?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            success: function (rs) {
                                if (rs.status === 204) {
                                    $(_this).parents().eq(1).remove();
                                    bootbox.alert("Destroy success!");
                                } else {
                                    bootbox.alert("Destroy not success!");
                                }
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                alert("Status: " + textStatus);
                                alert("Error: " + errorThrown);
                            }
                        })
                    }
                }
            });
        })
    </script>
@endsection
