<div class="header-container header-color color">
    <div class="header_full">
        <div class="header">
            <div class="header-logo show-992">
                <a href="{{route('index')}}" class="logo"> <img class="img-responsive"
                                                                src="{{ (isset($general->img_logo)) ? asset($general->img_logo) : 'client/assets/images/logo-asia.png' }}"
                                                                alt=""/></a>
            </div><!--- .header-logo -->
            <div class="header-bottom">
                <div class="container">
                    <div class="row">
                        <div class="header-config-bg">
                            <div class="header-wrapper-bottom">
                                <div class="custom-menu col-lg-12">
                                    <div class="header-logo hidden-992">
                                        <a href="{{route('index')}}" class="logo"> <img class="img-responsive"
                                                                                        src="{{ (isset($general->img_logo)) ? asset($general->img_logo) : 'client/assets/images/logo-asia.png' }}"
                                                                                        alt=""/></a>
                                    </div><!--- .header-logo -->
                                    <div class="magicmenu clearfix">
                                        <ul class="nav-desktop sticker">
                                            <li class="level0 logo display"><a class="level-top"
                                                                               href="{{route('index')}}"><img alt="logo"
                                                                                                              src="{{ (isset($general->img_logo)) ? asset($general->img_logo) : 'client/assets/images/logo-asia.png' }}"></a>
                                            </li>
                                            <li class="level0 home">
                                                <a class="level-top" href="{{route('index')}}"><span
                                                            class="icon-home fa fa-home"></span><span class="icon-text">Home</span></a>

                                            </li>
                                            <li class="level0 cat">
                                                <a class="level-top" href="{{route('booking')}}"></span><span class="icon-text">Booking</span></a>

                                            </li>
                                            <li class='level0 cat'
                                                data-options='{"cat_columns":"3","cat_proportions":"3","right_proportions":"2","left_proportions":null}'>
                                                <a class="level-top" href="{{ route('bus-schedule') }}">
                                                    <span>Bus schedule <span class="cat_label New">New</span></span>
                                                    <span class="boder-menu"></span>
                                                </a>
                                            </li>
                                            <li class='level0 cat '>
                                                <a class="level-top" href="{{route('carasia-rental')}}"><span>Car-asia Rental </span><span
                                                            class="boder-menu"></span></a>


                                            {{--<li class='level0 home'>--}}
                                                {{--<a class="level-top" href="{{route('portfolio')}}"><span--}}
                                                            {{--class="icon-text">Portfolio</span><span--}}
                                                            {{--class="boder-menu"></span></a>--}}
                                            {{--</li>--}}
                                            <li class='level0 home'>
                                                <a class="level-top" href="{{route('about')}}"><span class="icon-text">About us</span><span
                                                            class="boder-menu"></span></a>
                                            </li>

                                            <li class='level0 home'>
                                                <a class="level-top" href="{{route('blog')}}"><span class="icon-text">Blog</span><span
                                                            class="boder-menu"></span></a>
                                            </li>
                                            <li class='level0 cat '>
                                                <a class="level-top" href="{{route('contact')}}"><span>Contact Us</span><span
                                                            class="boder-menu"></span></a>

                                        </ul>
                                    </div><!--- .magicmenu -->
                                    <div class="nav-mobile">
                                        <h3 class="mobi-title">Menu Car Asia</h3>
                                        <ul>
                                            <li class="level0 home">
                                                <a class="level-top" href="{{route('index')}}"><span
                                                            class="icon-home fa fa-home"></span><span class="icon-text">Home</span></a>

                                            </li>
                                            <li class="level-top">
                                                <a class="level-top" href="{{route('booking')}}"><span>Booking</span><span
                                                            class="boder-menu"></span></a>
                                            </li>
                                            <li class="level-top">
                                                <a class="level-top"
                                                   href="{{route('bus-schedule')}}"><span>Bus schedule<span
                                                                class="cat_label New">New</span> </span><span
                                                            class="boder-menu"></span></a>
                                            </li>
                                            <li class="level-top">
                                                <a class="level-top" href="{{route('carasia-rental')}}"><span>Car-asia Rental </span><span
                                                            class="boder-menu"></span></a>
                                            </li>
                                            {{--<li class='level0 home'>--}}
                                                {{--<a class="level-top" href="{{route('portfolio')}}"><span--}}
                                                            {{--class="icon-text">Portfolio</span><span--}}
                                                            {{--class="boder-menu"></span></a>--}}
                                            {{--</li>--}}
                                            <li class='level0 home'>
                                                <a class="level-top" href="{{route('about')}}"><span class="icon-text">About us</span><span
                                                            class="boder-menu"></span></a>
                                            </li>

                                            <li class='level0 home'>
                                                <a class="level-top" href="{{route('blog')}}"><span class="icon-text">Blog</span><span
                                                            class="boder-menu"></span></a>
                                            </li>
                                            <li class='level0 home'>
                                                <a class="level-top" href="{{route('contact')}}"><span class="icon-text">Contact Us</span><span
                                                            class="boder-menu"></span></a>
                                            </li>

                                        </ul>
                                    </div><!--- .nav-mobile -->
                                </div><!--- .custom-menu -->
                            </div><!--- .header-wrapper-bottom -->
                        </div><!--- .header-config-bg -->
                    </div><!--- .row -->
                </div><!--- .container -->
            </div><!--- .header-bottom -->
            <div class="header-page clearfix">
                <div class="header-setting">
                    <div class="settting-switcher">
                        <div class="dropdown-toggle">
                            <div class="icon-setting"><i class="icon-settings icons"></i></div>
                        </div>
                        <div class="dropdown-switcher">
                            <div class="top-links-alo">
                                <div class="header-top-link">
                                    <ul class="links">
                                        <li>
                                            <a href="javascript:;" style="color:#0b58a2 !important;"><span class="icon-text">Website counter: <br><strong><?= file_get_contents('views.txt') ?> people viewing </strong></span><span
                                                        class="boder-menu"></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {{--<div class="top-form-language">--}}
                                {{--<div class="lang-switcher">--}}
                                    {{--<div class="label-title">Language</div>--}}
                                    {{--<ul class="language-switcher dropdown">--}}
                                        {{--<li class="current"><span class="label dropdown-icon" style="background-image:url(assets/images/flag_default.png);">&nbsp;</span></li>--}}
                                        {{--<li><a href="#"><span class="label dropdown-icon" style="background-image:url(assets/images/flag_french.png);">&nbsp;</span></a></li>--}}
                                        {{--<li><a href="#"><span class="label dropdown-icon" style="background-image:url(assets/images/flag_german.png);">&nbsp;</span></a></li>--}}
                                        {{--<li><a href="#"><span class="label dropdown-icon" style="background-image:url(assets/images/flag_au.png);">&nbsp;</span></a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
                <div class="header-setting hidden-sm hidden-xs">
                    <div class="settting-switcher">
                        <div class="dropdown-toggle">
                            <div class="icon-setting">
                                <i class="icon-arrow-up icons"></i>
                            </div>
                        </div>
                        <div class="dropdown-switcher dropdown-full">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="collapsible top-dropdown1">
                                            <h3 class="block-title heading">{{ (isset($about->title)) ? $about->title : 'Nothing' }}</h3>
                                            <div class="block1 dropdown1 block-content-statick">
                                                <p>
                                                @if(isset($about->overview))
                                                    {!! $about->overview !!}
                                                @elseif(isset($about->description))
                                                    {!! $about->description !!}
                                                @else
                                                     {{ 'Nothing' }}
                                                @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div><!--- About Abani -->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="collapsible top-dropdown2">
                                            <h3 class="block-title heading">Recent News</h3>
                                            <div class="block2 dropdown2 block-content-statick">
                                                <ul>
                                                    @foreach($articles as $item)
                                                    <li class="recent first">
                                                        <div class="recent-left">
                                                            <a href="{{ route('blog-detail',$item->slug) }}"><img class="img-responsive" alt="Recent News"
                                                                             src="{{ (isset($item->img_thumbnail)) ? asset($item->img_thumbnail) : 'http://placehold.it/60x60/ffffff' }}"/></a>
                                                        </div>
                                                        <div class="recent-right"><p class="no-margin"> {!! str_limit($item->content,50,'...') !!} </p></div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--- Recent news-->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="collapsible top-dropdown3">
                                            <h3 class="block-title heading">Get in Touch</h3>
                                            <div class="block3 dropdown3 block-content-statick">
                                                <ul>
                                                    <li class="feature-icon first">
																	<span class="icon-pointer icons">
																		<span class="hidden">hidden</span>
																	</span>
                                                        <p class="no-margin">{{ (isset($general->address)) ? $general->address : '' }}</p>
                                                    </li>
                                                    <li class="feature-icon">
                                                        <span class="icon-envelope-open icons"><span class="hidden">hidden</span></span>
                                                        <p><span>Email: </span>{{ (isset($general->email)) ? $general->email : ''  }}</p>
                                                    </li>
                                                    <li class="feature-icon">
                                                        <span class="icon-call-in icons"><span
                                                                    class="hidden">hidden</span></span>
                                                        <p><span>Phone: </span> {{ (isset($general->hotline)) ? $general->hotline : '' }}</p>
                                                    </li>

                                                    <li class="feature-icon">
																	<span class="icon-link icons">
																		<span class="hidden">hidden</span>
																	</span>
                                                        <p><a href="{{route('index')}}">{{ (isset($general->domain)) ? $general->domain : '' }}</a></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div><!--- Get in touch-->
                                </div>
                            </div><!--- .container -->
                        </div><!--- .dropdown-switcher -->
                    </div><!--- .settting-switcher -->
                </div><!--- .header-setting -->

            </div><!--- .header-page -->
        </div><!--- .header -->
    </div><!--- .header_full -->
</div><!--- .header-container -->
<style>
    .template-default .magicmenu .nav-desktop .level0.over > a, .template-default .magicmenu .nav-desktop .level0.active > a, .template-default .magicmenu .nav-desktop .level0.active > a span.icon-text, .template-default .magicmenu .nav-desktop .level0 > a:hover, .template-default .magicmenu .nav-desktop .level0 > a.icon-text:hover {
        color: #ee1b24;
        background-color: transparent;
    }
</style>