<div class="footer-container footer-color color">
    <div class="block-footer-top block-footer-top-index">
        <div class="container">
            <div class="main">
                <div class="footer">
                    <div class="collapsible mobile-collapsible">
                        <div class="block-newletter">
                            <div class="block block-subscribe">
                                <div class="block-title"><strong><span>Newsletter</span></strong></div>
                                <form action="{{ route('cms-subscribes.store') }}" method="post" id="newsletter-validate-detail">
                                    @csrf
                                    @php
                                        $index_text_subscribe = \App\Models\System\Setting::select('value')->where('key','index_text_subscribe')->first();
                                    @endphp
                                    <div class="block-content">
                                        <div class="form-subscribe-header">
                                            <p>{!!   (isset($index_text_subscribe->value)) ? $index_text_subscribe->value : 'Do You Want<strong>Discover Us?</strong><span>Just Subscribe Now!' !!}</span>
                                            </p>
                                        </div>
                                        <div class="input-box">
                                            <input type="text" name="email" id="newsletter"
                                                   title="Sign up for our newsletter"
                                                   class="input-text required-entry validate-email"
                                                   placeholder="Your email adress ..."/>
                                        </div>
                                        <div class="actions">
                                            <button type="submit" title="Subscribe" class="button">
                                                <span><i class="fa fa-play"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .block-footer-top -->
    <div class="block-footer-bottom">
        <div class="container">
            <div class="main">
                <div class="footer">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="collapsible mobile-collapsible">
                                <h3 class="block-title heading">about us</h3>
                                <div class="title-divider"><span>&nbsp;</span></div>
                                <div class="block1 block-content-statick">
                                    <ul class="list-info">
                                        <li class="feature-icon first">
														<span class="icon-pointer icons">
															<span class="hidden">hidden</span>
														</span>
                                            <p class="no-margin">{{ (isset($general->address)) ? $general->address : '' }}</p>
                                        </li>
                                        <li class="feature-icon">
														<span class="icon-envelope-open icons">
															<span class="hidden">hidden</span>
														</span>
                                            <p>
                                                <a href="mailto:suport@nouthemes.com">{{ (isset($general->email)) ? $general->email : '' }}</a>
                                            </p>
                                        </li>
                                        <li class="feature-icon">
														<span class="icon-call-in icons">
															<span class="hidden">hidden</span>
														</span>
                                            <p>
                                                <span>Phone: </span>{{ (isset($general->hotline)) ? $general->hotline : '' }}
                                            </p>
                                        </li>
                                    </ul>
                                    <ul class="list-inline">
                                        <li><a class="social-link facebook" href="{{ (isset($general->link_facebook)) ? $general->link_facebook : ''  }}"><i
                                                        class="icon-social-facebook"><span
                                                            class="hidden">facebook</span></i></a></li>
                                        <li><a class="social-link twitter" href="{{ (isset($general->link_twitter)) ? $general->link_twitter : ''  }}"><i class="icon-social-twitter"><span
                                                            class="hidden">twitter</span></i></a></li>
                                        <li><a class="social-link youtube" href="javascript:;"><i class="icon-social-youtube"><span
                                                            class="hidden">youtube</span></i></a></li>
                                        <li><a class="social-link github" href="javascript:;"><i class="icon-camera"><span
                                                            class="hidden">github</span></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="collapsible mobile-collapsible">
                                <h3 class="block-title heading">Userful Links</h3>
                                <div class="title-divider"><span>&nbsp;</span></div>
                                <div class="block-content block-content-statick">
                                    <ul class="bullet">
                                        @if(isset($general->link_facebook))
                                            <li><a href="{{ $general->link_facebook }}">My Facebook</a></li>
                                        @endif
                                        @if(isset($general->link_google_plus))
                                            <li><a href="{{ $general->link_google_plus }}">Google Plus</a></li>
                                            @endif
                                        @if(isset($general->link_twitter))
                                            <li><a href="{{ $general->link_twitter }}">Twitter</a></li>
                                            @endif
                                        @if(isset($general->link_linkedin))
                                            <li><a href="{{ $general->link_linkedin }}">Linkedin</a></li>
                                            @endif
                                        @if(isset($general->link_vimeo))
                                            <li><a href="{{ $general->link_vimeo }}">Vimeo</a></li>
                                            @endif
                                        @if(isset($general->link_skype))
                                            <li><a href="{{ $general->link_skype }}">Skype</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="collapsible mobile-collapsible">
                                <h3 class="block-title heading">Customer Care</h3>
                                <div class="title-divider"><span>&nbsp;</span></div>
                                <div class="block-content block-content-statick">
                                    <ul class="bullet">
                                        <li><a href="{{route('bus-schedule')}}">Bus Schedule</a></li>
                                        <li><a href="{{route('carasia-rental')}}">Car-asia Rental</a></li>
                                        {{--<li><a href="{{route('portfolio')}}">Portfolio</a></li>--}}
                                        <li><a href="{{route('about')}}">About Us</a></li>
                                        <li><a href="{{route('blog')}}">Blog</a></li>
                                        <li><a href="{{route('faq')}}">FAQS</a></li>
                                        <li><a href="{{route('term')}}">Terms & Condition</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <h3 class="block-title heading">Featured News</h3>
                            <div class="title-divider"><span>&nbsp;</span></div>
                            <div class="collapsible mobile-collapsible">
                                <div class="block">
                                    <div class="block-content">
                                        <ul class="featured-list list-info">
                                            @foreach($articles as $item)
                                                <li>
                                                    <a href="{{route('blog-detail',$item->slug)}}" class="thumb"><img
                                                                src="{{ (isset($item->img_thumbnail)) ? asset($item->img_thumbnail) : 'http://placehold.it/60x60' }}"
                                                                alt="{{ $item->title }}" width="60" height="60"></a>
                                                    <a href="#" class="title">{{ $item->title }}</a>
                                                    <div class="date">{{ date('d-m-Y', strtotime($item->created_at)) }}</div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .block-footer-bottom -->
    <div class="block-footer-copyright">
        <div class="container">
            <div class="footer">
                <div class="main">
                    <div class="row">
                        <address>
                            © 2018 Designed by <a class="active" href=""> {{ $general->name ?? 'Car Asia' }}</a> - All rights reserved - Author: <a class="active" href="https://github.com/tongvanduc">Tong Van Duc</a>
                        </address>
                        <div class="paypal"><img class="img-responsive" alt="" src="client/assets/images/paypal.png"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- .block-footer-copyright -->
    <a href="#" id="backtotop" style="bottom: 80px;display: block;background:  red;"><span class="fa fa-angle-up"></span><span class="back-to-top">Back to Top</span></a>
</div><!-- .footer-container -->

