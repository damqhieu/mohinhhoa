<script type="text/javascript" src="client/assets/scripts/jquery.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.noconflict.js"></script>
<script type="text/javascript" src="client/assets/scripts/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.bxslider.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.ddslick.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.easing.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.meanmenu.hack.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.animateNumber.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery.heapbox-0.9.4.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/packery-mode.pkgd.min.js"></script>
<script type="text/javascript" src="client/assets/scripts/video.js"></script>
<script type="text/javascript" src="client/assets/scripts/jquery-ui.js"></script>

<script type="text/javascript" src="client/assets/scripts/magiccart/magicproduct.js"></script>
<script type="text/javascript" src="client/assets/scripts/magiccart/magicaccordion.js"></script>
<script type="text/javascript" src="client/assets/scripts/magiccart/magicmenu.js"></script>

<script type="text/javascript" src="client/assets/scripts/script.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="client/assets/scripts/bootstrap/html5shiv.js"></script>
<script type="text/javascript" src="client/assets/scripts/bootstrap/respond.min.js"></script> <![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="client/assets/scripts/lte-ie7.js"></script>
<script type="text/javascript" src="client/assets/scripts/ds-sleight.js"></script>
<![endif]-->