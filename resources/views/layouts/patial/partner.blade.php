<div class="alo-brands">
    <div class="container">
        <div class="main">
            <div class="row">
                <div class="col-lg-12">
                    <div id="footer-brand">
                        <ul class="magicbrand">
                            @foreach($partners as $item)
                                <li> <a > <img class="brand img-responsive" src="{{ (isset($item->image) ? asset($item->image ) : 'http://placehold.it/190x80/ffffff' ) }}" alt="{{$item->name}}" title="{{$item->name}}" /> </a></li>
                            @endforeach
                        </ul>
                    </div><!-- #footer-brand -->
                </div>
            </div>
        </div>
    </div><!-- .container-->
</div><!-- .alo-brands -->