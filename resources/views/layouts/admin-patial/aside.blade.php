<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('backend/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="javascript:;" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
    <?php
    $routeName = Route::currentRouteName();
    ?>
    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@if($routeName == 'admin.dashboard') active @endif">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview @if(strpos($routeName, 'transport') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-car"></i>
                    <span>{{ __('Transport Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'transport') === 0) display: block; @endif">
                    <li class="@if($routeName == 'transport-places.index') active @endif"><a
                                href="{{ route('transport-places.index') }}"><i class="fa fa-circle-o"></i> {{ __('Transport Place') }}</a>
                    </li>
                    <li class="@if($routeName == 'transport-trips.index') active @endif"><a
                                href="{{ route('transport-trips.index') }}"><i class="fa fa-circle-o"></i> {{ __('Transport Trip') }}</a>
                    </li>
                    <li class="@if($routeName == 'transport-schedules.index') active @endif"><a
                                href="{{ route('transport-schedules.index') }}"><i class="fa fa-circle-o"></i> {{ __('Transport Schedule') }}
                        </a></li>
                    <li class="@if($routeName == 'transport-rentals.index') active @endif"><a
                                href="{{ route('transport-rentals.index') }}"><i class="fa fa-circle-o"></i> {{ __('Transport Rental') }}
                        </a></li>
                </ul>
            </li>
            <li class="treeview @if(strpos($routeName, 'transaction') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-cart-plus"></i>
                    <span>{{ __('Transaction Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'transaction') === 0) display: block; @endif">
                    <li class="@if($routeName == 'transaction-bookings.index') active @endif"><a
                                href="{{ route('transaction-bookings.index') }}"><i class="fa fa-circle-o"></i> {{ __('Booking') }}
                        </a></li>
                    <li class="@if($routeName == 'transaction-booking-rentals.index') active @endif"><a
                                href="{{ route('transaction-booking-rentals.index') }}"><i class="fa fa-circle-o"></i> {{ __('Booking Rental') }}
                        </a></li>
                    {{--<li class=""><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Sales') }}</a></li>--}}
                    {{--<li class=""><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Promotion') }}</a></li>--}}
                </ul>
            </li>
            <li class="treeview @if(strpos($routeName, 'cms') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-pie-chart"></i>
                    <span>{{ __('CMS Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'cms') === 0) display: block; @endif">
                    <li class="@if($routeName == 'cms-categories.index') active @endif"><a
                                href="{{ route('cms-categories.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Categories') }}</a></li>
                    <li class="@if($routeName == 'cms-abouts.index') active @endif"><a
                                href="{{ route('cms-abouts.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS About') }}</a></li>
                    <li class="@if($routeName == 'cms-contacts.index') active @endif"><a
                                href="{{ route('cms-contacts.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Contact') }}</a></li>
                    <li class="@if($routeName == 'cms-articles.index') active @endif"><a
                                href="{{ route('cms-articles.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Article') }}</a></li>
                    <li class="@if($routeName == 'cms-feedbacks.index') active @endif"><a
                                href="{{ route('cms-feedbacks.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Feedback') }}</a></li>
                    <li class="@if($routeName == 'cms-services.index') active @endif"><a
                                href="{{ route('cms-services.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Service') }}</a></li>
                    <li class="@if($routeName == 'cms-slides.index') active @endif"><a
                                href="{{ route('cms-slides.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Slide') }}</a></li>
                    <li class="@if($routeName == 'cms-faqs.index') active @endif"><a
                                href="{{ route('cms-faqs.index') }}"><i class="fa fa-circle-o"></i> {{ __('CMS Faq') }}
                        </a></li>
                    <li class="@if($routeName == 'cms-partners.index') active @endif"><a
                                href="{{ route('cms-partners.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Partner') }}</a></li>
                    {{--<li class="@if($routeName == 'cms-statistics.index') active @endif"><a--}}
                                {{--href="{{ route('cms-statistics.index') }}"><i--}}
                                    {{--class="fa fa-circle-o"></i> {{ __('CMS Statistic') }}</a></li>--}}
                    <li class="@if($routeName == 'cms-testimonials.index') active @endif"><a
                                href="{{ route('cms-testimonials.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Testimonial') }}</a></li>
                    <li class="@if($routeName == 'cms-portfolios.index') active @endif"><a
                                href="{{ route('cms-portfolios.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Portfolio') }}</a></li>
                    <li class="@if($routeName == 'cms-subscribes.index') active @endif"><a
                                href="{{ route('cms-subscribes.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('CMS Subscribe') }}</a></li>
                    <li class="@if($routeName == 'cms-tags.index') active @endif"><a
                                href="{{ route('cms-tags.index') }}"><i class="fa fa-circle-o"></i> {{ __('CMS Tag') }}
                        </a></li>
                    <li class="@if($routeName == 'cms-pages.index') active @endif"><a
                                href="{{ route('cms-pages.index') }}"><i class="fa fa-circle-o"></i> {{ __('CMS Page') }}
                        </a></li>
                </ul>
            </li>
            <li class="treeview @if(strpos($routeName, 'media') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-picture-o"></i>
                    <span>{{ __('Media') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'transaction') === 0) display: block; @endif">
                    <li class="@if($routeName == 'media.index') active @endif"><a
                                href="{{ route('media.index') }}"><i class="fa fa-circle-o"></i> {{ __('Media') }}
                        </a></li>
                    {{--<li class=""><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Sales') }}</a></li>--}}
                    {{--<li class=""><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Promotion') }}</a></li>--}}
                </ul>
            </li>
            <li class="treeview @if(strpos($routeName, 'user') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>{{ __('User Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'user') === 0) display: block; @endif">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Member') }}</a></li>
                    <li class="@if($routeName == 'user-admins.index') active @endif"><a
                                href="{{ route('user-admins.index') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('Administrator') }}</a></li>
                </ul>
            </li>

            <li class="treeview @if(strpos($routeName, 'system') === 0) menu-open @endif">
                <a href="javascript:;">
                    <i class="fa fa-cogfa fa-cog"></i>
                    <span>{{ __('System Administrator') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" style="@if(strpos($routeName, 'system') === 0) display: block; @endif">
                    <li class="@if($routeName == 'system-general.save') active @endif"><a
                                href="{{ route('system-general.save') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('General Configuration') }}</a></li>
                    <li class="@if($routeName == 'system-setting.save') active @endif"><a
                                href="{{ route('system-setting.save') }}"><i
                                    class="fa fa-circle-o"></i> {{ __('Application Setting') }} (
                            <small>{{ __('key => value') }} </small>
                            )</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>