<section class="content-header">
    <h1>
        {{ $page }}
        <small>{{ $page_small}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ $page }}</li>
    </ol>
</section>