<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="template-default template-all">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {!! SEO::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <base href="{{ asset('') }}">
    <link rel="stylesheet" type="text/css" href="client/assets/styles/styles.css" media="all"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <meta name="robots" content="index,follow">
    @yield('style')
    <style>  
       .block-content .actions button.button {
            height: 38px !important;
        }
    </style>
    {!! $general->google_analytics ?? '' !!}
    {!! $general->google_webmaster ?? '' !!}
    {!! $general->google_adsense ?? '' !!}
    {!! $general->social_share ?? '' !!}
    {!! $general->live_chat ?? '' !!}
</head>
<body>
<div class="wrapper">
    <div class="page">
        @include('layouts.patial.header')

        <div class="main-container col1-layout content-color color">
            @yield('content')
            @include('layouts.patial.partner')
        </div><!--- .main-container -->
        @include('layouts.patial.footer')
    </div><!--- .page -->
</div><!--- .wrapper -->


@include('layouts.patial.script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    @if (Session::has('error'))
        toastr['error']('', '{!! Session::get("error") !!}');
    @elseif(Session::has('success'))
        toastr['success']('', '{!! Session::get("success") !!}');
    @elseif(Session::has('warning'))
        toastr['warning']('', '{!! Session::get("warning") !!}');
    @endif
</script>
@yield('js')
<link rel="stylesheet" type="text/css" href="client/assets/styles/styles-ie.css" media="all"/>
{{--<script>--}}
    {{--jQuery(document).ready(function ($) {--}}
        {{--$('#newsletter-validate-detail').submit(function () {--}}
            {{--var email = $("input[name='email']").val();--}}

            {{--$.ajax({--}}
                {{--url: "{{route('cms-subscribes.store')}}",--}}
                {{--method: 'post',--}}
                {{--data: {--}}
                    {{--email: email,--}}
                    {{--_token: $('#ajaxToken').val()--}}
                {{--},--}}
                {{--dataType: 'JSON',--}}
                {{--success: function (rs) {--}}
                    {{--if (rs) {--}}
                        {{--alert('success');--}}
                    {{--}--}}
                    {{--else {--}}
                        {{--alert('fail');--}}
                    {{--}--}}
                {{--}--}}
            {{--});--}}
            {{--return false;--}}
        {{--});--}}
    {{--})--}}
{{--</script>--}}
</body>
</html>