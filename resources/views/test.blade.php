<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <title>Car - Asia RESERVATION #{{ $transaction_code  ?? ''}}</title>
    <style type="text/css">
        .image-center{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
        }
        #tables{
            width: 80% !important;
            margin-left: 10% !important;
        }
        .td-left{
            padding:8px 10px 8px 20px;
            font-family:Arial,Helvetica,sans-serif;
            color:#666666;
            font-size:12px;
            border-bottom:1px solid #dcdcdc
        }
        .td-right{
            padding:8px 20px 8px 10px;font-family:Arial,Helvetica,sans-serif;font-size:12px; color:#252525; border-bottom:1px solid #dcdcdc;
        }
    </style>
</head>

<body>
<table cellspacing="0" cellpadding="0" border="0"  id="tables">
    <tbody>
    <tr>
        <td style="border-collapse:collapse;border-left:1px solid #ff6e40;border-right:1px solid #ff6e40">
            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                <tr>
                    <td style="padding:18px 20px 0px 20px;vertical-align:middle;line-height:20px;font-family:Arial;background-color:#ff6e40;text-align:center"><span
                                class="im">
								<span style="color:#ffffff;font-size:115%;text-transform:uppercase">Car - Asia RESERVATION #{{ $transaction_code ?? ''}}</span>
								</span>
                        <div style="vertical-align:top;padding-top:8px;padding-bottom:8px;color:#ffffff;font-size:14px;line-height:16px;font-style:italic">
                            <span>Transaction code: {{ $transaction_code  ?? ''}}</span></div>
                        <div style="vertical-align:top;padding-top:8px;padding-bottom:8px;color:#ffffff;font-size:14px;line-height:16px;font-style:italic">
                            <span><a href="{{ $booking_detail }}">Booking Detail</a></span></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:20px 20px 12px 20px">
                        <span style="font-size:13px;color:#252525;font-family:Arial,Helvetica,sans-serif"> Hello {{ $name  ?? '' }}</span>
                    </td>
                </tr>
                <tr>
                    <td style="padding:4px 20px 12px 20px">
                        <span style="font-size:12px;color:#252525;font-family:Arial,Helvetica,sans-serif;line-height:18px">Booking tickets successfully</span>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 20px 12px 20px">
                        <table style="border-left:1px solid #dcdcdc;border-right:1px solid #dcdcdc" width="100%"
                               cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                            <tr>
                                <td colspan="2"
                                    style="font-family:Arial,Helvetica,sans-serif;background-color:#f2f2f2;padding:8px 20px;border-top:1px solid #dcdcdc"
                                    align="center"><span style="font-size:13px;color:#252525;font-weight:bold">Your Trip Detail </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Transaction Code:</span></td>
                                <td class="td-right" align="left"><strong>{{ $transaction_code ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Email:</span></td>
                                <td class="td-right" align="left"><strong>{{ $email ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Phone:</span></td>
                                <td class="td-right" align="left"><strong>{{ $phone ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Address:</span></td>
                                <td class="td-right" align="left"><strong>{{ $address ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Time Departure:</span></td>
                                <td class="td-right" align="left"><strong>{{ $time_departure ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Time Arrival:</span></td>
                                <td class="td-right" align="left"><strong>{{ $time_arrival ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Flight Number:</span></td>
                                <td class="td-right" align="left"><strong>{{ $flight_number ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Quantity Adult:</span></td>
                                <td class="td-right" align="left"><strong>{{ $quantity_adult ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Quantity Child:</span></td>
                                <td class="td-right" align="left"><strong>{{ $quantity_child ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Departure Date:</span></td>
                                <td class="td-right" align="left"><strong>{{ $departure_date ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Return Date:</span></td>
                                <td class="td-right" align="left"><strong>{{ $return_date ?? 'No'}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Booking Type:</span></td>
                                <td class="td-right" align="left"><strong>{{ $booking_type ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Booking Status:</span></td>
                                <td class="td-right" align="left"><strong>{{ ucfirst($booking_status) ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Booking Note:</span></td>
                                <td class="td-right" align="left"><strong>{{ $booking_note ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Payment Method:</span></td>
                                <td class="td-right" align="left"><strong>{{ $payment_method ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td class="td-left" width="50%" align="right"><span>Payment Status:</span></td>
                                <td class="td-right" align="left"><strong>{{ $payment_status ?? ''}}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center;"><h4>QR code</h4></td>
                            </tr>
                            <tr >
{{--                                <td colspan="2" style=" border-bottom:1px solid #dcdcdc"> <img src="{{ $message->embed('qrcode/'. md5($order_code) .'.png') }}" class="image-center"></td>--}}
                            </tr>
                            <tr >
                                <td colspan="2" style=" border-bottom:1px solid #dcdcdc">{{ $message->embed('file/term_conditions.pdf') }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td style="padding:4px 20px 12px 20px;border-left:1px solid #ff6e40;border-right:1px solid #ff6e40">
                        <span style="font-size:12px;color:#252525;font-family:Arial,Helvetica,sans-serif">Thank you for your interest and use of our services!</span>
                    </td>
                </tr>

                <tr>
                    <td style="background-color:#6e6e6e;font-size:11px;vertical-align:middle;text-align:center;padding:10px 20px 10px 20px;line-height:18px;border:1px solid #6e6e6e;font-family:Arial;color:#cccccc"
                        valign="middle"><a href="https://www.car-asia.com.vn" style="color: #fff;text-decoration: none;">car-asia.com.vn</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>