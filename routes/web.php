<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('check-user/{token}', function ($token) {
    $user = \App\Admin::where('token', $token)->first();
    if ($user) {
        $user->token = null;
        $user->is_active = 1;
        $user->save();
        return redirect(route('admin.login'));
    }
    return redirect(route('web.index'));
})->name('user.check-user');

/*Route::get('/', function () {
    return view('welcome');
});*/

$suffix = '.html';

Route::group(['namespace' => 'Client'], function () use ($suffix) {

    Route::get('/', 'PageController@index')->name('index')->middleware('countview');
    Route::get('about-us' . $suffix, 'PageController@about')->name('about')->middleware('countview');
    Route::match(['get', 'post'], 'contact' . $suffix, 'PageController@contact')->name('contact')->middleware('countview');
    Route::get('blog' . $suffix, 'BlogController@blog')->name('blog')->middleware('countview');
    Route::get('blog/category/{slug}' . $suffix, 'BlogController@blogCategory')->name('blog-category')->middleware('countview');
    Route::get('blog/{slug}' . $suffix, 'BlogController@blogDetail')->name('blog-detail')->middleware('countview');
    Route::get('portfolio' . $suffix, 'PageController@portfolio')->name('portfolio')->middleware('countview');
    Route::get('bus-schedule' . $suffix, 'PageController@busSchedule')->name('bus-schedule')->middleware('countview');
    Route::get('carasia-rental' . $suffix, 'PageController@carasiaRental')->name('carasia-rental')->middleware('countview');
    Route::get('booking/{trip?}', 'PageController@booking')->name('booking')->middleware('countview');
    Route::get('contact-us' . $suffix, 'PageController@contact')->name('contact')->middleware('countview');
    Route::get('faqs' . $suffix, 'PageController@faq')->name('faq')->middleware('countview');
    Route::get('terms-and-conditions' . $suffix, 'PageController@term')->name('term')->middleware('countview');
    Route::get('contact-us/store' . $suffix, 'ContactController@store')->name('contact.store')->middleware('countview');

    Route::group(['prefix' => 'cms-subscribe'], function () {
        Route::get('/', 'SubscribeController@index')->name('cms-subscribes.index')->middleware('countview');
        Route::post('store', 'SubscribeController@store')->name('cms-subscribes.store');
    });
});

/*************************PAYMENT***********************************/

Route::group(['prefix' => 'payment', 'namespace' => 'Client'], function () use ($suffix) {

    Route::post('handle-booking', 'AlepayController@handleBooking')->name('payment.handle-booking');
    Route::match(['get', 'post'], 'checkout-step1' . $suffix, 'AlepayController@checkoutStep1')->name('payment.checkout-step1');
    Route::get('checkout-step2' . $suffix, 'AlepayController@checkoutStep2')->name('payment.checkout-step2');
    Route::get('empty-booking' . $suffix, 'AlepayController@emptyBooking')->name('payment.empty-booking');
    Route::get('handle-payment' . $suffix, 'AlepayController@handlePayment')->name('payment.handle-payment');
    Route::get('result' . $suffix, 'AlepayController@result')->name('payment.result');
    Route::get('booking-detail/{order_code}-{transaction_code}' . $suffix, 'AlepayController@bookingDetail')->name('payment.booking-detail');

});

/*Route::post('checkout', 'CheckOutNlController@checkout')->name('checkout');
Route::get('checkout-success/{id}', 'CheckOutNlController@checkoutSuccess')->name('checkout-success');
Route::get('booking-detail-qr/{order_code}', 'CheckOutNlController@bookingQrCode')->name('booking-detail-qr');
Route::any('payment-success', 'CheckOutNlController@paymentSuccess')->name('payment-success');
Route::get('cancel-url/{id}', 'CheckOutNlController@cancelUrl')->name('cancel-url');*/

/*Route::group(['prefix' => 'paypal'], function () {
    Route::post('/', 'PayPalController@index')->name('paypal.index');
    Route::get('status', 'PayPalController@status')->name('paypal.status');
    Route::get('detail/{id}', 'PayPalController@paymentDetail')->name('paypal.detail');
    Route::get('lists', 'PayPalController@paymentList')->name('paypal.lists');
});*/


Route::get('routes', function () {
    $routeCollection = Route::getRoutes();

    echo "<table style='width:100%'>";
    echo "<tr>";
    echo "<td width='10%'><h4>HTTP Method</h4></td>";
    echo "<td width='10%'><h4>Route</h4></td>";
    echo "<td width='10%'><h4>Name</h4></td>";
    echo "<td width='70%'><h4>Corresponding Action</h4></td>";
    echo "</tr>";
    foreach ($routeCollection as $value) {
        if (strpos($value->uri(), 'admin') !== 0) {
            echo "<tr>";
            echo "<td>" . json_encode($value->methods()) . "</td>";
            echo "<td>" . $value->uri() . "</td>";
            echo "<td>" . $value->getName() . "</td>";
            echo "<td>" . $value->getActionName() . "</td>";
            echo "</tr>";
        }
    }
    echo "</table>";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*Route::get('/mail', function () {
    $dataSendMail = [
        'transaction_code' => 'transaction_code',
        'order_code' => 'order_code',
        'name' => 'name',
        'email' => 'email',
        'phone' => 'phone',
        'address' => 'address',
        'time_departure' => 'time_departure',
        'time_arrival' => 'time_arrival',
        'flight_number' => 'flight_number',
        'quantity_adult' => 'quantity_adult',
        'quantity_child' => 'quantity_child',
        'departure_date' => 'departure_date',
        'return_date' => 'return_date',
        'booking_type' => 'booking_type',
        'booking_status' => 'booking_status',
        'booking_note' => 'booking_note',
        'payment_method' => 'payment_method',
        'payment_status' => 'payment_status',
        'booking_detail' =>  'https://www.car-asia.com.vn/carasia-rental'
    ];

    \Illuminate\Support\Facades\Mail::send('test', $dataSendMail, function ($message) {
        $message->to('tongduc315@gmail.com');
        $message->subject('Car - Asia RESERVATION #' . 'transaction_code');
    });
});*/


