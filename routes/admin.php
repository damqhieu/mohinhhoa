<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 11/03/2018
 * Time: 10:26 SA
 */

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'AdminLoginController@logout')->name('admin.logout.submit');
    Route::post('/forgot-password', 'AdminLoginController@logout')->name('admin.forgot-password.submit');

    Route::post('/password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'AdminResetPasswordController@reset')->name('admin.password.admin-request');
});
Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');

Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {
    Route::resource('user-admins', 'UserAdminController')->except('show');

    Route::group(['namespace' => 'Cms', 'prefix' => 'cms-manager'], function () {
        Route::get('cms-subscribes', 'SubscribeController@index')->name('cms-subscribes.index');
//        Route::resource('cms-abouts', 'CmsAboutController', ['except' => 'show']);
        Route::group(['prefix' => 'cms-pages'], function () {
            Route::get('/', 'CmsPageController@index')->name('cms-pages.index');
            Route::get('create', 'CmsPageController@create')->name('cms-pages.create');
            Route::post('store', 'CmsPageController@store')->name('cms-pages.store');
            Route::get('{id}/edit', 'CmsPageController@edit')->name('cms-pages.edit');
            Route::post('update/{id}', 'CmsPageController@update')->name('cms-pages.update');
            Route::get('destroy/{id}', 'CmsPageController@destroy')->name('cms-pages.destroy');
        });
        Route::group(['prefix' => 'cms-articles'], function () {
            Route::get('/', 'CmsArticleController@index')->name('cms-articles.index');
            Route::get('create', 'CmsArticleController@create')->name('cms-articles.create');
            Route::post('store', 'CmsArticleController@store')->name('cms-articles.store');
            Route::get('{id}/edit', 'CmsArticleController@edit')->name('cms-articles.edit');
            Route::post('update/{id}', 'CmsArticleController@update')->name('cms-articles.update');
            Route::post('destroy/{id}', 'CmsArticleController@destroy')->name('cms-articles.destroy');
        });
        Route::group(['prefix' => 'cms-abouts'], function () {
            Route::get('/', 'CmsAboutController@index')->name('cms-abouts.index');
            Route::get('create', 'CmsAboutController@create')->name('cms-abouts.create');
            Route::post('store', 'CmsAboutController@store')->name('cms-abouts.store');
            Route::get('{id}/edit', 'CmsAboutController@edit')->name('cms-abouts.edit');
            Route::post('update/{id}', 'CmsAboutController@update')->name('cms-abouts.update');
            Route::get('destroy/{id}', 'CmsAboutController@destroy')->name('cms-abouts.destroy');
        });
        Route::group(['prefix' => 'cms-contacts'], function () {
            Route::get('/', 'CmsContactController@index')->name('cms-contacts.index');
            Route::get('create', 'CmsContactController@create')->name('cms-contacts.create');
            Route::post('store', 'CmsContactController@store')->name('cms-contacts.store');
            Route::get('{id}/edit', 'CmsContactController@edit')->name('cms-contacts.edit');
            Route::post('update/{id}', 'CmsContactController@update')->name('cms-contacts.update');
            Route::post('destroy/{id}', 'CmsContactController@destroy')->name('cms-contacts.destroy');
        });
        Route::group(['prefix' => 'cms-categories'], function () {
            Route::get('/', 'CmsCategoryController@index')->name('cms-categories.index');
            Route::get('create', 'CmsCategoryController@create')->name('cms-categories.create');
            Route::post('store', 'CmsCategoryController@store')->name('cms-categories.store');
            Route::get('{id}/edit', 'CmsCategoryController@edit')->name('cms-categories.edit');
            Route::post('update/{id}', 'CmsCategoryController@update')->name('cms-categories.update');
            Route::post('destroy/{id}', 'CmsCategoryController@destroy')->name('cms-categories.destroy');
            Route::post('get-cate-by-objectname', 'CmsCategoryController@getCateByObjectName')->name('get-cate-by-objectname');
        });
        Route::group(['prefix' => 'cms-feedbacks'], function () {
            Route::get('/', 'CmsFeedbackController@index')->name('cms-feedbacks.index');
            Route::get('{id}/edit', 'CmsFeedbackController@edit')->name('cms-feedbacks.edit');
            Route::post('update/{id}', 'CmsFeedbackController@update')->name('cms-feedbacks.update');
            Route::post('destroy/{id}', 'CmsFeedbackController@destroy')->name('cms-feedbacks.destroy');
        });
        Route::group(['prefix' => 'cms-services'], function () {
            Route::get('/', 'CmsServiceController@index')->name('cms-services.index');
            Route::get('create', 'CmsServiceController@create')->name('cms-services.create');
            Route::post('store', 'CmsServiceController@store')->name('cms-services.store');
            Route::get('{id}/edit', 'CmsServiceController@edit')->name('cms-services.edit');
            Route::post('update/{id}', 'CmsServiceController@update')->name('cms-services.update');
            Route::post('destroy/{id}', 'CmsServiceController@destroy')->name('cms-services.destroy');
        });
        Route::group(['prefix' => 'cms-slides'], function () {
            Route::get('/', 'CmsSlideController@index')->name('cms-slides.index');
            Route::get('create', 'CmsSlideController@create')->name('cms-slides.create');
            Route::post('store', 'CmsSlideController@store')->name('cms-slides.store');
            Route::get('{id}/edit', 'CmsSlideController@edit')->name('cms-slides.edit');
            Route::post('update/{id}', 'CmsSlideController@update')->name('cms-slides.update');
            Route::post('destroy/{id}', 'CmsSlideController@destroy')->name('cms-slides.destroy');
        });
        Route::group(['prefix' => 'cms-slide-element'], function () {
            Route::match(['post', 'get'], '/', 'CmsSlideElementController@index')->name('cms-slide-elements.index');
            Route::get('create', 'CmsSlideElementController@create')->name('cms-slide-elements.create');
            Route::post('store', 'CmsSlideElementController@store')->name('cms-slide-elements.store');
            Route::get('{id}/edit', 'CmsSlideElementController@edit')->name('cms-slide-elements.edit');
            Route::post('update/{id}', 'CmsSlideElementController@update')->name('cms-slide-elements.update');
            Route::post('destroy/{id?}', 'CmsSlideElementController@destroy')->name('cms-slide-elements.destroy');
        });
        Route::group(['prefix' => 'cms-faq'], function () {
            Route::match(['post', 'get'], '/', 'CmsFaqController@index')->name('cms-faqs.index');
            Route::get('create', 'CmsFaqController@create')->name('cms-faqs.create');
            Route::post('store', 'CmsFaqController@store')->name('cms-faqs.store');
            Route::get('{id}/edit', 'CmsFaqController@edit')->name('cms-faqs.edit');
            Route::post('update/{id}', 'CmsFaqController@update')->name('cms-faqs.update');
            Route::post('destroy/{id?}', 'CmsFaqController@destroy')->name('cms-faqs.destroy');
        });
        Route::group(['prefix' => 'cms-partner'], function () {
            Route::match(['post', 'get'], '/', 'CmsPartnerController@index')->name('cms-partners.index');
            Route::get('create', 'CmsPartnerController@create')->name('cms-partners.create');
            Route::post('store', 'CmsPartnerController@store')->name('cms-partners.store');
            Route::get('{id}/edit', 'CmsPartnerController@edit')->name('cms-partners.edit');
            Route::post('update/{id}', 'CmsPartnerController@update')->name('cms-partners.update');
            Route::post('destroy/{id?}', 'CmsPartnerController@destroy')->name('cms-partners.destroy');
        });
        Route::group(['prefix' => 'cms-statistic'], function () {
            Route::match(['post', 'get'], '/', 'CmsStatisticController@index')->name('cms-statistics.index');
            Route::get('create', 'CmsStatisticController@create')->name('cms-statistics.create');
            Route::post('store', 'CmsStatisticController@store')->name('cms-statistics.store');
            Route::get('{id}/edit', 'CmsStatisticController@edit')->name('cms-statistics.edit');
            Route::post('update/{id}', 'CmsStatisticController@update')->name('cms-statistics.update');
            Route::post('destroy/{id?}', 'CmsStatisticController@destroy')->name('cms-statistics.destroy');
        });
        Route::group(['prefix' => 'cms-testimonial'], function () {
            Route::match(['post', 'get'], '/', 'CmsTestimonialController@index')->name('cms-testimonials.index');
            Route::get('create', 'CmsTestimonialController@create')->name('cms-testimonials.create');
            Route::post('store', 'CmsTestimonialController@store')->name('cms-testimonials.store');
            Route::get('{id}/edit', 'CmsTestimonialController@edit')->name('cms-testimonials.edit');
            Route::post('update/{id}', 'CmsTestimonialController@update')->name('cms-testimonials.update');
            Route::post('destroy/{id?}', 'CmsTestimonialController@destroy')->name('cms-testimonials.destroy');
        });
        Route::group(['prefix' => 'cms-portfolio'], function () {
            Route::match(['post', 'get'], '/', 'CmsPortfolioController@index')->name('cms-portfolios.index');
            Route::get('create', 'CmsPortfolioController@create')->name('cms-portfolios.create');
            Route::post('store', 'CmsPortfolioController@store')->name('cms-portfolios.store');
            Route::get('{id}/edit', 'CmsPortfolioController@edit')->name('cms-portfolios.edit');
            Route::post('update/{id}', 'CmsPortfolioController@update')->name('cms-portfolios.update');
            Route::post('destroy/{id?}', 'CmsPortfolioController@destroy')->name('cms-portfolios.destroy');
        });
        Route::group(['prefix' => 'cms-tags'], function () {
            Route::get('/', 'CmsTagController@index')->name('cms-tags.index');
            Route::get('create', 'CmsTagController@create')->name('cms-tags.create');
            Route::post('store', 'CmsTagController@store')->name('cms-tags.store');
            Route::get('{id}/edit', 'CmsTagController@edit')->name('cms-tags.edit');
            Route::post('update/{id}', 'CmsTagController@update')->name('cms-tags.update');
            Route::post('destroy/{id}', 'CmsTagController@destroy')->name('cms-tags.destroy');
        });
    });

    Route::group(['namespace' => 'System', 'prefix' => 'system-administrator'], function () {
        Route::match(['post', 'get'], 'general', 'GeneralController@save')->name('system-general.save');
        Route::match(['post', 'get'], 'setting-key-value/{id?}', 'SettingController@save')->name('system-setting.save');
    });

    Route::group(['namespace' => 'Transaction', 'prefix' => 'transaction'], function () {
        Route::group(['prefix' => 'transaction-booking'], function () {
            Route::get('/', 'TransactionBookingController@index')->name('transaction-bookings.index');
            Route::get('create', 'TransactionBookingController@create')->name('transaction-bookings.create');
            Route::post('store', 'TransactionBookingController@store')->name('transaction-bookings.store');
            Route::get('{id}/edit', 'TransactionBookingController@edit')->name('transaction-bookings.edit');
            Route::post('update/{id}', 'TransactionBookingController@update')->name('transaction-bookings.update');
            Route::get('destroy/{id}', 'TransactionBookingController@destroy')->name('transaction-bookings.destroy');
        });
        Route::group(['prefix' => 'transaction-booking-rentals'], function () {
            Route::get('/', 'TransactionBookingRentalController@index')->name('transaction-booking-rentals.index');
            Route::get('create', 'TransactionBookingRentalController@create')->name('transaction-booking-rentals.create');
            Route::post('store', 'TransactionBookingRentalController@store')->name('transaction-booking-rentals.store');
            Route::get('{id}/edit', 'TransactionBookingRentalController@edit')->name('transaction-booking-rentals.edit');
            Route::post('update/{id}', 'TransactionBookingRentalController@update')->name('transaction-booking-rentals.update');
            Route::get('destroy/{id}', 'TransactionBookingRentalController@destroy')->name('transaction-booking-rentals.destroy');
        });
    });

    Route::group(['namespace' => 'Media', 'prefix' => 'media'], function () {
        Route::get('/', 'MediaController@index')->name('media.index');
        Route::get('create', 'MediaController@create')->name('media.create');
        Route::post('store', 'MediaController@store')->name('media.store');
        Route::post('destroy/{id}', 'MediaController@destroy')->name('media.destroy');
    });

    Route::group(['namespace' => 'Transport', 'prefix' => 'transport'], function () {
        Route::group(['prefix' => 'transport-schedule'], function () {
            Route::get('/', 'TransportScheduleController@index')->name('transport-schedules.index');
            Route::get('create', 'TransportScheduleController@create')->name('transport-schedules.create');
            Route::post('store', 'TransportScheduleController@store')->name('transport-schedules.store');
            Route::get('{id}/edit', 'TransportScheduleController@edit')->name('transport-schedules.edit');
            Route::post('update/{id}', 'TransportScheduleController@update')->name('transport-schedules.update');
            Route::get('destroy/{id?}', 'TransportScheduleController@destroy')->name('transport-schedules.destroy');
        });
        Route::group(['prefix' => 'transport-trip'], function () {
            Route::get('/', 'TransportTripController@index')->name('transport-trips.index');
            Route::get('create', 'TransportTripController@create')->name('transport-trips.create');
            Route::post('store', 'TransportTripController@store')->name('transport-trips.store');
            Route::get('{id}/edit', 'TransportTripController@edit')->name('transport-trips.edit');
            Route::post('update/{id}', 'TransportTripController@update')->name('transport-trips.update');
            Route::get('destroy/{id?}', 'TransportTripController@destroy')->name('transport-trips.destroy');
        });
        Route::group(['prefix' => 'transport-place'], function () {
            Route::get('/', 'TransportPlaceController@index')->name('transport-places.index');
            Route::get('create', 'TransportPlaceController@create')->name('transport-places.create');
            Route::post('store', 'TransportPlaceController@store')->name('transport-places.store');
            Route::get('{id}/edit', 'TransportPlaceController@edit')->name('transport-places.edit');
            Route::post('update/{id}', 'TransportPlaceController@update')->name('transport-places.update');
            Route::get('destroy/{id?}', 'TransportPlaceController@destroy')->name('transport-places.destroy');
        });
        Route::group(['prefix' => 'transport-rental'], function () {
            Route::get('/', 'TransportRentalController@index')->name('transport-rentals.index');
            Route::get('create', 'TransportRentalController@create')->name('transport-rentals.create');
            Route::post('store', 'TransportRentalController@store')->name('transport-rentals.store');
            Route::get('{id}/edit', 'TransportRentalController@edit')->name('transport-rentals.edit');
            Route::post('update/{id}', 'TransportRentalController@update')->name('transport-rentals.update');
            Route::get('destroy/{id?}', 'TransportRentalController@destroy')->name('transport-rentals.destroy');
        });
    });
});
